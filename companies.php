<?php
if(isset($_GET['companyId']) && ($_GET['companyId'])){
	
session_start();
include("_aseanhr_admin/_config/connect.php");
include_once 'langauge/common.php';
	$com_id=$_GET['companyId'];
	$select_count_company=mysqli_query($conn,"select com_id from tbl_company where com_id=$com_id");
	$count_company=mysqli_num_rows($select_count_company);
	if($count_company>0){
		
mysqli_query($conn,'SET NAMES utf8');
$select_com_id=mysqli_query($conn,"SELECT tbl_company . * , tbl_industry.ind_name_en, tbl_industry.ind_name_kh, tbl_province.prov_name_en, tbl_province.prov_name_kh,tbl_company_type.type_name_en,tbl_company_type.type_name_kh
								FROM tbl_company
									INNER JOIN tbl_industry ON tbl_company.ind_id = tbl_industry.ind_id
									INNER JOIN tbl_province ON tbl_company.prov_id = tbl_province.prov_id
									INNER JOIN tbl_company_type ON tbl_company.type_id = tbl_company_type.type_id
									where com_id=$com_id
							");
$row_com=mysqli_fetch_array($select_com_id);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/icon.png" />
<title>ASEANHR - <?=$row_com['com_name']?></title>
<link href="css/style1.css" rel="stylesheet" type="text/css" />
<link href="css/color.css" rel="stylesheet" type="text/css"  />
<!-- Start WOWSlider.com HEAD section -->

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
<!-- wow slide2-->
<link rel="stylesheet" type="text/css" href="js/engine1/style.css" />
<link rel="stylesheet" type="text/css" href="js/engine2/style.css" />
<link rel="stylesheet" type="text/css" href="js/engine4/style.css" />
<!-- wow slide2-->
</head>

<body id="home">

<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('includes/menu.php');?>
        </div>
        
        <div class="container">
        <!-- main -->
            <div class="main">
            	<?php include('includes/company.php');?>
            </div>
        <!-- end main -->
            <div class="side">
            	<?php include('includes/contact_side.php'); ?>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('includes/copy-right.php');?>
</div>
<script type="text/javascript" src="js/engine1/wowslider.js"></script>
<script type="text/javascript" src="js/engine1/script.js"></script>
<script type="text/javascript" src="js/engine2/wowslider.js"></script>
<script type="text/javascript" src="js/engine2/script.js"></script>
<script type="text/javascript" src="js/engine4/wowslider.js"></script>
<script type="text/javascript" src="js/engine4/script.js"></script>
</body>
</html>

<?php	
	}else{
		?>
		<script type="text/javascript">
            window.location.href="/";
        </script>
        <?php
	}
}else{
	?>
	<script type="text/javascript">
		window.location.href="/";
	</script>
	<?php
}

?>