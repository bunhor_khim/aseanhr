<?php
$com_id = $_SESSION['com_id'];   
    if (isset($_POST['btcomsave'])) {        
        $job_title = $_POST['job_title'];
        $job_pub_date = $_POST['job_pub_date'];
        $hiring = $_POST['hiring'];
        $age= $_POST['age'];
        
		$job_term= $_POST['job_term'];
		$qualification =$_POST['qualification'];
		
        $ind_id= $_POST['ind_id'];
        $fun_id= $_POST['fun_id'];
        $prov_id= $_POST['prov_id'];
        $desc= $_POST['desc'];
        $salary= $_POST['salary'];
        $job_require= $_POST['job_require'];
        $apply= $_POST['apply'];

		
		$contact_person=$_POST['contact_person'];
		$phone=$_POST['phone'];
		$email=$_POST['email'];
		$web=$_POST['website'];
		$address=$_POST['address'];
		$language=$_POST['language'];
		$job_alert=$_POST['job_alert'];
				    
		mysqli_query($conn,'SET NAMES utf8');
		mysqli_query($conn,"update tbl_jobs set com_id=$com_id,func_id=$fun_id,prov_id=$prov_id,ind_id=$ind_id,job_title='$job_title',job_hiring='$hiring',job_salary='$salary',job_age='$age',job_pub_date='$job_pub_date',job_desc='$desc',job_require='$job_require',how_to_apply='$apply',job_term='$job_term',job_qual='$qualification',contact_person='$contact_person',phone='$phone',email='$email',web='$web',address='$address',job_lang='$language',job_alert='$job_alert' where job_id='$job_id'") or die(mysql_error());
        ?>
        <script type="text/javascript">
            window.location.href="../employer/";
        </script>
        <?php   
    }
	?> 

<form method="post" enctype="multipart/form-data">
	<!-- column cv -->
    <div class="emp_header"><h3><?=$lang['edit_jobs'];?></h3></div>
    <div class="resume-edit-list" style="padding-top:15px;">
    
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Company']; ?> : </label>
            <?php
			$select_company=mysqli_query($conn,"select com_name,com_id from tbl_company where com_id=$com_id");
			$row_company=mysqli_fetch_array($select_company);
			?>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="company" name="company" value="<?=$row_company['com_name']?>" maxlength="30" disabled="disabled"><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Job_Title']; ?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="job_title" name="job_title" value="<?=$row_job['job_title']?>" maxlength="30" required><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Publish_Date']; ?> : </label>
            <label class="form-filed">
            	<input type="text" class="inputstyle w200" name="job_pub_date" data-beatpicker="true"​ placeholder="yyyy/mm/dd" value="<?=$row_job['job_pub_date']?>" required/>
            </label>
        </div>
        <div class="user-form-item clear">
            <label><?= $lang['Closing_Date']; ?> : </label>
            <label class="form-filed">
            	<p style="color:#F90"><?=$lang['close_date_label'];?></p>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Hiring']; ?> : </label>
            <label class="form-filed">
            	<input  type="text" class="inputstyle w80" id="hiring" name="hiring" maxlength="30" value="<?=$row_job['job_hiring']?>" required><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Category']; ?> : </label>
            <label class="form-filed">
            	<select class="inputstyle w210" name="ind_id" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <?php 
                            $sql=mysqli_query($conn,"select * from tbl_industry");
                            while($row=mysqli_fetch_array($sql)){
                                $m_name = $row['ind_name_en'];
                          ?>  
                          <option <?php if($row_job['ind_id']==$row['ind_id']){echo 'selected="selected"';}?> value="<?php echo $row['ind_id'];?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row['ind_name_kh'];else echo $row['ind_name_en'];?></option>
                       <?php } ?>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Section'];?> : </label>
            <label class="form-filed">
            	<select class="inputstyle w210" name="fun_id" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <?php 
							mysqli_query($conn,'SET NAMES utf8'); 
                            $sql=mysqli_query($conn,"select * from tbl_function");
                            while($row=mysqli_fetch_array($sql)){
                                $m_name = $row['func_name_en'];
                          ?>  
                          <option <?php if($row_job['func_id']==$row['func_id']){echo 'selected="selected"';}?> value="<?php echo $row['func_id'];?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row['func_name_kh'];else echo $row['func_name_en'];?></option>
                       <?php } ?>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Location'];?> : </label>
            <label class="form-filed">
            	<select class="inputstyle w210" name="prov_id" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <?php 
							mysqli_query($conn,'SET NAMES utf8'); 
                            $sql=mysqli_query($conn,"select * from tbl_province");
                            while($row=mysqli_fetch_array($sql)){
                                $prov_name = $row['prov_name_en'];
						?>  
                            <option <?php if($row_job['prov_id']==$row['prov_id']){echo 'selected="selected"';}?> value="<?php echo $row['prov_id'];?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row['prov_name_kh'];else echo $row['prov_name_en'];?></option>
                       <?php } ?>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Salary'];?> : </label>
            <label class="form-filed">
            	<select class="inputstyle w200" name="salary" required>
                        <option <?php if($row_job['job_salary']=='negotiable'){echo 'selected="selected"';}?> value="negotiable"><?= $lang['Negotiable'];?></option>
                        <option <?php if($row_job['job_salary']=='200'){echo 'selected="selected"';}?> value="200"><200</option>
                        <option <?php if($row_job['job_salary']=='200-500'){echo 'selected="selected"';}?> value="200-500">200-500</option>
                        <option <?php if($row_job['job_salary']=='500-1000'){echo 'selected="selected"';}?> value="500-1000">500-1000</option>
                        <option <?php if($row_job['job_salary']=='1000-2000'){echo 'selected="selected"';}?> value="1000-2000">100-200</option>
                        <option <?php if($row_job['job_salary']=='2000'){echo 'selected="selected"';}?> value="2000">>2000</option>
                	</select><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        
        
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Qualification']; ?> : </label>
            <label class="form-filed">
            	<select class="inputstyle w210" name="qualification" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <option <?php if($row_job['job_qual']=='1'){echo 'selected';}?> value="1"><?=$lang['High_School']?></option>  
                        <option <?php if($row_job['job_qual']=='2'){echo 'selected';}?> value="2"><?=$lang['Associate_Degree']?></option>  
                        <option <?php if($row_job['job_qual']=='3'){echo 'selected';}?> value="3"><?=$lang['Bachelor_Degree']?></option>  
                        <option <?php if($row_job['job_qual']=='4'){echo 'selected';}?> value="4"><?=$lang['Master_Degree']?></option>
                        <option <?php if($row_job['job_qual']=='5'){echo 'selected';}?> value="5"><?=$lang['Doctor_Degree']?></option>    
                        <option <?php if($row_job['job_qual']=='6'){echo 'selected';}?> value="6"><?=$lang['Others']?></option><strong></strong>                    
                </select><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Job_Term'];?> : </label>
            <label class="form-filed">
            	<select class="inputstyle w210" name="job_term" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <option <?php if($row_job['job_term']=='1'){echo 'selected';}?> value="1"><?=$lang['full_time']?></option>
                        <option <?php if($row_job['job_term']=='2'){echo 'selected';}?> value="2"><?=$lang['Part_time']?></option>
                        <option <?php if($row_job['job_term']=='3'){echo 'selected';}?> value="3"><?=$lang['Internship']?></option>
                        <option <?php if($row_job['job_term']=='4'){echo 'selected';}?> value="4"><?=$lang['Volunteer']?></option>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Age'];?> : </label>
            <label class="form-filed">
            	<input  type="text" class="inputstyle w80" id="age" name="age" value="<?=$row_job['job_age'];?>" maxlength="2">
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['language']; ?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="language" name="language" value="<?=$row_job['job_lang'];?>"><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['job_alert']; ?> : </label>
            <label class="form-filed">
            <select name="job_alert" id="job_alert" class="inputstyle w210">
            	<option value="">Please Select</option>
            	<option <?php if($row_job['job_alert']=='1'){echo 'selected';}?> value="1"><?=$lang['new'];?></option>
                <option <?php if($row_job['job_alert']=='2'){echo 'selected';}?> value="2"><?=$lang['Urgent'];?></option>
            </select><span class="valid_info"><i style="color: red;">*</i></span>
            </label>
        </div>
        <h3 style="margin:8px;"><?=$lang['Company_profile'];?></h3>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Company_profile'];?> : </label>
            <label class="form-filed">
            	<?php
				$select_company_profile=mysqli_query($conn,"select * from tbl_company where com_id=$com_id");
				$row_company=mysqli_fetch_array($select_company_profile);
				?>
            	<input type="text" class="inputstyle w350" disabled="disabled" value="<?=$row_company['com_name'];?>">
            </label>
        </div><div class="user-form-item clear">
            <label style="margin-top:5px;"></label>
            <label class="form-filed"><a href="/employer/company_profile.php">Edit</a></label>
        </div>
        <h3 style="margin:8px;"><?= $lang['Job_Description']; ?></h3>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Description'];?> : </label>
            <label class="form-filed">
            	<textarea name="desc" class="w451"><?=$row_job['job_desc']?></textarea>
            </label>
        </div>
        <h3 style="margin:8px;"> <?= $lang['Job_Requirements'];?> </h3>
        
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Job_Requirements'];?> : </label>
            <label class="form-filed">
            	<textarea name="job_require" class="w451"  required><?=$row_job['job_require']?></textarea>
            </label>
        </div>
        <h3 style="margin:8px;"><?= $lang['How_to_Apply'];?></h3>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['How_to_Apply'];?> : </label>
            <label class="form-filed">
            	<textarea name="apply" class="w451" required><?=$row_job['how_to_apply']?></textarea>
            </label>
        </div>
         <h3 style="margin:8px;"><?= $lang['Contact_Infomation'];?></h3>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Contact_person']; ?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="Contact_person" name="contact_person" maxlength="50" value="<?=$row_job['contact_person'];?>" required><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Phone']; ?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="phone" name="phone" value="<?=$row_job['phone'];?>" maxlength="30" required><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Email']; ?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="email" name="email" value="<?=$row_job['email'];?>" maxlength="30" required><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Website']; ?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="website" name="website" value="<?=$row_job['web'];?>" maxlength="30" required><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        <div class="user-form-item clear">
            <label style="margin-top:5px;"><?= $lang['Address']; ?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w350" id="address" name="address" value="<?=$row_job['address'];?>" required><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
            </label>
        </div>
        
        <div id="sumbitdiv" class="paddingleft form-submit-btn" style="padding-bottom:10px;">
            <label class="form-button">
                <button name="btcomsave"  class="btn btn-orange p30" type="submit"> <?= $lang['Save'];?> </button>
            </label>
        </div> 
    </div>
</form>