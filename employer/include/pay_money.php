<div class="pay_money">
<?php
$select_package=mysqli_query($conn,"select * from tbl_package where pk_id=$packageId");
?>
<table class="main-job-list-tab" border="0">
         <tr>
            <th style="width: 100px;text-align:center;"><?=$lang["Package_Name"];?></th> 
            <th style="width: 120px; text-align:center;"><?=$lang["Duration_Post"];?></th>
            <th style="width: 80px; text-align:center;"><?=$lang["POST"];?></th>
            <th style="width: 80px; text-align:center;"><?=$lang["Price"];?></th>
            <th style="width: 250px; text-align:center;"><?=$lang["Description"];?></th>
        </tr>
        <?php
		$row_package=mysqli_fetch_array($select_package)
		?>
        </tr>
            <td><?=$row_package['pk_name'];?></td>
            <td><?=$row_package['pk_duration'];?> days</td>
            <td><?=$row_package['pk_post'];?></td>
            <td>$ <?=$row_package['pk_price'];?></td>
            <td><?=$row_package['pk_desc'];?></td>
        </tr>
</table>
</div>

<!-- form payment -->
<div style="height:10px;width:100px;"></div>
<form method="post" enctype="multipart/form-data">
    <div class="register" style="border-radius:0px; border-top:0px;">
        <div class="emp_header"><h3 style="border-radius:0px;"><?=$lang['payment_form'];?></h3></div>
        <div class="content_register">
            <div class="form-item">
                <label><?=$lang['pay_by']?> : </label>
                <label class="form-filed"><input  type="text" class="inputstyle w200" id="user_pay" name="user_pay" maxlength="30" required="required" ></label>
            </div>
            <div class="form-item">
                <label><?=$lang['pin_code']?> : </label>
                <label class="form-filed"><input type="text" class="inputstyle w200" id="pin_code" name="pin_code" maxlength="30" required="required" ></label>
            </div>
            <div class="form-item">
                <label><?=$lang['Transfer_from']?> : </label>
                <label class="form-filed">
                	<select name="t_from" class="inputstyle w210" required>
                    	<option value="">Please Select</option>
                    	<option value="Wing">Wing</option>
                    	<option value="E-Money">EMoney</option>
                    	<option value="AMK">AMK</option>
                    	<option value="True Money">True Money</option>
                    </select>
                </label>
            </div>
            <div class="form-item">
                <label><?=$lang['Amount']?>($) : </label>
                <label class="form-filed"><input type="text" class="inputstyle w200" id="amount" name="amount" maxlength="10" required="required" ></label>
            </div>
            <div class="form-item">
                <label><?=$lang['Receiver_number_phone']?> : </label>
                <label class="form-filed"><input  type="text" class="inputstyle w200" id="com_contact" name="com_contact" value="097 980 9697 / 086 639 267" maxlength="30" disabled="disabled" ></label>
            </div>
            <div class="form-item clear">
                <div id="sumbitdiv" class="paddingleft form-submit-btn">
                    <label class="form-button">
                        <button name="btcomsave"  class="btn btn-orange p30" type="submit"><?= $lang['Save'];?></button>
                    </label>
                </div>
            </div> 
        </div>
    </div>
</form>
<!-- end form payment -->