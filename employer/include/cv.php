<div class="browe-job">
    <ul id="tabs">
    	
        <li><a href="#" title="tab1"><?= $lang['Section']; ?></a></li>
        <li><a href="#" title="tab2"><?= $lang['Location']; ?></a></li>
        <li><a href="#" title="tab3"><?= $lang['Salary']; ?></a></li> 
        <li style="float:left;padding-left:10px; margin-top:10px;"><label><strong><?= $lang['Browse_cv']; ?></strong></label></li>   
    </ul>
    
    <div id="content"> 
       <div id="tab1">
        <?php
		$select_browse=mysqli_query($conn,"select * from tbl_function");
		?>
            <ul>	
            	<?php
				while($row_browse=mysqli_fetch_array($select_browse)){
					$func_id=$row_browse['func_id'];
					$select_func=mysqli_query($conn,"select section from tbl_cv where section=$func_id");
					$count_func=mysqli_num_rows($select_func);
				?>
                <li><a href="section_cv.php?section_cvId=<?=$row_browse['func_id']?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row_browse['func_name_en'];else echo $row_browse['func_name_kh'];?></a><span> (<?=$count_func;?>)</span></li>
                <?php
				}
				?>
            </ul>
        </div>
        <div id="tab2">
			<?php
            $select_locate=mysqli_query($conn,"select * from tbl_province");
            ?>
            <ul>	
                <?php
				while($row_locate=mysqli_fetch_array($select_locate)){
					$locate_id=$row_locate['prov_id'];
					$select_func=mysqli_query($conn,"select province_id from tbl_cv_local where province_id=$locate_id");
					$count_func=mysqli_num_rows($select_func);
				?>
                <li><a target="_blank" href="location_cv.php?location_cvId=<?=$row_locate['prov_id'];?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row_locate['prov_name_en'];else echo $row_locate['prov_name_kh'];?></a><span> (<?=$count_func;?>)</span></li>
                <?php
				}
				?>

            </ul>
        </div>
        <div id="tab3">
            <ul><?php
					$select_200=mysqli_query($conn,"select salary from tbl_cv where salary='200'");
					$count_200=mysqli_num_rows($select_200);
				?>
                <li><a target="_blank" href="salary.php?salary=200"><200</a><a href="#"><span> (<?=$count_200;?>)</span></a></li>
                <?php
					$select_200_500=mysqli_query($conn,"select salary from tbl_cv where salary='200-500'");
					$count_200_500=mysqli_num_rows($select_200_500);
				?>
                <li><a  target="_blank" href="salary.php?salary=200-500">200-500</a><a href="#"><span> (<?=$count_200_500;?>)</span></a></li>
                
                <?php
					$select_500_1000=mysqli_query($conn,"select salary from tbl_cv where salary='500-1000'");
					$count_500_1000=mysqli_num_rows($select_500_1000);
				?>
                <li><a  target="_blank" href="salary.php?salary=500-1000">500-1000</a><a href="#"><span> (<?=$count_500_1000;?>)</span></a></li>
                
                <?php
					$select_1000_2000=mysqli_query($conn,"select salary from tbl_cv where salary='1000_2000'");
					$count_1000_2000=mysqli_num_rows($select_1000_2000);
				?>
                <li><a  target="_blank" href="salary.php?salary=1000-2000">1000-2000</a><a href="#"><span> (<?=$count_1000_2000;?>)</span></a></li>
                
                <?php
					$select_2000=mysqli_query($conn,"select salary from tbl_cv where salary='2000'");
					$count_2000=mysqli_num_rows($select_2000);
				?>
                <li><a  target="_blank" href="salary.php?salary=2000">>2000</a><a href="#"><span> (<?=$count_2000;?>)</span></a></li>

            </ul>
        </div>
    </div>

</div>