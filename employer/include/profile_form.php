<?php
$com_id = $_SESSION['com_id'];

function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }

    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}
    $upload_dir = "images/"; 
            
    if (isset($_POST['btcomsave'])) {
        
        $ind_id = $_POST['ind_id'];
        $type_id = $_POST['type_id'];
        $prov_id = $_POST['prov_id'];
        $employee= $_POST['com_emp'];
        $com_name= $_POST['com_name'];
        $contact_per= $_POST['com_contact'];
        $contact_fax= $_POST['com_fax'];
        $contact_web= $_POST['com_website'];
        $contact_addr= $_POST['com_addr'];
        $contact_desc= $_POST['com_desc'];
    
        if (file_exists($_FILES['file']['tmp_name'])){
            $check=mysqli_query($conn,"SELECT com_image FROM tbl_company WHERE com_id=$com_id");
                $url_row=mysqli_fetch_array($check);
					if($url_row['com_image']!=""){
                    	unlink("images/".$url_row['com_image']);
					}
                
            $image = $_FILES['file']['name'];
            $tempext = getExtension($image);
            $extfile = mt_rand() . "." . $tempext;
            //===function random name
            move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir . $extfile);
            //echo "Uploaded File :" . $_FILES["myfile"]["name"];
            $url=$extfile;      
        }
        if($_FILES['file']['tmp_name'] !=NULL){
            mysqli_query($conn,'SET NAMES utf8');   
            mysqli_query($conn,"update tbl_company set `ind_id`=$ind_id,`type_id`=$type_id,`prov_id`=$prov_id,`com_emp`='$employee',`com_name`='$com_name',`com_contact`='$contact_per',`com_fax`='$contact_fax',`com_website`='$contact_web',`com_addr`='$contact_addr',`com_desc`='$contact_desc',`com_image`='$url' where com_id=$com_id") or die(mysql_error());
        }else{
            mysqli_query($conn,'SET NAMES utf8');
            mysqli_query($conn,"update tbl_company set `ind_id`=$ind_id,`type_id`=$type_id,`prov_id`=$prov_id,`com_emp`='$employee',`com_name`='$com_name',`com_contact`='$contact_per',`com_fax`='$contact_fax',`com_website`='$contact_web',`com_addr`='$contact_addr',`com_desc`='$contact_desc' where com_id=$com_id") or die(mysql_error());
        }
        ?>
        <script type="text/javascript">
            window.location.href="../employer/company_profile.php";
        </script>
        <?php   
    }   
?> 
<?php
	$sql=mysqli_query($conn,"select * from tbl_company where com_id=$com_id");
	mysqli_query($conn,'SET NAMES utf8');
	while($row2=mysqli_fetch_array($sql)){
?>
<form method="post" enctype="multipart/form-data">
<div class="register">
	<div class="emp_header"><h3><?= $lang['Company_profile'];?></h3></div>
    <div class="content_register">
        	<div class="form-item">
                <label>ID : </label>
                <label class="form-filed"><input type="text" class="inputstyle w200" value="<?php echo $_SESSION['com_id']; ?>" style="border:0px;" readonly="readonly"/></label>
            </div>
            <div class="form-item">
            	<label><?= $lang['Company'];?> : </label>
                <label class="form-filed"><input  type="text" class="inputstyle w200" id="com_name" name="com_name" maxlength="30" validmessage="*" value="<?php echo $row2['com_name']; ?>" ><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
                </label>
            </div>
            <div class="form-item">
            	<label><?= $lang['Logo'];?> : </label>
                <label class="form-filed"><input type="file"  id="file" name="file" >
                </label>
            </div>
            <?php
				if($row2['com_image']!=""){
			?>
            <div class="form-item">
            	<label>&nbsp;</label>
                <label class="form-filed"><img src="images/<?= $row2['com_image'];?>" style=" height:80px; border:1px solid #CCC;"/>
                </label>
            </div>
            <?php
			}
			?>
            <div class="form-item">
            	<label><?= $lang['Type'];?> : </label>
                <label class="form-filed">
               <select class="inputstyle w210" name="type_id" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <?php 
                            $sql=mysqli_query($conn,"select * from tbl_company_type");
                            while($row=mysqli_fetch_array($sql)){
                                $type_name = $row['type_name_en'];
                        	if($row['type_id']== $row2['type_id'])
								$select="selected";
							else
								$select="";
						?>  
                           <option value="<?php echo $row['type_id'];?>" <?php echo $select;?>><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row['type_name_en'];else echo $row['type_name_kh'];?></option>
                       <?php } ?>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
                </label>
            </div>
            <div class="form-item">
            	<label><?= $lang['Industry'];?> : </label>
                <label class="form-filed">
               <select class="inputstyle w210" name="ind_id" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <?php 
                            $sql=mysqli_query($conn,"select * from tbl_industry");
                            while($row=mysqli_fetch_array($sql)){
                                $m_name = $row['ind_name_en'];
                        		if($row['ind_id']== $row2['ind_id'])
									$select="selected";
								else
									$select="";
                          ?>  
                          <option value="<?php echo $row['ind_id'];?>" <?php echo $select;?>><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row['ind_name_en'];else echo $row['ind_name_kh'];?></option>
                       <?php } ?>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
                </label>
            </div>
            <div class="form-item">
            	<label><?= $lang['Employees'];?> : </label>
                <label class="form-filed">
               <select class="inputstyle w210" name="com_emp" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <option <?php if($row2['com_emp']=='10'){echo "selected='selected'";}?> value="10"><10</option>
                        <option  <?php if($row2['com_emp']=='10-50'){echo "selected='selected'";}?> value="10-50">10-50</option>
                        <option  <?php if($row2['com_emp']=='50-100'){echo "selected='selected'";}?> value="50-100">50-100</option>
                        <option  <?php if($row2['com_emp']=='100-200'){echo "selected='selected'";}?> value="100-200">100-200</option>
                        <option  <?php if($row2['com_emp']=='200'){echo "selected='selected'";}?> value="200">>200</option>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
                </label>
            </div>
            <div class="form-item">
            	<label>Location : </label>
                <label class="form-filed">
               <select class="inputstyle w210" name="prov_id" required>
                        <option value=""><?= $lang['Please_Select'];?></option>
                        <?php 
                            $sql=mysqli_query($conn,"select * from tbl_province");
                            while($row=mysqli_fetch_array($sql)){
                                $prov_name = $row['prov_name_en'];
                        		if($row['prov_id']== $row2['prov_id'])
									$select="selected";
								else
									$select="";
						?>  
                            <option value="<?php echo $row['prov_id'];?>" <?php echo $select;?>><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row['prov_name_en'];else echo $row['prov_name_kh'];?></option>
                       <?php } ?>
                </select><span class="valid_info"><i style="color: red;">*</i></span>
                </label>
            </div>
            <div class="form-item">
                <label><?= $lang['Address'];?> : </label>
                <label class="form-filed"><textarea name="com_addr" class="w450"><?php echo $row2['com_addr'];?></textarea>
                </label>
            </div>
            <div class="form-item">
                <label><?= $lang['Description'];?> : </label>
                <label class="form-filed"><textarea name="com_desc" class="w451"><?php echo $row2['com_desc'];?></textarea>
                </label>
            </div>  
    </div>
            
</div>
<div class="register" style="border-radius:0px; border-top:0px;">
 	<div class="emp_header"><h3 style="border-radius:0px;"><?= $lang['Contact_Infomation'];?></h3></div>
    <div class="content_register">
        <div class="form-item">
            <label><?= $lang['Contact_Name'];?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w200" id="com_contact" name="com_contact" maxlength="30" value="<?php echo $row2['com_contact'];?>"></label>
        </div>
         <div class="form-item">
                <label><?= $lang['Website'];?> : </label>
                <label class="form-filed"><input  type="text" class="inputstyle w200" id="com_website" name="com_website" maxlength="30" value="<?php echo $row2['com_website'];?>">
                </label>
        </div>
        <div class="form-item">
            <label><?= $lang['Fax'];?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w200" id="com_fax" name="com_fax" maxlength="30" value="<?php echo $row2['com_fax'];?>"></label>
        </div>
        <div class="form-item">
            <label><?= $lang['Phone'];?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w201" id="com_phone" name="com_phone" maxlength="30" value="<?php echo $row2['com_phone'];?>" readonly="readonly"></label>
        </div>
        <div class="form-item">
            <label><?= $lang['Email'];?> : </label>
            <label class="form-filed"><input  type="text" class="inputstyle w201" id="com_email" name="com_email" maxlength="30" value="<?php echo $row2['com_email'];?>" readonly="readonly"></label>
        </div>
        <div class="form-item clear">
            <div id="sumbitdiv" class="paddingleft form-submit-btn">
                <label class="form-button">
                    <button name="btcomsave"  class="btn btn-orange p30" type="submit"><?= $lang['Save'];?></button>
                </label>
            </div>
        </div> 
    </div>
</div>
</form>
<?php } ?>