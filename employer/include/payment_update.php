<!-- form payment -->
<div style="height:10px;width:100px;"></div>
<form method="post" enctype="multipart/form-data">
    <div class="register" style="border-radius:0px; border-top:0px;">
        <div class="emp_header"><h3 style="border-radius:0px;"><?=$lang['payment_form'];?></h3></div>
        <?php
		$select_user_get_package=mysqli_query($conn,"select * from tbl_payment where pm_id=$packageId");
		$row_user_get_package=mysqli_fetch_array($select_user_get_package);
		?>
        <div class="content_register">
            <div class="form-item">
                <label><?=$lang['pay_by']?> : </label>
                <label class="form-filed"><input  type="text" class="inputstyle w200" id="user_pay" name="user_pay" maxlength="30" required="required" value="<?=$row_user_get_package['pm_from'];?>" ></label>
            </div>
            <div class="form-item">
                <label><?=$lang['pin_code']?>: </label>
                <label class="form-filed"><input type="text" class="inputstyle w200" id="pin_code" name="pin_code" maxlength="30" required="required" value="<?=$row_user_get_package['pm_code'];?>" ></label>
            </div>
            <div class="form-item">
                <label><?=$lang['Transfer_from']?> : </label>
                <label class="form-filed">
                	<select name="t_from" class="inputstyle w210" required>
                    	<option value="">Please Select</option>
                    	<option <?php if($row_user_get_package['pm_com']=='Wing'){echo'selected="selected"';}?> value="Wing">Wing</option>
                    	<option <?php if($row_user_get_package['pm_com']=='E-Money'){echo'selected="selected"';}?> value="E-Money">EMoney</option>
                    	<option <?php if($row_user_get_package['pm_com']=='AMK'){echo'selected="selected"';}?> value="AMK">AMK</option>
                    	<option <?php if($row_user_get_package['pm_com']=='True Money'){echo'selected="selected"';}?> value="True Money">True Money</option>
                    </select>
                </label>
            </div>
            <div class="form-item">
                <label><?=$lang['Amount']?>($) : </label>
                <label class="form-filed"><input type="text" class="inputstyle w200" id="amount" name="amount" value="<?=$row_user_get_package['pm_amount'];?>"" maxlength="10" required="required" ></label>
            </div>
            <div class="form-item">
                <label><?=$lang['Receiver_number_phone']?> : </label>
                <label class="form-filed"><input  type="text" class="inputstyle w200" id="com_contact" name="com_contact" value="097 980 9697 / 086 639 267" maxlength="30" disabled="disabled" ></label>
            </div>
            <div class="form-item clear">
                <div id="sumbitdiv" class="paddingleft form-submit-btn">
                    <label class="form-button">
                        <button name="btcomsave"  class="btn btn-orange p30" type="submit"><?= $lang['Save'];?></button>
                    </label>
                </div>
            </div> 
        </div>
    </div>
</form>
<!-- end form payment -->