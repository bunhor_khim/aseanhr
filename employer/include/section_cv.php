<!-- page next-- -->
<?php
$page_name = "/section_cv.php?section_cvId=$select_id"; // If you use this code with a different page ( or file ) name then change this

if (isset ( $_GET ['start'] )) {
	
	$start = $_GET ['start'];
} else {
	
	$start = "";
}
if (strlen ( $start ) > 0 and ! is_numeric ( $start )) {
	echo "Data Error";
	exit ();
}

$eu = ($start - 0);
$limit = 30; // No of records to be shown per page.
$this1 = $eu + $limit;
$back = $eu - $limit;
$next = $eu + $limit;
$query2="select * from tbl_cv where section=$select_id order by job_seeker_id desc";
							
$result2 = mysqli_query ( $conn, $query2 );
echo mysqli_error ( $conn );
$nume = mysqli_num_rows ( $result2 );

?>

<div class="category_job_list">
    <table class="main-job-list-tab" border="0">
         <tr>
            <th><?= $lang['position'];?></th> 
            <th style="width: 120px; text-align:left;"><?= $lang['Location'];?></th>
            <th style="width: 120px; text-align:left;"><?= $lang['Salary'];?></th>
            <th style="width: 120px; text-align:center;"><?= $lang['Action'];?></th>
        </tr>
        <?php
		$select_cate=mysqli_query($conn,"select * from tbl_cv where section=$select_id order by job_seeker_id desc limit $eu, $limit");
		while($row_cate=mysqli_fetch_array($select_cate)){
		?>
        
        <tr>
            <td>
                <?=$row_cate['position'];?>
            </td> 
            <td align="left">
            	<?=$row_cate['local'];?>
                <?php //if(isset($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row2['prov_name_en'];else echo $row2['prov_name_kh'];?>
            </td>
            <td align="left">
                <strong><?=$row_cate['salary'];?></strong>
            </td>
            <td nowrap="nowrap" class="action" align="center">
                <a target="_blank" title="Edit" href="preview.php?job_seeker_cvId=<?=$row_cate['job_seeker_id'];?>">Preview</a>
            </td>
        </tr>
        <?php
		}
		?>
    </table>
</div>

<!-- pagination -->
<div style="text-align: center;">
	<ul class="fancy pagination">
    <?php
												
	if ($back >= 0) {
		print "<li><a href='$page_name&start=$back'>«</a></li>";
	}
	
	$i = 0;
	$l = 1;
	for($i = 0; $i < $nume; $i = $i + $limit) {
		
		if ($i != $eu) {
			echo " <li><a href='$page_name&start=$i'>$l</a></li> ";
		} else {
			
			if ($nume < $limit) {
			} else {
				echo "<li class='active'><a href='#'>$l</a></li>";
			}
		} // / Current page is not displayed as link and given font color red
		
		$l = $l + 1;
	}
													
	if ($this1 < $nume) {
		print "<li><a href='$page_name&start=$next'>»</a></li>";
	}
	?>    
    </ul>
</div>
<!-- pagination -->