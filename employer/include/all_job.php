<!-- page next-- -->
<?php
$page_name = "/employer/"; // If you use this code with a different page ( or file ) name then change this

if (isset ( $_GET ['start'] )) {
	
	$start = $_GET ['start'];
} else {
	
	$start = "";
}
if (strlen ( $start ) > 0 and ! is_numeric ( $start )) {
	echo "Data Error";
	exit ();
}

$eu = ($start - 0);
$limit = 10; // No of records to be shown per page.
$this1 = $eu + $limit;
$back = $eu - $limit;
$next = $eu + $limit;
$query2="select tbl_jobs.user_enable,tbl_jobs.job_title,tbl_jobs.job_close_date,
								tbl_jobs.job_id,tbl_province.prov_name_en,tbl_province.prov_name_kh,
								tbl_function.func_name_en,tbl_function.func_name_kh
							from tbl_jobs
								inner join tbl_province on tbl_jobs.prov_id=tbl_province.prov_id
								inner join tbl_function on tbl_jobs.func_id=tbl_function.func_id
 							where tbl_jobs.com_id=$com_id order by job_id desc";
							
$result2 = mysqli_query ( $conn, $query2 );
echo mysqli_error ( $conn );
$nume = mysqli_num_rows ( $result2 );

?>

<div class="category_job_list">
    <table class="main-job-list-tab" border="0">
         <tr>
            <th><?= $lang['Job_Title'];?></th> 
            <th style="width: 120px; text-align:center;"><?= $lang['Location'];?></th>
            <th style="width: 120px; text-align:center;"><?= $lang['Closing_Date'];?></th>
            <th style="width: 100px; text-align:center;"><?= $lang['Industry'];?></th>
            <th style="width: 120px; text-align:center;"><?= $lang['Action'];?></th>
        </tr>
        <?php
		$select_cate=mysqli_query($conn,"select tbl_jobs.user_enable,tbl_jobs.job_title,tbl_jobs.job_close_date,
								tbl_jobs.job_id,tbl_province.prov_name_en,tbl_province.prov_name_kh,
								tbl_function.func_name_en,tbl_function.func_name_kh
							from tbl_jobs
								inner join tbl_province on tbl_jobs.prov_id=tbl_province.prov_id
								inner join tbl_function on tbl_jobs.func_id=tbl_function.func_id
 							where tbl_jobs.com_id=$com_id order by job_id desc limit $eu, $limit");
		while($row2=mysqli_fetch_array($select_cate)){
		?>
			<tr>
				<td>
					<a title="View" target="_blank" href="http://<?=$_SERVER["HTTP_HOST"];?>/job.php?jobId=<?=$row2['job_id'];?>"><?=$row2['job_title'];?></a>
				</td> 
				<td align="center">
					<?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row2['prov_name_kh'];else echo $row2['prov_name_en'];?>
				</td>
				<td align="center">
					<strong><?php if($row2['job_close_date']=='0000-00-00'){}else{?><?=getDateFormat($row2['job_close_date']);?><?php }?></strong>
				</td>
				<td align="center">
					<?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row2['func_name_kh'];else echo $row2['func_name_en'];?> 
				</td>
				<td nowrap="nowrap" class="action" align="center">
                <?php
				if($row2['user_enable']==0){
				$count_payment=mysqli_num_rows(mysqli_query($conn,"select * from tbl_payment where com_id=$com_id"));
				if($count_payment<=0){
				?>
                <a title="Public" target="_blank" href="/employer/package.php">Public</a>  |
                <?php
				}else{
				?>
                	<a title="Public" target="_blank" href="/employer/my_package.php?jobId=<?=$row2['job_id'];?>">Public</a>  |
					<?php
				}
				}
					?>
					<a title="Edit" href="edit_job.php?job_id=<?=$row2['job_id'];?>"><img src="images/edit.jpg" /></a><!--  |
					<a title="Delete" href="javascript:delete_id(<?php //echo $row2['job_id']; ?>)"><img src="images/b_drop.png"/></a>-->
				</td>
			</tr>
		<?php
		}
		?>
    </table>
</div>

<!-- pagination -->
<div style="text-align: center;">
	<ul class="fancy pagination">
    <?php
												
	if ($back >= 0) {
		print "<li><a href='$page_name?start=$back'>«</a></li>";
	}
	
	$i = 0;
	$l = 1;
	for($i = 0; $i < $nume; $i = $i + $limit) {
		
		if ($i != $eu) {
			echo " <li><a href='$page_name?start=$i'>$l</a></li> ";
		} else {
			
			if ($nume < $limit) {
			} else {
				echo "<li class='active'><a href='#'>$l</a></li>";
			}
		} // / Current page is not displayed as link and given font color red
		
		$l = $l + 1;
	}
													
	if ($this1 < $nume) {
		print "<li><a href='$page_name?start=$next'>»</a></li>";
	}
	?>    
    </ul>
</div>
<!-- pagination -->