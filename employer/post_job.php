<?php session_start();
require_once("../_aseanhr_admin/_config/connect.php");
include '../langauge/common.php';
if($page=="logout"){
  session_destroy();
  ?>
  <script type="text/javascript">
        window.location.href="../employer/";
  </script>
<?php   
}

if(isset($_SESSION['login_employer']) && $_SESSION['login_employer']){
	$userlog=$_SESSION['login_employer'];
	$emp_name=$_SESSION['emp_name'];
	$com_id = $_SESSION['com_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../images/icon.png" />
<title>ASEANHR - <?= $lang['post_jobs']; ?></title>
<link href="../css/style1.css" rel="stylesheet" type="text/css" />
<link href="../css/color.css" rel="stylesheet" type="text/css"  />
<link href="css/css_styles.css" rel="stylesheet" type="text/css"  />
<!-- jquery tab -->
<script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/com_script.js"></script>
<script type="text/javascript" src="js/css_script.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<!-- date picker -->
<link rel="stylesheet" href="css/BeatPicker.min.css"/>
<script src="js/BeatPicker.min.js"></script>
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="../js/engine1/style.css" />
<!-- End WOWSlider.com HEAD section -->
</head>

<body  id="home">
<!-- alert message -->
<div class="err warning" id="add_err">
</div>
<div class="err success" id="err_success">
</div>
<!-- end alert message -->
<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('include/header.php')?>
        </div>
        <div class="menu">
        	<?php include('../includes/menu.php');?>
        </div>
        
        <div class="emp_container">
        <!-- main -->
            <div class="emp_main">
            	<?php 
					include('include/job_form.php');
				?>
            </div>
        <!-- end main -->
        <div class="side">
        	    <?php include('include/emp_right.php');?>	
        </div>
        <!-- footer -->
        <div class="fonter-info-bg" style="margin-top:10px;">
            <?php include("../includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('../includes/copy-right.php');?>
</div>
<script type="text/javascript" src="../js/engine1/wowslider.js"></script>
<script type="text/javascript" src="../js/engine1/script.js"></script>
</body>
</html>
<?php
    }else{
?>
    <script type="text/javascript">
        window.location.href="../employer/";
    </script>
<?php
    }
?>