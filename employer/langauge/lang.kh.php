<?php
/* 
-----------------
Language: German
-----------------
*/

$lang = array();

$lang['Edit']='កែប្រែ';
$lang['welcome']="សូមស្វាគមន៍";
// Menu
$lang['MENU_HOME'] = 'ទំព័រដើម';
$lang['MENU_ABOUT_US'] = 'អំពីយើងខ្ញុំ';
$lang['Our_Services']= "សេវាកម្មយើង";
$lang['MENU_CONTACT'] = 'ទំនាក់ទំនងយើងខ្ញុំ';
$lang['Job_seeker']='អ្នកសែ្វងរកការងារ';
$lang['Job_learning']='ការរៀនការងារ';

//post jobs
$lang['post_jobs']='ផ្សព្វផ្សាយការងារ ';
$lang['posts_jobs']='ការងារ';
$lang['All_Jobs']='ការងារទាំងអស់';
$lang['new_Jobs']='ផ្សព្វផ្សាយការងារថ្មី';

$lang['account']="អំពីគណនី";
$lang['change_pass']="ប្តូរលេខសំងាត់";
$lang['Logout']="ចាកចេញ";

$lang['Company_profile']='ប្រវត្តិក្រុមហ៊ុន';
$lang['Job_Description']="ពណ៌នាការងារ";
$lang['Company']='ក្រុមហ៊ុន';
$lang['Industry']='វិស័យ';
$lang['Category']="ប្រភេទ";
$lang['Type']='ប្រភេទក្រុមហ៊ុន';
$lang['Employees']='ចំនួននិយោជិក';
$lang['Location']='ទីតាំង';
$lang['Website']='គេហទំព័រ';
$lang['Address']='អាសយដា្ឋន';
$lang['Company_Description']='ពិព័ណនាពីក្រុមហ៊ុន';
$lang['Publish_Date']='កាលបរិច្ឆទចុះផ្សាយ';
$lang['Closing_Date']='ថ្ងៃផុតកំណត់';
$lang['Hiring']='ចំនួនជ្រើសរើស';
$lang['Function']='មុខងារ';
$lang['Salary']='បា្រក់ខែ';
$lang['Description']='ការពិពណ័នា';
$lang['Job_Level']='កម្រិតការងារ';
$lang['job_alert']='ជូនដំណឹងការងារ';
$lang['new']='ការងារថ្មី';
$lang['Urgent']='ការងារបន្ទាន់';

// querlification
$lang['Qualification']='កម្រិតវប្បធម៌';
$lang['High_School']='វិទ្យាល័យ';
$lang['Associate_Degree']="ថ្នាក់បរិញ្ញាបត្ររង";
$lang['Bachelor_Degree']='ថ្នាក់បរិញ្ញាបត្រ';
$lang['Master_Degree']='ថ្នាក់អនុបណ្ទិត';
$lang['Doctor_Degree']='ថ្នាក់បណ្ឌិត';
$lang['Others']='ផ្សេងៗ';

$lang['Job_Term']='ប្រភេទការងារ';
$lang['Year_of_Exp']='បទពិសោធន៍​ (ឆ្នំា)';
$lang['Age']='អាយុ';
$lang['Sex']='ភេទ';
$lang['Job_Requirements']='តម្រូវការការងារ';
$lang['How_to_Apply']='របៀបក្នុងការដាក់ពាក្យ';
$lang['Save']='រក្សាទុក';

$lang['Please_Select']="សូមជ្រើសរើស";
$lang['Negotiable']="តម្លៃចចារបាន";

// company profiles
$lang['Logo']="ស្លាកសញ្ញា";
$lang['Contact_Infomation']="ពត៏មានទំនាក់ទំនង";
$lang['Contact_Name']="ឈ្មោះអ្នកទំនាក់ទំនង";
$lang['Fax']="ទូរសារ";
$lang['Phone']="ទូរស័ព្ទ";
$lang['Email']="អ៊ីម៉ែល";
$lang['Contact_person']="ឈ្មោះអ្នកទំនាក់ទំនង";

// change password
$lang['Employer_Change_Password']="ប្តូរលេខសំងាត់";
$lang['New_Password']="លេខសម្ងាត់ថ្មី";
$lang['Confirm_Password']="បំពេញលេខសម្ងាត់ថ្មីម្តងទៀត";

// all jobs
$lang['Job_Title']="មុខតំណែង ";
$lang['Action']="សកម្មភាព";

//Edit jobs
$lang['edit_jobs']="កែប្រែការងារ";

$lang['employer']="និយោជក";
$lang['Profile']="ព័ត៌មានផ្ទាល់ខ្លួន";
$lang['Create_CV']="បង្កើតជីវប្រវត្តិ";

$lang['hotline']="លេខទូរស័ព្ទទំនាក់ទំនងបន្ទាន់";
$lang['em']="អ៊ីមែល";
$lang['tels']="ទូរស័ព្ទ";
$lang['Hours']="ម៉ោងធើ្វការ";
$lang['addr']="អាសយដ្ឋាន";

// cv search
$lang['cv_search']="ស្វែងរក CV";
$lang['Browse_cv']="ស្វែងរក CV";
$lang['Section']="ផ្នែក";
$lang['position']="មុខដំណែង";
$lang['field_of_study']="មុខវិជ្ជាសិក្សា";


// language
$lang['Poor']="ខ្សោយ";
$lang['Fair']="មធ្យម";
$lang['Good']="ល្អ";
$lang['Excellent']="ល្អណាស់";
$lang['language']="ភាសា";

// job term
$lang['full_time']="ពេញម៉ោង";
$lang['Part_time']="ក្រៅម៉ោង";
$lang['Internship']="កម្មសិក្សា";
$lang['Volunteer']="ស្មគ្រ័ចិត្ត";
$lang['Urgent']="បន្ទាន់";


//register
$lang['Employer_Register']="ចុះឈ្មោះ និយោជក";
$lang['uname']="ឈ្មោះ";
$lang['Company_Name']="ឈ្មោះក្រុមហ៊ុន";
$lang['a']="៤~៣០ អក្សរ និង​លេខ";
$lang['b']="ប្រវែងគួតែវែងជាង​៦តួ​";
$lang['c']="ប្រសិនអ៊ីម៉ែលអ្នកមិនត្រឹមត្រូវ អ្នកមិនអាច សែ្វងរកលេខសម្ងាត់ ពេលភ្លេចបានទេ";
$lang['agreement']="ខ្ញុំបានអាននិង យល់ព្រមពីល័ក្ខខ័ណ្ឌនៃសេវាកម្ម។";
$lang['Register']="ចុះឈ្មោះ";
$lang['password']="លេខសំងាត់";
$lang['conpassword']="បញ្ជាក់លេខសម្ងាត់";

//forget password
$lang['forget']="ស្នើរសុំលេខសំងាត់ថ្មី";
$lang['send']="ផ្ញើរអ៊ីមែល";

// form pay money
$lang['Quanlity_Post']="គុណភាពប្រកាសផ្សាយ";
$lang['Duration_post']="អំឡុងពេលប្រកាសផ្សាយ";
$lang['Fee']="ថ្លៃសេវា";
$lang['How_to_pay']="របៀបបង់ប្រាក់";
$lang['Pay_Througth_Company']="Pay Througth Company";
$lang['amount_pay']="Amount Pay";
$lang['Receiver_number_phone']="លេខទូរស័ព្ទអ្នកទទួល";
$lang['pin_code']="PIN Code";
$lang['close_date_label']="ការផុតកំណត់នឹងត្រូវបានបង្កើតដោយឯកឯង បន្ទាប់ពីការងាររបស់អ្នកបានpublished​ រួច";

$lang['Package_Name']="ឈ្មោះកញ្ចប់សេវាកម្ម";
$lang['Duration_Post']="ពេលវេលាប្រកាសផ្សាយ";
$lang['POST']="ចំនួនប្រកាសផ្សាយ";
$lang['Price']="តំលៃ";
$lang['Description']="ការពិពណ៏នា";
$lang['payment_form']="សំណុំបែបបទការបង់ប្រាក់";

// user payment
$lang['pay_by']="ឈ្មោះអ្នកផ្ញើរ";
$lang['Transfer_from']="ផ្ញើរតាម";
$lang['Amount']="ចំនួនទឹកប្រាក់";
?>