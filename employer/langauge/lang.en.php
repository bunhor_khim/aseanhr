<?php
/* 
-----------------
Language: German
-----------------
*/

$lang = array();

$lang['Edit']='Edit';
$lang['welcome']="Welcome to";
// Menu

$lang['MENU_HOME'] = 'Home';
$lang['MENU_ABOUT_US'] = 'About us';
$lang['Our_Services']= "Our Services";
$lang['MENU_CONTACT'] = 'Contact us';
$lang['Job_seeker']='Job Seeker';
$lang['Job_learning']='Job Learning';

//post jobs
$lang['post_jobs']='Post Job';
$lang['posts_jobs']='Jobs';
$lang['All_Jobs']='All Jobs';
$lang['new_Jobs']='Post a new Jobs';

$lang['account']="Account Setting";
$lang['change_pass']="Change Password";
$lang['Logout']="Logout";

$lang['Company_profile']='Company Profile';
$lang['Job_Description']="Job Description"; 
$lang['Company']='Company';
$lang['Industry']='Industry';
$lang['Type']='Type';
$lang['Employees']='Employees';
$lang['Location']='Location';
$lang['Website']='Website';
$lang['Address']='Address';
$lang['Company_Description']='Company Description';
$lang['Publish_Date']='Publish Date';
$lang['Closing_Date']='Closing Date';
$lang['Hiring']='Hiring';
$lang['Function']='Function';
$lang['Salary']='Salary';
$lang['Description']='Description';
$lang['Job_Level']='Job Level';
$lang['job_alert']='Job Alert';
$lang['new']='New';
$lang['Urgent']='Urgent';
// querlification
$lang['Qualification']='Qualification';
$lang['High_School']='High School​';
$lang['Associate_Degree']="Associate's Degree";
$lang['Bachelor_Degree']='Bachelor Degree';
$lang['Master_Degree']='Master Degree';
$lang['Doctor_Degree']='Doctor Degree';
$lang['Others']='Others';

$lang['Job_Term']='Job Term';
$lang['Year_of_Exp']='Year of Exp';
$lang['Age']='Age';
$lang['Sex']='Sex';
$lang['Job_Requirements']='Job Requirements';
$lang['How_to_Apply']='How to Apply';
$lang['Save']='Save';

$lang['Please_Select']="Please Select";
$lang['Negotiable']="Negotiable";


// company profiles
$lang['Logo']="Logo";
$lang['Address']="Address";
$lang['Contact_Infomation']="Contact Infomation";
$lang['Contact_Name']="Contact Name";
$lang['Fax']="Fax";
$lang['Phone']="Phone";
$lang['Email']="Email";
$lang['Contact_person']="Contact Person";

// change password
$lang['Employer_Change_Password']="Change Password";
$lang['New_Password']="New Password";
$lang['Confirm_Password']="Confirm Password";

// all jobs
$lang['Job_Title']="Job Title";
$lang['Category']="Category";
$lang['Action']="Action";

//Edit jobs
$lang['edit_jobs']="Edit_Jobs";
$lang['employer']="Employer";
$lang['Profile']="Profile";
$lang['Create_CV']="Create CV";
$lang['hotline']="Service hot line";
$lang['em']="Email";
$lang['tels']="Tel";
$lang['Hours']="Operating Hours";
$lang['addr']="Address";


// cv search
$lang['cv_search']="CV Search";
$lang['Browse_cv']="Browse CV";
$lang['Section']="Section";
$lang['position']="Position";
$lang['field_of_study']="Field of Study";

// language
$lang['Poor']="Poor";
$lang['Fair']="Fair";
$lang['Good']="Good";
$lang['Excellent']="Excellent";
$lang['language']="Language";

// job term
$lang['full_time']="Full Time";
$lang['Part_time']="Part Time";
$lang['Internship']="Internship";
$lang['Volunteer']="Volunteer";
$lang['Urgent']="Urgent";

//register
$lang['Employer_Register']="Employer Register";
$lang['uname']="Username";
$lang['Company_Name']="Company Name";
$lang['a']="4~30 letters and numbers";
$lang['b']="The length should be longer than 6";
$lang['c']="Without safe email, you can not reset your password if you forget it";
$lang['agreement']="I have read and agreed to the Terms of Service.";
$lang['Register']="Register";
$lang['password']="Password";
$lang['conpassword']="Confirm Password";

//Forget password
$lang['forget']="Forget Password";
$lang['send']="Send Email";

// form pay money
$lang['Quanlity_Post']="Quanlity Post";
$lang['Duration_post']="Duration Post";
$lang['Fee']="Fee";
$lang['How_to_pay']="How to pay";
$lang['Pay_Througth_Company']="Pay Througth Company";
$lang['amount_pay']="Amount Pay";
$lang['Receiver_number_phone']="Receiver Number Phone";
$lang['pin_code']="PIN Code ";
$lang['close_date_label']="The closing date will be generated automaticall after your job published";

$lang['Package_Name']="Package Name";
$lang['Duration_Post']="Duration Post";
$lang['POST']="POST";
$lang['Price']="Price";
$lang['Description']="Description";
$lang['payment_form']="Payment Form";

// user payment
$lang['pay_by']="Pay By";
$lang['Transfer_from']="Transfer From";
$lang['Amount']="Amount";
?>







