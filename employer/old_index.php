<?php
include '../langauge/common.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../images/icon.png" />
<title>Register</title>
<link href="../css/style1.css" rel="stylesheet" type="text/css" />
<link href="../css/color.css" rel="stylesheet" type="text/css"  />

<!-- jquery tab -->
<script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/emp_script.js"></script>
</head>

<body  id="home">
<!-- alert message -->
<div class="err warning" id="add_err">
</div>
<div class="err success" id="err_success">
</div>
<!-- end alert message -->
<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <div class="header-logo">
                <img src="../images/asean_logo.png" />
            </div>
            <div class="header-banner">
                <img src="../images/banner.gif" />
            </div>
        </div>
        <div class="menu">
        	<?php include('../includes/menu.php');?>
        </div>
        
        <div class="container">
        <!-- main -->
            <div class="main">
            	<?php include('include/emp_main.php');?>
            </div>
        <!-- end main -->
            <div class="side">
            	<?php include('include/emp_right_side.php'); ?>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("../includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <div class="footer">All Rights Reserved By Aseanhr Information(Cambodia) Co., LTD.</div>
</div>

</body>
</html>