<?php
session_start();
include("_aseanhr_admin/_config/connect.php");
include_once 'langauge/common.php';

if(isset($_GET['keyword']) && $_GET['keyword']){
	$keyword=$_GET['keyword'];
}else{
	$keyword="";
}
if(isset($_GET['category']) && $_GET['category']){
	$fun_id=$_GET['category'];
}else{
	$fun_id="";
}

if(isset($_GET['locationId']) && $_GET['locationId']){
	$locate_id=$_GET['locationId'];
}else{
	$locate_id="";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/icon.png" />
<title>ASEANHR -Search Result</title>
<link href="css/style1.css" rel="stylesheet" type="text/css" />
<link href="css/color.css" rel="stylesheet" type="text/css"  />
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="js/engine1/style.css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
<!-- wow slide2-->
<link rel="stylesheet" type="text/css" href="js/engine2/style.css" />
<script type="text/javascript" src="js/engine2/wowslider.js"></script>
<script type="text/javascript" src="js/engine2/script.js"></script>
<!-- wow slide2-->
</head>

<body id="home">

<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('includes/menu.php');?>
        </div>
        
        <div class="container">
        <!-- main -->
            <div class="main">
            	<?php include('includes/result_search.php');?>
            </div>
        <!-- end main -->
            <div class="side">
            	<?php include('includes/contact_side.php'); ?>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('includes/copy-right.php');?>
</div>
<script type="text/javascript" src="js/engine1/wowslider.js"></script>
<script type="text/javascript" src="js/engine1/script.js"></script>
</body>
</html>