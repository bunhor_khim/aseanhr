-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 29, 2015 at 08:09 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aseanhr_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `counter_ips`
--

CREATE TABLE IF NOT EXISTS `counter_ips` (
  `ip` varchar(15) NOT NULL,
  `visit` datetime NOT NULL,
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `counter_ips`
--

INSERT INTO `counter_ips` (`ip`, `visit`) VALUES
('127.0.0.1', '2015-09-29 15:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `counter_values`
--

CREATE TABLE IF NOT EXISTS `counter_values` (
  `id` bigint(11) NOT NULL,
  `day_id` bigint(11) NOT NULL,
  `day_value` bigint(11) NOT NULL,
  `yesterday_id` bigint(11) NOT NULL,
  `yesterday_value` bigint(11) NOT NULL,
  `week_id` bigint(11) NOT NULL,
  `week_value` bigint(11) NOT NULL,
  `month_id` bigint(11) NOT NULL,
  `month_value` bigint(11) NOT NULL,
  `year_id` bigint(11) NOT NULL,
  `year_value` bigint(11) NOT NULL,
  `all_value` bigint(11) NOT NULL,
  `record_date` datetime NOT NULL,
  `record_value` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `counter_values`
--

INSERT INTO `counter_values` (`id`, `day_id`, `day_value`, `yesterday_id`, `yesterday_value`, `week_id`, `week_value`, `month_id`, `month_value`, `year_id`, `year_value`, `all_value`, `record_date`, `record_value`) VALUES
(1, 271, 2, 270, 0, 40, 2, 9, 2, 2015, 2, 2, '2015-09-29 13:55:13', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `ban_id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL,
  `ban_image` varchar(100) NOT NULL,
  `ban_url` varchar(500) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`ban_id`, `cat_id`, `ban_image`, `ban_url`) VALUES
(1, 1, '1130103027.jpg', 'google.com'),
(4, 2, '829900703.png', '#'),
(5, 2, '1876545573.png', '#'),
(6, 2, '1072432973.png', '#'),
(8, 3, '2120045205.jpg', '#');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE IF NOT EXISTS `tbl_company` (
  `com_id` int(10) NOT NULL AUTO_INCREMENT,
  `ind_id` int(10) NOT NULL,
  `type_id` int(10) NOT NULL,
  `prov_id` int(10) NOT NULL,
  `emp_name` varchar(200) NOT NULL,
  `emp_password` varchar(200) NOT NULL,
  `com_emp` varchar(20) NOT NULL,
  `com_name` varchar(200) NOT NULL,
  `com_contact` varchar(100) NOT NULL,
  `com_phone` varchar(100) NOT NULL,
  `com_fax` varchar(100) NOT NULL,
  `com_email` varchar(100) NOT NULL,
  `com_website` varchar(100) NOT NULL,
  `com_addr` text NOT NULL,
  `com_desc` text NOT NULL,
  `com_image` varchar(100) NOT NULL,
  `reset_key` varchar(220) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`com_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`com_id`, `ind_id`, `type_id`, `prov_id`, `emp_name`, `emp_password`, `com_emp`, `com_name`, `com_contact`, `com_phone`, `com_fax`, `com_email`, `com_website`, `com_addr`, `com_desc`, `com_image`, `reset_key`, `status`) VALUES
(1, 1, 1, 5, '', '', '11', 'Pathmazing Inc', 'Mr.Vutha', '012334221', '023494553', 'vutha@gmail.com', 'www.google.com', '<p>#33-34,St.114,Monorom,7Makara,Phnom Penh.</p>\r\n', '<p>Pathmazing is a global software solution provider in pursuit of extraordinary mobile application</p>\r\n', '1113161005.jpg', '', 0),
(3, 1, 1, 1, '', '', '10', 'ABC co., Ltd.', 'Mr.Vutha', '0123342214', '023494553', 'vutha@gmail.com', 'www.google.com', '<p>erterte</p>\r\n', '<p>ertrtrtertre</p>\r\n', '1398377058.jpg', '', 0),
(6, 0, 0, 0, 'bunhor', 'e10adc3949ba59abbe56e057f20f883e', '', 'isale cambodia', '', '012353822', '', 'admin@gmail.com', '', '', '', '', '', 0),
(7, 2, 1, 4, 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '10', 'khemarahost', 'khaypor', '070 99 06 05', '', 'khaypor168@gmail.com', 'khmerhd.com', 'pp', 'create website', '1585065498.png', '5604d266750c0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_type`
--

CREATE TABLE IF NOT EXISTS `tbl_company_type` (
  `type_id` int(10) NOT NULL AUTO_INCREMENT,
  `type_name_en` varchar(100) NOT NULL,
  `type_name_kh` varchar(100) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_company_type`
--

INSERT INTO `tbl_company_type` (`type_id`, `type_name_en`, `type_name_kh`) VALUES
(1, 'Private Limited Company', 'ក្រុមហ៊ុនឯកជនមានកំណត់');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cv`
--

CREATE TABLE IF NOT EXISTS `tbl_cv` (
  `cv_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `additional_skills` varchar(255) CHARACTER SET utf8 NOT NULL,
  `training` text CHARACTER SET utf8 NOT NULL,
  `hobby` text CHARACTER SET utf8 NOT NULL,
  `reference` text CHARACTER SET utf8 NOT NULL,
  `position` varchar(255) CHARACTER SET utf8 NOT NULL,
  `section` int(11) NOT NULL,
  `local` int(11) NOT NULL,
  `salary` varchar(255) CHARACTER SET utf8 NOT NULL,
  `update_cv` date NOT NULL,
  PRIMARY KEY (`cv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_cv`
--

INSERT INTO `tbl_cv` (`cv_id`, `job_seeker_id`, `additional_skills`, `training`, `hobby`, `reference`, `position`, `section`, `local`, `salary`, `update_cv`) VALUES
(1, 1, 'website', '', 'football', '070 90 06 05', 'web developer', 4, 1, '200', '2015-09-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_education`
--

CREATE TABLE IF NOT EXISTS `tbl_education` (
  `edu_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `qualification` int(255) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `university` varchar(255) CHARACTER SET utf8 NOT NULL,
  `f_study` varchar(255) CHARACTER SET utf8 NOT NULL,
  `des` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`edu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_education`
--

INSERT INTO `tbl_education` (`edu_id`, `job_seeker_id`, `qualification`, `from_date`, `to_date`, `university`, `f_study`, `des`) VALUES
(1, 1, 3, '2015-09-16', '2015-04-12', 'school', 'Information Technology', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_function`
--

CREATE TABLE IF NOT EXISTS `tbl_function` (
  `func_id` int(11) NOT NULL AUTO_INCREMENT,
  `func_name_en` varchar(50) NOT NULL,
  `func_name_kh` varchar(50) CHARACTER SET ucs2 NOT NULL,
  PRIMARY KEY (`func_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_function`
--

INSERT INTO `tbl_function` (`func_id`, `func_name_en`, `func_name_kh`) VALUES
(1, ' Accounting', 'គណនេយ្យ'),
(2, 'Administration', 'រដ្ឋបាល'),
(4, 'Information Technology', 'ព័ត៌មានបច្ចេកវិទ្យា'),
(5, 'Human Resource', 'ធនធានមនុស្ស');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_industry`
--

CREATE TABLE IF NOT EXISTS `tbl_industry` (
  `ind_id` int(11) NOT NULL AUTO_INCREMENT,
  `ind_name_en` varchar(100) NOT NULL,
  `ind_name_kh` varchar(100) NOT NULL,
  PRIMARY KEY (`ind_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_industry`
--

INSERT INTO `tbl_industry` (`ind_id`, `ind_name_en`, `ind_name_kh`) VALUES
(1, 'Accounting/Audit/Tax Services', 'គណនេយ្យ'),
(2, ' Administration', 'រដ្ឋបាល');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobs`
--

CREATE TABLE IF NOT EXISTS `tbl_jobs` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `com_id` int(11) NOT NULL,
  `func_id` int(10) NOT NULL,
  `prov_id` int(11) NOT NULL,
  `ind_id` int(11) NOT NULL,
  `job_title` varchar(250) NOT NULL,
  `job_exp` varchar(250) NOT NULL,
  `job_level` varchar(250) NOT NULL,
  `job_term` varchar(250) NOT NULL,
  `job_hiring` varchar(250) NOT NULL,
  `job_salary` varchar(250) NOT NULL,
  `job_qual` varchar(250) NOT NULL,
  `job_sex` varchar(50) NOT NULL,
  `job_lang` varchar(100) NOT NULL,
  `job_age` varchar(10) NOT NULL,
  `job_pub_date` date NOT NULL,
  `job_close_date` date NOT NULL,
  `job_desc` text NOT NULL,
  `job_require` text NOT NULL,
  `how_to_apply` text NOT NULL,
  `contact_person` varchar(220) NOT NULL,
  `phone` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `web` varchar(220) NOT NULL,
  `address` varchar(220) NOT NULL,
  `user_enable` int(11) NOT NULL,
  `admin_enable` int(11) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_jobs`
--

INSERT INTO `tbl_jobs` (`job_id`, `com_id`, `func_id`, `prov_id`, `ind_id`, `job_title`, `job_exp`, `job_level`, `job_term`, `job_hiring`, `job_salary`, `job_qual`, `job_sex`, `job_lang`, `job_age`, `job_pub_date`, `job_close_date`, `job_desc`, `job_require`, `how_to_apply`, `contact_person`, `phone`, `email`, `web`, `address`, `user_enable`, `admin_enable`) VALUES
(6, 7, 1, 9, 2, 'web', '2', '1', '1', '1', '200-500', '6', '2', '', '13-15', '2015-09-30', '2015-09-30', '-new \r\n-new\r\n-new\r\n-new', '-new\r\n-new\r\n-new\r\n-new', '-new\r\n-new\r\n-new\r\n-new', '', '', '', '', '', 1, 0),
(9, 7, 1, 5, 1, 'web design', '2', '2', '2', '2', '200', '3', '2', '', '80', '2015-09-22', '2015-09-30', 'asdf\r\nasdf\r\nasdf\r\n', 'asdf\r\nasdf\r\nasdf\r\n', 'asdf\r\nasdf\r\nsadf\r\n', '', '', '', '', '', 1, 1),
(7, 7, 2, 4, 2, 'web develop', '1', '1', '1', '1', '200-500', '1', '1', '', '1', '2015-09-01', '2015-09-01', '12\r\n-asdf\r\nasdf\r\nasdf\r\nasdf\r\nsadf\r\n\r\nsadf', 'asdf\r\nasdf\r\nasdf\r\nsadf\r\nsdaf\r\nsdaf\r\n', 'asdf\r\nasdf\r\nsadf\r\nsdaf', '', '', '', '', '', 1, 1),
(10, 7, 1, 1, 2, 'new ', '1', '1', '2', '1', '200', '3', '1', '', '80', '2015-09-08', '2015-09-17', 'sadf\r\nasdf\r\nasdf', 'asdf\r\nasdf\r\nsadf\r\nsadf\r\nsdf', 'asdf\r\nsadf\r\nsd\r\nfsad\r\nfs\r\ndf\r\nsdf', '', '', '', '', '', 1, 1),
(12, 7, 2, 1, 1, 'asdf', '2', '4', '2', '2', '200-500', '2', '3', '', '18', '2015-09-22', '2015-09-21', 'asdf', 'sadf', 'asdf', '', '', '', '', '', 1, 1),
(13, 7, 1, 5, 2, 'asdf', '1', '2', '2', '12', '200', '3', '2', '', '18', '2015-09-22', '2015-09-14', 'asdf', 'asdf', 'asdf', '', '', '', '', '', 1, 1),
(15, 7, 1, 3, 1, 'web design', '1', '', '1', '12', '200-500', '2', '', '', '12', '2015-09-09', '2015-09-24', '12', '12', '12', 'khaypor', '070 99 06 05', 'khaypor168@gmail.com', 'www.khmerhd.com', 'pp', 1, 1),
(16, 7, 1, 1, 1, 'web hosting', '1', '', '2', '1', '200-500', '3', '', '', '18', '2015-09-29', '2015-09-29', 'asdfsadf\r\nasdf\r\nasdf\r\n\r\nasdfsadf\r\n\r\nasdf\r\nsd\r\nf\r\nsadf  sdf\r\n      asdfsadf\r\n\r\n', 'asdfsadf\r\nasdf\r\n\r\n\r\nsadf\r\n\r\nsadf\r\nsd\r\nf\r\nsd\r\nfs\r\nadf\r\nasd\r\nf\r\nsd\r\nfs\r\nadf\r\nsad\r\nf', 'asdfsdfsadf\r\nas\r\ndf\r\nsdfa\r\nsdf\r\nasd\r\nfsdf', 'khaypor', '070 99 06 05', 'khaypor@khemarahost.com', 'www.khmerhd.com', 'PP', 1, 1),
(17, 7, 4, 4, 1, 'accounting', '', '2', '1', '12', '500-1000', '3', '2', '', '', '2015-09-30', '2015-09-30', ' 1. សូមធើ្វការ ចុះឈោ្មះ ជាមួយ CAMHR ដើម្បីទទួលបានឱកាសការងារខ្ពស់ \r\n    និងសម្រេចការងារក្នុងក្តីស្រមៃរបស់អ្នក\r\n\r\n2. ប្រសិនជាអ្នកចង់ដាក់ពាក្យការងារដោយចុចប៊ូតុង “ដាក់ពាក្យឥឡូវនេះ”\r\n   សូមធើ្វការបងើ្កតជីវប្រវត្តិរបស់អ្នកជាមុនសិន, ម្យ៉ាងវិញទៀតនិយោកជកក៏អាច\r\n   សែ្វងរកជីវប្រវត្តិរបស់អ្នកបានដោយងាយស្រួល\r\n\r\nសូម ចុចទីនេះ ដើម្បីសិក្សាពីរបៀបចុះឈោ្មះ និងបងើ្កតជីវប្រវត្តិសងេ្ខប!', ' 1. សូមធើ្វការ ចុះឈោ្មះ ជាមួយ CAMHR \r\n\r\nដើម្បីទទួលបានឱកាសការងារខ្ពស់ \r\n\r\n    និងសម្រេចការងារក្នុងក្តីស្រមៃរបស់អ្នក\r\n\r\n2. ប្រសិនជាអ្នកចង់ដាក់ពាក្យការងារដោយចុចប៊ូតុង “ដាក់ពាក្យឥឡូវនេះ”\r\n   សូមធើ្វការបងើ្កតជីវប្រវត្តិរបស់អ្នកជាមុនសិន, ម្យ៉ាងវិញទៀតនិយោកជកក៏អាច\r\n   សែ្វងរកជីវប្រវត្តិរបស់អ្នកបានដោយងាយស្រួល\r\nសូម ចុចទីនេះ ដើម្បីសិក្សាពីរបៀបចុះឈោ្មះ និងបងើ្កតជីវប្រវត្តិសងេ្ខប!', ' 1. សូមធើ្វការ ចុះឈោ្មះ ជាមួយ CAMHR ដើម្បីទទួលបានឱកាសការងារខ្ពស់ និងសម្រេចការងារក្នុងក្តីស្រមៃរបស់អ្នក\r\n2. ប្រសិនជាអ្នកចង់ដាក់ពាក្យការងារដោយចុចប៊ូតុង “ដាក់ពាក្យឥឡូវនេះ”សូមធើ្វការបងើ្កតជីវប្រវត្តិរបស់អ្នកជាមុនសិន, ម្យ៉ាងវិញទៀតនិយោកជកក៏អាចសែ្វងរកជីវប្រវត្តិរបស់អ្នកបានដោយងាយស្រួល\r\nសូម ចុចទីនេះ ដើម្បីសិក្សាពីរបៀបចុះឈោ្មះ និងបងើ្កតជីវប្រវត្តិសងេ្ខប!', 'khaypor1', '070 99 06 051', 'khaypor168@gmail.com1', 'www.khmerhd.com1', 'pp1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobseeker`
--

CREATE TABLE IF NOT EXISTS `tbl_jobseeker` (
  `job_seeker_id` int(12) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(225) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf16 NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sex` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(220) CHARACTER SET utf8 NOT NULL,
  `date_of_birth` date NOT NULL,
  `plase_of_bitth` text CHARACTER SET utf8 NOT NULL,
  `marital` text CHARACTER SET utf8 NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `location_of_residence` int(12) NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `reset_key` varchar(220) CHARACTER SET utf8 NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`job_seeker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `tbl_jobseeker`
--

INSERT INTO `tbl_jobseeker` (`job_seeker_id`, `first_name`, `last_name`, `username`, `password`, `sex`, `email`, `photo`, `date_of_birth`, `plase_of_bitth`, `marital`, `phone`, `location_of_residence`, `address`, `reset_key`, `status`) VALUES
(1, 'sor', 'khaypor', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '1', 'khaypor@gmail.com', '1442637921335_814959908596500_2187293545535390811_n.jpg', '2015-09-07', 'pp', 'Single', '070990605', 4, 'phnom penh', '5604d3f376b3a', 1),
(26, '', '', 'bunhor', '827ccb0eea8a706c4c34a16891f84e7b', '', 'bunhor.khim@gmail.com', '', '0000-00-00', '', '', '', 0, '', '', 0),
(33, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor168@gmail.com', '', '0000-00-00', '', '', '', 0, '', '', 0),
(34, '', '', 'khaypor', 'luvica', '', 'khaypor@gmail.com', '', '0000-00-00', '', '', '', 0, '', '12szdcvfxzcfg', 0),
(35, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'info@khemarahost.com', '', '0000-00-00', '', '', '', 0, '', '5605f587dd7bb', 0),
(36, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor167@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065887bbe83', 0),
(37, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'kahypor166@gmail.com', '', '0000-00-00', '', '', '', 0, '', '5606591338981', 1),
(38, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor165@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065943a24f4', 1),
(39, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor164@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065a05a53a0', 1),
(40, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor163@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065ab85db9b', 1),
(41, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor162@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065add82fed', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language`
--

CREATE TABLE IF NOT EXISTS `tbl_language` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `language` varchar(220) NOT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_language`
--

INSERT INTO `tbl_language` (`lang_id`, `job_seeker_id`, `level`, `language`) VALUES
(1, 1, 3, 'English');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `m_name_en` varchar(50) NOT NULL,
  `m_name_kh` varchar(50) NOT NULL,
  `m_url` varchar(500) NOT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`m_id`, `parent_id`, `m_name_en`, `m_name_kh`, `m_url`) VALUES
(0, 0, 'Main Menu', 'មីនុយដើម', '0'),
(2, 0, 'Our Services', 'សេវាកម្ម​ របស់យើង', 'financeplustraining.com'),
(3, 2, 'Basic Jobs', 'ការងារចំបង', 'http://google.com'),
(4, 5, 'Urgent Jobs', 'ការងារ​ ប្រញាប់', '3'),
(5, 0, 'About us', 'អំពីយើង', '4'),
(7, 5, 'Promotion', 'បញ្ចុះតំលៃ', 'financeplustraining.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_province`
--

CREATE TABLE IF NOT EXISTS `tbl_province` (
  `prov_id` int(10) NOT NULL AUTO_INCREMENT,
  `prov_name_en` varchar(50) NOT NULL,
  `prov_name_kh` varchar(50) NOT NULL,
  PRIMARY KEY (`prov_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_province`
--

INSERT INTO `tbl_province` (`prov_id`, `prov_name_en`, `prov_name_kh`) VALUES
(1, 'Phnom Penh', 'ភ្នំពេញ'),
(3, 'Kompong Cham', 'កំពង់ចាម'),
(4, 'kandal', 'កណ្តាល'),
(5, 'Prey Veng', 'ព្រៃវែង'),
(6, 'Siem Reap', 'សៀមរាប'),
(7, 'Kompong Thom', 'កំពង់ធំ'),
(8, 'Batambong', 'បាត់ដំបង'),
(9, 'kompong Chnang', 'កំពង់ឆ្នាំង');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` date NOT NULL,
  `user_description` longtext NOT NULL,
  `user_type` int(11) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `full_name`, `password`, `register_date`, `user_description`, `user_type`, `email`) VALUES
(1, 'admin', 'supper administrator', '750ebbb77044187afa7851138dcbb660', '2015-03-20', 'for supper admin', 1, 'bunhor.khim@gmail.com'),
(73, 'normal', 'normal', '827ccb0eea8a706c4c34a16891f84e7b', '2015-09-10', 'normal', 2, 'normal@gmail.com'),
(71, 'aseanhr', 'admin page', '97f551ad0ea7e7b1a0acaaacaf59f1b3', '2015-09-10', 'for admin page', 1, 'info@aseanhr.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_experience`
--

CREATE TABLE IF NOT EXISTS `tbl_work_experience` (
  `work_ex_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `to_date` date NOT NULL,
  `position` varchar(255) NOT NULL,
  `des` text NOT NULL,
  PRIMARY KEY (`work_ex_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_work_experience`
--

INSERT INTO `tbl_work_experience` (`work_ex_id`, `job_seeker_id`, `company`, `start_date`, `to_date`, `position`, `des`) VALUES
(1, 1, 'khemarahost', '2015-08-31', '2015-09-15', 'web develop', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
