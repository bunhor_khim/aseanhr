-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2015 at 07:43 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aseanhr_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `counter_ips`
--

CREATE TABLE IF NOT EXISTS `counter_ips` (
  `ip` varchar(15) NOT NULL,
  `visit` datetime NOT NULL,
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `counter_ips`
--

INSERT INTO `counter_ips` (`ip`, `visit`) VALUES
('127.0.0.1', '2015-10-08 11:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `counter_values`
--

CREATE TABLE IF NOT EXISTS `counter_values` (
  `id` bigint(11) NOT NULL,
  `day_id` bigint(11) NOT NULL,
  `day_value` bigint(11) NOT NULL,
  `yesterday_id` bigint(11) NOT NULL,
  `yesterday_value` bigint(11) NOT NULL,
  `week_id` bigint(11) NOT NULL,
  `week_value` bigint(11) NOT NULL,
  `month_id` bigint(11) NOT NULL,
  `month_value` bigint(11) NOT NULL,
  `year_id` bigint(11) NOT NULL,
  `year_value` bigint(11) NOT NULL,
  `all_value` bigint(11) NOT NULL,
  `record_date` datetime NOT NULL,
  `record_value` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `counter_values`
--

INSERT INTO `counter_values` (`id`, `day_id`, `day_value`, `yesterday_id`, `yesterday_value`, `week_id`, `week_value`, `month_id`, `month_value`, `year_id`, `year_value`, `all_value`, `record_date`, `record_value`) VALUES
(1, 280, 3, 279, 7, 41, 119, 10, 165, 2015, 176, 176, '2015-10-08 11:46:44', 100);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `ban_id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL,
  `ban_image` varchar(100) NOT NULL,
  `ban_url` varchar(500) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`ban_id`, `cat_id`, `ban_image`, `ban_url`) VALUES
(10, 1, '1695325866.jpg', '#'),
(4, 2, '829900703.png', '#'),
(5, 2, '1876545573.png', '#'),
(6, 2, '1072432973.png', '#'),
(11, 1, '110905425.jpg', '#'),
(12, 1, '1207692811.jpg', '#'),
(13, 1, '2039432075.jpg', '#'),
(16, 3, '1780131007.png', '#'),
(17, 3, '972569919.png', '#');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE IF NOT EXISTS `tbl_company` (
  `com_id` int(10) NOT NULL AUTO_INCREMENT,
  `ind_id` int(10) NOT NULL,
  `type_id` int(10) NOT NULL,
  `prov_id` int(10) NOT NULL,
  `emp_name` varchar(200) NOT NULL,
  `emp_password` varchar(200) NOT NULL,
  `com_emp` varchar(20) NOT NULL,
  `com_name` varchar(200) NOT NULL,
  `com_contact` varchar(100) NOT NULL,
  `com_phone` varchar(100) NOT NULL,
  `com_fax` varchar(100) NOT NULL,
  `com_email` varchar(100) NOT NULL,
  `com_website` varchar(100) NOT NULL,
  `com_addr` text NOT NULL,
  `com_desc` text NOT NULL,
  `com_image` varchar(100) NOT NULL,
  `reset_key` varchar(220) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`com_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`com_id`, `ind_id`, `type_id`, `prov_id`, `emp_name`, `emp_password`, `com_emp`, `com_name`, `com_contact`, `com_phone`, `com_fax`, `com_email`, `com_website`, `com_addr`, `com_desc`, `com_image`, `reset_key`, `status`) VALUES
(1, 1, 1, 5, '', '', '11', 'Pathmazing Inc', 'Mr.Vutha', '012334221', '023494553', 'vutha@gmail.com', 'www.google.com', '<p>#33-34,St.114,Monorom,7Makara,Phnom Penh.</p>\r\n', '<p>Pathmazing is a global software solution provider in pursuit of extraordinary mobile application</p>\r\n', '1113161005.jpg', '', 0),
(3, 1, 1, 1, '', '', '10', 'ABC co., Ltd.', 'Mr.Vutha', '0123342214', '023494553', 'vutha@gmail.com', 'www.google.com', '<p>erterte</p>\r\n', '<p>ertrtrtertre</p>\r\n', '1398377058.jpg', '', 0),
(6, 0, 0, 0, 'bunhor', 'e10adc3949ba59abbe56e057f20f883e', '', 'isale cambodia', '', '012353822', '', 'admin@gmail.com', '', '', '', '', '', 0),
(7, 2, 1, 4, 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '10', 'khemarahost', 'khaypor', '070 99 06 05', '', 'khaypor168@gmail.com', 'khmerhd.com', 'pp', 'create website', '1585065498.png', '5604d266750c0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_type`
--

CREATE TABLE IF NOT EXISTS `tbl_company_type` (
  `type_id` int(10) NOT NULL AUTO_INCREMENT,
  `type_name_en` varchar(100) NOT NULL,
  `type_name_kh` varchar(100) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_company_type`
--

INSERT INTO `tbl_company_type` (`type_id`, `type_name_en`, `type_name_kh`) VALUES
(1, 'Private Limited Company', 'ក្រុមហ៊ុនឯកជនមានកំណត់');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cv`
--

CREATE TABLE IF NOT EXISTS `tbl_cv` (
  `cv_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `additional_skills` varchar(255) CHARACTER SET utf8 NOT NULL,
  `training` text CHARACTER SET utf8 NOT NULL,
  `hobby` text CHARACTER SET utf8 NOT NULL,
  `reference` text CHARACTER SET utf8 NOT NULL,
  `position` varchar(255) CHARACTER SET utf8 NOT NULL,
  `section` int(11) NOT NULL,
  `local` int(11) NOT NULL,
  `salary` varchar(255) CHARACTER SET utf8 NOT NULL,
  `update_cv` date NOT NULL,
  PRIMARY KEY (`cv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_cv`
--

INSERT INTO `tbl_cv` (`cv_id`, `job_seeker_id`, `additional_skills`, `training`, `hobby`, `reference`, `position`, `section`, `local`, `salary`, `update_cv`) VALUES
(8, 1, 'Additional Skills', 'Training', 'Hobby', 'Reference', 'web developer', 2, 3, '200-500', '2015-10-05'),
(9, 1, '1', '1', '1', '1', 'administrator', 2, 1, '1000-2000', '2015-10-05'),
(10, 1, 'asdf', 'asdf', 'asdf', 'asdf', 'new test', 18, 0, '1000-2000', '2015-10-08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cv_local`
--

CREATE TABLE IF NOT EXISTS `tbl_cv_local` (
  `cv_local_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) NOT NULL,
  `jobseeker_id` int(11) NOT NULL,
  `cv_id` int(11) NOT NULL,
  PRIMARY KEY (`cv_local_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_education`
--

CREATE TABLE IF NOT EXISTS `tbl_education` (
  `edu_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `edu_cv_id` int(11) NOT NULL,
  `qualification` int(255) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `university` varchar(255) CHARACTER SET utf8 NOT NULL,
  `f_study` varchar(255) CHARACTER SET utf8 NOT NULL,
  `des` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`edu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_education`
--

INSERT INTO `tbl_education` (`edu_id`, `job_seeker_id`, `edu_cv_id`, `qualification`, `from_date`, `to_date`, `university`, `f_study`, `des`) VALUES
(7, 1, 8, 0, '0000-00-00', '0000-00-00', '', '', ''),
(9, 1, 9, 0, '0000-00-00', '0000-00-00', '', '', ''),
(10, 1, 10, 3, '2015-04-12', '2015-04-12', 'school', 'Information Technology', ''),
(11, 1, 10, 5, '2015-04-12', '2015-04-12', 'school', 'it', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feature_emp`
--

CREATE TABLE IF NOT EXISTS `tbl_feature_emp` (
  `fe_id` int(11) NOT NULL AUTO_INCREMENT,
  `fe_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `fe_image` varchar(500) CHARACTER SET utf8 NOT NULL,
  `fe_url` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`fe_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_feature_emp`
--

INSERT INTO `tbl_feature_emp` (`fe_id`, `fe_name`, `fe_image`, `fe_url`) VALUES
(1, 'Smart', '756665945.png', '#'),
(3, 'Seatel', '1821997623.png', '#'),
(4, 'Open Net', '533023964.png', '#'),
(5, 'Chip Mong Group', '1124253085.png', '#'),
(6, 'Peng Hout', '203610113.png', '#'),
(7, 'Master Gold ', '1077500481.png', '#'),
(8, 'UMG', '1217160268.png', '#'),
(9, 'CBL', '790468009.png', '#'),
(10, 'MW Grolup', '1329965412.png', '#'),
(11, 'Wing', '1932082093.png', '#'),
(12, 'NYIS', '1312415380.png', '#'),
(13, 'SSEZ', '637725891.png', '#');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_function`
--

CREATE TABLE IF NOT EXISTS `tbl_function` (
  `func_id` int(11) NOT NULL AUTO_INCREMENT,
  `func_name_en` varchar(50) NOT NULL,
  `func_name_kh` varchar(50) CHARACTER SET ucs2 NOT NULL,
  PRIMARY KEY (`func_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `tbl_function`
--

INSERT INTO `tbl_function` (`func_id`, `func_name_en`, `func_name_kh`) VALUES
(1, ' Accounting', 'គណនេយ្យ'),
(2, 'Administration', 'រដ្ឋបាល'),
(4, 'Information Technology', 'ព័ត៌មានបច្ចេកវិទ្យា'),
(5, 'Human Resource', 'ធនធានមនុស្ស'),
(7, 'Assistant/Secretary', ' ជំនួយការ/លេខាធិការ'),
(8, 'Catering/Restaurant', ' ភោជនីយដ្ឋាន'),
(9, 'Design', ' ឌីហ្សាញ '),
(10, 'Hotel/Hospitality', ' សណ្ឋាគារ/បដិសណ្ឋារកិច្'),
(11, 'Lawyer/Legal Service', ' មេធាវី/ផ្នែកច្បាប់ '),
(12, 'Media/Advertising', ' ផ្សព្វផ្សាយ '),
(13, 'Operations', ' ប្រតិបតិ្តការ '),
(14, 'Resort/Casino', ' រមណីយដ្ឋាន/កាស៊ីណូ'),
(15, 'Telecommunication', ' ទូរគមនាគមន៍ '),
(16, 'Banking/Insurance', 'ធនាគារ/ធានារ៉ាប់រង'),
(17, 'Finance', ' ហិរញ្ញវត្ថុ'),
(18, 'Audit/Taxation', ' សវនកម្ម/ពន្ធដា '),
(19, 'Management', 'គ្រប់គ្រង'),
(20, 'Medical/Health/Nursing', ' ពិនិត្យ/ថែទាំសុខភាព'),
(21, 'Education/Training', ' អប់រំ/បណ្តុះបណ្តាល'),
(22, 'Consultancy', ' ប្រឹក្សាយោបល់ '),
(23, 'Customer Service', 'សេវាបម្រើអតិថិជន'),
(24, 'Project Management', ' ការគ្រប់គ្រងគម្រោង'),
(25, 'Sales ', ' លក់ '),
(26, 'Security guard/Driver/Maid', ' សន្តិសុខ/អ្នកបើកបរ/អ្នកមើលក្មេង'),
(27, 'Marketing', ' ទីផ្សារ'),
(28, 'Manufacturing', 'ការផលិត'),
(29, 'Merchandising/Purchasing ', 'បញ្ជាទិញ'),
(30, 'Quality Control', ' ត្រួតពិនិត្យគុណភាព '),
(31, 'Technician', ' ព័ត៌មានបច្ចេកវិទ្យា'),
(32, 'Architecture/ Engeneering', ' សា្ថបត្យកម្ម/វិស្វកម្ម'),
(33, 'Translation/Interpretation', ' បកប្រែ '),
(34, 'Travel Agent/Ticket Sales', ' ភ្នាក់ងារទេសចរណ៏/លក់សំបុត្រ '),
(35, 'Freight/Shipping/Delivery/Warehouse', ' ដឹកជញ្ចូន/នាំចេញ/នាំចូល '),
(36, 'Others', ' ផ្សេងៗ '),
(37, 'Real Estate', 'អចលនទ្រព្យ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_industry`
--

CREATE TABLE IF NOT EXISTS `tbl_industry` (
  `ind_id` int(11) NOT NULL AUTO_INCREMENT,
  `ind_name_en` varchar(100) NOT NULL,
  `ind_name_kh` varchar(100) NOT NULL,
  PRIMARY KEY (`ind_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `tbl_industry`
--

INSERT INTO `tbl_industry` (`ind_id`, `ind_name_en`, `ind_name_kh`) VALUES
(1, 'Accounting/Audit/Tax Services', 'គណនេយ្យ'),
(2, ' Administration', 'រដ្ឋបាល'),
(3, 'Assistant/Secretary', 'ជំនួយការ/លេខាធិការ'),
(4, 'Audit/Taxation', 'សវនកម្ម/ពន្ធដា'),
(5, 'Banking/Insurance', ' ធនាគារ/ធានារ៉ាប់រង'),
(6, 'Architecture/ Engeneering', ' សា្ថបត្យកម្ម/វិស្វកម្ម '),
(7, 'Catering/Restaurant', ' ភោជនីយដ្ឋាន '),
(8, 'Design', 'ឌីហ្សាញ'),
(9, 'Consultancy ', ' ប្រឹក្សាយោបល់ '),
(10, 'Customer Service', ' សេវាបម្រើអតិថិជន'),
(11, 'Education/Training', ' អប់រំ/បណ្តុះបណ្តាល '),
(12, 'Finance', ' ហិរញ្ញវត្ថុ '),
(13, 'Human Resource', ' ធនធានមនុស្ស'),
(14, 'Information Technology', ' ព័ត៌មានបច្ចេកវិទ្យា'),
(15, 'Management ', ' គ្រប់គ្រង '),
(16, 'Manufacturing', ' ការផលិត '),
(17, 'Medical/Health/Nursing', ' ពិនិត្យ/ថែទាំសុខភាព'),
(18, 'Merchandising/Purchasing ', ' បញ្ជាទិញ '),
(19, 'Project Management', ' ការគ្រប់គ្រងគម្រោង '),
(20, 'Quality Control', ' ត្រួតពិនិត្យគុណភាព'),
(21, 'Sales', ' លក់ '),
(22, 'Technician', ' បច្ចេកទេស '),
(23, 'Security guard/Driver/Maid', ' សន្តិសុខ/អ្នកបើកបរ/អ្នកមើលក្មេង '),
(24, 'Marketing', ' ទីផ្សារ '),
(25, 'Hotel/Hospitality', ' សណ្ឋាគារ/បដិសណ្ឋារកិច្ច'),
(26, 'Lawyer/Legal Service', 'មេធាវី/ផ្នែកច្បាប់'),
(27, 'Media/Advertising', 'ផ្សព្វផ្សាយ'),
(28, 'Operations', 'ប្រតិបតិ្តការ'),
(29, 'Resort/Casino', 'រមណីយដ្ឋាន/កាស៊ីណូ'),
(30, 'Telecommunication', ' ទូរគមនាគមន៍ '),
(31, 'Translation/Interpretation', ' បកប្រែ '),
(32, 'Travel Agent/Ticket Sales', ' ភ្នាក់ងារទេសចរណ៏/លក់សំបុត្រ'),
(33, 'Freight/Shipping/Delivery/Warehouse', ' ដឹកជញ្ចូន/នាំចេញ/នាំចូល '),
(34, 'Others', ' ផ្សេងៗ'),
(35, 'Real Estate', 'អចលនទ្រព្យ'),
(36, ' 	Food & Beverages', 'ម្ហូបអាហារ/ភេសជ្ជៈ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobs`
--

CREATE TABLE IF NOT EXISTS `tbl_jobs` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `com_id` int(11) NOT NULL,
  `func_id` int(10) NOT NULL,
  `prov_id` int(11) NOT NULL,
  `ind_id` int(11) NOT NULL,
  `job_title` varchar(250) NOT NULL,
  `job_exp` varchar(250) NOT NULL,
  `job_level` varchar(250) NOT NULL,
  `job_term` varchar(250) NOT NULL,
  `job_hiring` varchar(250) NOT NULL,
  `job_salary` varchar(250) NOT NULL,
  `job_qual` varchar(250) NOT NULL,
  `job_sex` varchar(50) NOT NULL,
  `job_lang` varchar(100) NOT NULL,
  `job_age` varchar(10) NOT NULL,
  `job_pub_date` date NOT NULL,
  `job_close_date` date NOT NULL,
  `job_desc` text NOT NULL,
  `job_require` text NOT NULL,
  `how_to_apply` text NOT NULL,
  `contact_person` varchar(220) NOT NULL,
  `phone` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `web` varchar(220) NOT NULL,
  `address` varchar(220) NOT NULL,
  `job_alert` int(11) NOT NULL,
  `user_enable` int(11) NOT NULL,
  `admin_enable` int(11) NOT NULL,
  `job_package` int(11) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_jobs`
--

INSERT INTO `tbl_jobs` (`job_id`, `com_id`, `func_id`, `prov_id`, `ind_id`, `job_title`, `job_exp`, `job_level`, `job_term`, `job_hiring`, `job_salary`, `job_qual`, `job_sex`, `job_lang`, `job_age`, `job_pub_date`, `job_close_date`, `job_desc`, `job_require`, `how_to_apply`, `contact_person`, `phone`, `email`, `web`, `address`, `job_alert`, `user_enable`, `admin_enable`, `job_package`) VALUES
(6, 7, 1, 9, 2, 'web', '2', '1', '1', '1', '200-500', '6', '2', '', '13-15', '2015-10-07', '2015-11-06', '-new \r\n-new\r\n-new\r\n-new', '-new\r\n-new\r\n-new\r\n-new', '-new\r\n-new\r\n-new\r\n-new', '', '', '', '', '', 1, 1, 1, 2),
(9, 7, 1, 5, 1, 'web design', '2', '2', '2', '2', '200', '3', '2', '', '80', '2015-10-07', '2015-11-06', 'asdf\r\nasdf\r\nasdf\r\n', 'asdf\r\nasdf\r\nasdf\r\n', 'asdf\r\nasdf\r\nsadf\r\n', '', '', '', '', '', 1, 1, 1, 2),
(7, 7, 2, 4, 2, 'web develop', '1', '1', '1', '1', '200-500', '1', '1', '', '1', '2015-10-07', '2015-11-06', '12\r\n-asdf\r\nasdf\r\nasdf\r\nasdf\r\nsadf\r\n\r\nsadf', 'asdf\r\nasdf\r\nasdf\r\nsadf\r\nsdaf\r\nsdaf\r\n', 'asdf\r\nasdf\r\nsadf\r\nsdaf', '', '', '', '', '', 1, 1, 1, 2),
(10, 7, 1, 1, 2, 'new ', '1', '1', '2', '1', '200', '3', '1', 'English-good khmer-very Good', '80', '2015-10-07', '2015-10-22', 'sadf\r\nasdf\r\nasdf', 'asdf\r\nasdf\r\nsadf\r\nsadf\r\nsdf', 'asdf\r\nsadf\r\nsd\r\nfsad\r\nfs\r\ndf\r\nsdf', 'khaypor', '070 99 06 05', 'khaypor168@gmail.com', 'www.khmerhd.com', 'PP', 1, 1, 1, 1),
(12, 7, 2, 1, 1, 'asdf', '2', '4', '2', '2', '200-500', '2', '3', '', '18', '2015-10-07', '2015-11-06', 'asdf', 'sadf', 'asdf', '', '', '', '', '', 2, 1, 1, 2),
(13, 7, 1, 5, 2, 'asdf', '1', '2', '2', '12', '200', '3', '2', '', '18', '2015-10-07', '2015-10-22', 'asdf', 'asdf', 'asdf', '', '', '', '', '', 2, 1, 1, 1),
(15, 7, 1, 3, 1, 'web design', '1', '', '1', '12', '200-500', '2', '', '', '12', '2015-10-07', '2015-10-22', '12', '12', '12', 'khaypor', '070 99 06 05', 'khaypor168@gmail.com', 'www.khmerhd.com', 'pp', 2, 1, 1, 1),
(16, 7, 1, 1, 1, 'web hosting', '1', '', '2', '1', '200-500', '3', '', '', '18', '2015-10-07', '2015-11-06', 'asdfsadf\r\nasdf\r\nasdf\r\n\r\nasdfsadf\r\n\r\nasdf\r\nsd\r\nf\r\nsadf  sdf\r\n      asdfsadf\r\n\r\n', 'asdfsadf\r\nasdf\r\n\r\n\r\nsadf\r\n\r\nsadf\r\nsd\r\nf\r\nsd\r\nfs\r\nadf\r\nasd\r\nf\r\nsd\r\nfs\r\nadf\r\nsad\r\nf', 'asdfsdfsadf\r\nas\r\ndf\r\nsdfa\r\nsdf\r\nasd\r\nfsdf', 'khaypor', '070 99 06 05', 'khaypor@khemarahost.com', 'www.khmerhd.com', 'PP', 2, 1, 1, 2),
(17, 7, 4, 4, 1, 'accounting', '', '2', '1', '12', '500-1000', '3', '2', '', '', '2015-10-07', '2015-11-06', ' 1. សូមធើ្វការ ចុះឈោ្មះ ជាមួយ CAMHR ដើម្បីទទួលបានឱកាសការងារខ្ពស់ \r\n    និងសម្រេចការងារក្នុងក្តីស្រមៃរបស់អ្នក\r\n\r\n2. ប្រសិនជាអ្នកចង់ដាក់ពាក្យការងារដោយចុចប៊ូតុង “ដាក់ពាក្យឥឡូវនេះ”\r\n   សូមធើ្វការបងើ្កតជីវប្រវត្តិរបស់អ្នកជាមុនសិន, ម្យ៉ាងវិញទៀតនិយោកជកក៏អាច\r\n   សែ្វងរកជីវប្រវត្តិរបស់អ្នកបានដោយងាយស្រួល\r\n\r\nសូម ចុចទីនេះ ដើម្បីសិក្សាពីរបៀបចុះឈោ្មះ និងបងើ្កតជីវប្រវត្តិសងេ្ខប!', ' 1. សូមធើ្វការ ចុះឈោ្មះ ជាមួយ CAMHR \r\n\r\nដើម្បីទទួលបានឱកាសការងារខ្ពស់ \r\n\r\n    និងសម្រេចការងារក្នុងក្តីស្រមៃរបស់អ្នក\r\n\r\n2. ប្រសិនជាអ្នកចង់ដាក់ពាក្យការងារដោយចុចប៊ូតុង “ដាក់ពាក្យឥឡូវនេះ”\r\n   សូមធើ្វការបងើ្កតជីវប្រវត្តិរបស់អ្នកជាមុនសិន, ម្យ៉ាងវិញទៀតនិយោកជកក៏អាច\r\n   សែ្វងរកជីវប្រវត្តិរបស់អ្នកបានដោយងាយស្រួល\r\nសូម ចុចទីនេះ ដើម្បីសិក្សាពីរបៀបចុះឈោ្មះ និងបងើ្កតជីវប្រវត្តិសងេ្ខប!', ' 1. សូមធើ្វការ ចុះឈោ្មះ ជាមួយ CAMHR ដើម្បីទទួលបានឱកាសការងារខ្ពស់ និងសម្រេចការងារក្នុងក្តីស្រមៃរបស់អ្នក\r\n2. ប្រសិនជាអ្នកចង់ដាក់ពាក្យការងារដោយចុចប៊ូតុង “ដាក់ពាក្យឥឡូវនេះ”សូមធើ្វការបងើ្កតជីវប្រវត្តិរបស់អ្នកជាមុនសិន, ម្យ៉ាងវិញទៀតនិយោកជកក៏អាចសែ្វងរកជីវប្រវត្តិរបស់អ្នកបានដោយងាយស្រួល\r\nសូម ចុចទីនេះ ដើម្បីសិក្សាពីរបៀបចុះឈោ្មះ និងបងើ្កតជីវប្រវត្តិសងេ្ខប!', 'khaypor1', '070 99 06 051', 'khaypor168@gmail.com1', 'www.khmerhd.com1', 'pp1', 2, 1, 1, 2),
(18, 7, 1, 6, 1, 'new job', '', '', '3', '2', '500-1000', '3', '', 'English-good khmer-very Good', '18-30', '2015-10-07', '2015-11-06', 'serf', 'asdf', 'asdf', 'khaypor', '070 99 06 05', 'khaypor@khemarahost.com', 'www.khmerhd.com', 'pp', 2, 1, 1, 2),
(19, 7, 2, 6, 2, 'admin', '', '', '3', '1', '500-1000', '4', '', 'English-good khmer-very Good', '18-30', '2015-10-07', '2015-10-22', 'asdf', 'asdf', 'asd', 'khaypor', '070 99 06 05', 'khaypor168@gmail.com', 'www.khmerhd.com', 'pp', 2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobseeker`
--

CREATE TABLE IF NOT EXISTS `tbl_jobseeker` (
  `job_seeker_id` int(12) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(225) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf16 NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sex` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(220) CHARACTER SET utf8 NOT NULL,
  `date_of_birth` date NOT NULL,
  `plase_of_bitth` text CHARACTER SET utf8 NOT NULL,
  `marital` text CHARACTER SET utf8 NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `location_of_residence` int(12) NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `reset_key` varchar(220) CHARACTER SET utf8 NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`job_seeker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `tbl_jobseeker`
--

INSERT INTO `tbl_jobseeker` (`job_seeker_id`, `first_name`, `last_name`, `username`, `password`, `sex`, `email`, `photo`, `date_of_birth`, `plase_of_bitth`, `marital`, `phone`, `location_of_residence`, `address`, `reset_key`, `status`) VALUES
(1, 'sor', 'khaypor', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '1', 'khaypor@gmail.com', '1442637921335_814959908596500_2187293545535390811_n.jpg', '2015-09-07', 'pp', 'Single', '070990605', 4, 'phnom penh', '5604d3f376b3a', 1),
(26, '', '', 'bunhor', '827ccb0eea8a706c4c34a16891f84e7b', '', 'bunhor.khim@gmail.com', '', '0000-00-00', '', '', '', 0, '', '', 0),
(33, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor168@gmail.com', '', '0000-00-00', '', '', '', 0, '', '', 0),
(34, '', '', 'khaypor', 'luvica', '', 'khaypor@gmail.com', '', '0000-00-00', '', '', '', 0, '', '12szdcvfxzcfg', 0),
(35, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'info@khemarahost.com', '', '0000-00-00', '', '', '', 0, '', '5605f587dd7bb', 0),
(36, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor167@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065887bbe83', 0),
(37, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'kahypor166@gmail.com', '', '0000-00-00', '', '', '', 0, '', '5606591338981', 1),
(38, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor165@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065943a24f4', 1),
(39, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor164@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065a05a53a0', 1),
(40, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor163@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065ab85db9b', 1),
(41, '', '', 'khaypor', '374c35343788c417dcef8e0e490ba2e2', '', 'khaypor162@gmail.com', '', '0000-00-00', '', '', '', 0, '', '56065add82fed', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_learning`
--

CREATE TABLE IF NOT EXISTS `tbl_job_learning` (
  `jl_id` int(11) NOT NULL AUTO_INCREMENT,
  `jl_title` varchar(300) NOT NULL,
  `jl_desc` text NOT NULL,
  PRIMARY KEY (`jl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_job_learning`
--

INSERT INTO `tbl_job_learning` (`jl_id`, `jl_title`, `jl_desc`) VALUES
(1, 'How to Limit Your Mistakes during a Job Interview', '<p>1. Prep yourself about the company before the interview. You might not have enough idea about the company when you applied for a job but that has to change between the time you&#39;re asked for an inter view and the time you&#39;re sitting in there explaining why you want to work for the place so badly. Not know ing much about the place you&#39;re interviewing at reveals to the interviewers that you don&#39;t really care and that sets the interview offside from the beginning. Do some background research now and gather as much infor mation about the organization as possible. Some ways to do this include: - Read the companies or organization&#39;s annual report and related documents. Know all about their latest ventures, financial situation, philanthropic endeavors, awards and achievements. In particular, know what they expect from staff and how they reward high performing staff. - Do a search online for the company or organization. Few businesses stay offline, so even if it&#39;s just a Facebook page, you can find out information about your potential employer. Read through the website or related pages and soak up everything you can. - Be knowledgeable about the companies or organization&#39;s products and/or services, expertise, and milestones. If there is a long history, read up on it. - Avoid rote learning company facts. They don&#39;t want facts recited back at them; as you learn, think about how your job skills will match what you&#39;ve learned and what you can do to help the company or organization achieve more of the goals it?s aiming for. 1. Prep yourself about the company before the interview. You might not have enough idea about the company when you applied for a job but that has to change between the time you&#39;re asked for an inter view and the time you&#39;re sitting in there explaining why you want to work for the place so badly. Not know ing much about the place you&#39;re interviewing at reveals to the interviewers that you don&#39;t really care and that sets the interview offside from the beginning. Do some background research now and gather as much infor mation about the organization as possible. Some ways to do this include: - Read the companies or organization&#39;s annual report and related documents. Know all about their latest ventures, financial situation, philanthropic endeavors, awards and achievements. In particular, know what they expect from staff and how they reward high performing staff. - Do a search online for the company or organization. Few businesses stay offline, so even if it&#39;s just a Facebook page, you can find out information about your potential employer. Read through the website or related pages and soak up everything you can. - Be knowledgeable about the companies or organization&#39;s products and/or services, expertise, and milestones. If there is a long history, read up on it. - Avoid rote learning company facts. They don&#39;t want facts recited back at them; as you learn, think about how your job skills will match what you&#39;ve learned and what you can do to help the company or organization achieve more of the goals it?s aiming for</p>\r\n\r\n<p>.<img alt="" src="/htdocs/cljr/data/images/sky-a870-1.jpg" style="height:397px; width:505px" /></p>\r\n'),
(2, 'job learning testing email', '1.     Prep yourself about the company before the interview. You might not have enough idea about\r\n\r\nthe company when you applied for a job but that has to change between the time you''re asked for an inter\r\n\r\nview and the time you''re sitting in there explaining why you want to work for the place so badly. Not know\r\n\r\ning much about the place you''re interviewing at reveals to the interviewers that you don''t really care and that\r\n\r\nsets the interview offside from the beginning. Do some background research now and gather as much infor\r\n\r\nmation about the organization as possible. Some ways to do this include:\r\n \r\n\r\n-       Read the companies or organization''s annual report and related documents. Know all about their\r\n\r\nlatest ventures, financial situation, philanthropic endeavors, awards and achievements. In particular, know\r\n\r\nwhat they expect from staff and how they reward high performing staff.\r\n \r\n\r\n-       Do a search online for the company or organization. Few businesses stay offline, so even if it''s just\r\n\r\na Facebook page, you can find out information about your potential employer. Read through the website or\r\n\r\nrelated pages and soak up everything you can.\r\n \r\n\r\n-       Be knowledgeable about the companies or organization''s products and/or services, expertise, and\r\n\r\nmilestones. If there is a long history, read up on it.\r\n \r\n\r\n-       Avoid rote learning company facts. They don''t want facts recited back at them; as you learn, think about\r\n\r\nhow your job skills will match what you''ve learned and what you can do to help the company or organization\r\n\r\nachieve more of the goals it’s aiming for.\r\n\r\n1.     Prep yourself about the company before the interview. You might not have enough idea about\r\n\r\nthe company when you applied for a job but that has to change between the time you''re asked for an inter\r\n\r\nview and the time you''re sitting in there explaining why you want to work for the place so badly. Not know\r\n\r\ning much about the place you''re interviewing at reveals to the interviewers that you don''t really care and that\r\n\r\nsets the interview offside from the beginning. Do some background research now and gather as much infor\r\n\r\nmation about the organization as possible. Some ways to do this include:\r\n \r\n\r\n-       Read the companies or organization''s annual report and related documents. Know all about their\r\n\r\nlatest ventures, financial situation, philanthropic endeavors, awards and achievements. In particular, know\r\n\r\nwhat they expect from staff and how they reward high performing staff.\r\n \r\n\r\n-       Do a search online for the company or organization. Few businesses stay offline, so even if it''s just\r\n\r\na Facebook page, you can find out information about your potential employer. Read through the website or\r\n\r\nrelated pages and soak up everything you can.\r\n \r\n\r\n-       Be knowledgeable about the companies or organization''s products and/or services, expertise, and\r\n\r\nmilestones. If there is a long history, read up on it.\r\n \r\n\r\n-       Avoid rote learning company facts. They don''t want facts recited back at them; as you learn, think about\r\n\r\nhow your job skills will match what you''ve learned and what you can do to help the company or organization\r\n\r\nachieve more of the goals it’s aiming for.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language`
--

CREATE TABLE IF NOT EXISTS `tbl_language` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `land_cv_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `language` varchar(220) NOT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_language`
--

INSERT INTO `tbl_language` (`lang_id`, `job_seeker_id`, `land_cv_id`, `level`, `language`) VALUES
(8, 1, 8, 0, ''),
(10, 1, 9, 0, ''),
(11, 1, 10, 2, 'khmer'),
(12, 1, 10, 3, 'English');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `m_name_en` varchar(50) NOT NULL,
  `m_name_kh` varchar(50) NOT NULL,
  `m_url` varchar(500) NOT NULL,
  `order_no` int(11) NOT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`m_id`, `parent_id`, `m_name_en`, `m_name_kh`, `m_url`, `order_no`) VALUES
(0, 0, 'Main Menu', 'មីនុយដើម', '0', 0),
(2, 0, 'Our Services', 'សេវាកម្ម​ របស់យើង', '#', 1),
(9, 0, 'About Us', 'អំពីយើង', '#', 4),
(10, 0, 'Job Seeker', 'អ្នកសែ្វងរកការងារ', '#', 2),
(11, 0, 'Job Learning', 'ការរៀនការងារ', 'job_learning.php', 3),
(12, 0, 'Contact Us', 'ទំនាក់ទំនងយើងខ្ញុំ', 'contactus.php', 5),
(17, 9, 'Contact Us', 'អំពីយើង', '#', 1),
(14, 10, 'Create Acount', 'បង្កើតគណនី', 'users/register.php', 1),
(15, 2, 'Basic Job', 'ការងារជាមូលដ្ឋាន', 'services.php?ser_id=1', 1),
(16, 2, 'Urgent Job', 'ការងារបន្ទាន់', 'services.php?ser_id=3', 2),
(18, 9, 'AseanHR Ads', 'AseanHR Ads', '#', 2),
(19, 9, 'Our Servies', 'សេវាកម្មរបស់យើង', '#', 3),
(20, 9, 'How to pay', 'របៀបបង់ប្រាក់', '#', 4),
(21, 9, 'AseanHR Promotion Job Seekers', 'AseanHR Promotion Job Seekers', '#', 5),
(22, 9, 'AseanHR Promotion For Employer', 'AseanHR Promotion For Employer', '#', 6),
(23, 2, 'Featured Employs', 'Featured Employs', '#', 3),
(24, 2, 'Recruitment Agencies', 'Recruitment Agencies', '#', 4),
(25, 2, 'Banner Advertising', 'Banner Advertising', '#', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package`
--

CREATE TABLE IF NOT EXISTS `tbl_package` (
  `pk_id` int(11) NOT NULL AUTO_INCREMENT,
  `pk_name` varchar(300) CHARACTER SET utf8 NOT NULL,
  `pk_price` int(50) NOT NULL,
  `pk_post` int(11) NOT NULL,
  `pk_duration` int(11) NOT NULL,
  `pk_desc` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`pk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_package`
--

INSERT INTO `tbl_package` (`pk_id`, `pk_name`, `pk_price`, `pk_post`, `pk_duration`, `pk_desc`) VALUES
(1, '1 Basic Job', 10, 1, 15, 'Package Duration(day) means the number of days the package is valid from the day you purchased. For example: If you buy a package on 01/01/2014 and the package duration is 30 days, the last date for this package is 30/01/2014. '),
(2, '2 Basic Job', 20, 3, 30, 'Package Duration(day) means the number of days the package is valid from the day you purchased. For example: If you buy a package on 01/01/2014 and the package duration is 30 days, the last date for this package is 30/01/2014. ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE IF NOT EXISTS `tbl_payment` (
  `pm_id` int(10) NOT NULL AUTO_INCREMENT,
  `pk_id` int(10) NOT NULL,
  `com_id` int(11) NOT NULL,
  `pm_from` varchar(200) CHARACTER SET utf8 NOT NULL,
  `pm_code` varchar(200) NOT NULL,
  `pm_type` int(10) NOT NULL,
  `pm_date` date NOT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`pm_id`, `pk_id`, `com_id`, `pm_from`, `pm_code`, `pm_type`, `pm_date`) VALUES
(1, 1, 7, 'khaypor', '3453453453534', 1, '2015-10-07'),
(2, 2, 7, 'khaypor', '45645654645', 1, '2015-10-07'),
(3, 2, 7, 'khaypor', '123423sdaf', 1, '2015-10-07'),
(4, 2, 7, 'khaypor', '123234', 1, '2015-10-07'),
(5, 1, 7, 'khaypor', 'asdfsdf', 1, '2015-10-07'),
(6, 1, 7, 'khaypor', 'asdfsadf', 1, '2015-10-07'),
(7, 1, 7, 'khaypor', 'asdfsadf', 1, '2015-10-07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_province`
--

CREATE TABLE IF NOT EXISTS `tbl_province` (
  `prov_id` int(10) NOT NULL AUTO_INCREMENT,
  `prov_name_en` varchar(50) NOT NULL,
  `prov_name_kh` varchar(50) NOT NULL,
  PRIMARY KEY (`prov_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_province`
--

INSERT INTO `tbl_province` (`prov_id`, `prov_name_en`, `prov_name_kh`) VALUES
(1, 'Phnom Penh', 'ភ្នំពេញ'),
(3, 'Kompong Cham', 'កំពង់ចាម'),
(4, 'kandal', 'កណ្តាល'),
(5, 'Prey Veng', 'ព្រៃវែង'),
(6, 'Siem Reap', 'សៀមរាប'),
(7, 'Kompong Thom', 'កំពង់ធំ'),
(8, 'Batambong', 'បាត់ដំបង'),
(9, 'kompong Chnang', 'កំពង់ឆ្នាំង');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_services`
--

CREATE TABLE IF NOT EXISTS `tbl_services` (
  `ser_id` int(11) NOT NULL AUTO_INCREMENT,
  `ser_cate` int(11) NOT NULL,
  `ser_title` varchar(200) NOT NULL,
  `ser_desc` text NOT NULL,
  PRIMARY KEY (`ser_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_services`
--

INSERT INTO `tbl_services` (`ser_id`, `ser_cate`, `ser_title`, `ser_desc`) VALUES
(1, 1, 'Basic Job Posting 30 Day', '<p><strong>Detail and Location</strong></p>\r\n\r\n<p>There are 2 Basic Job Postings. One is for 15 days and the other for 30 days.</p>\r\n\r\n<p>The Basic Job Posting can be found in all job pages and job list pages. It is below the urgent job posting.</p>\r\n\r\n<p><strong>Price Lists</strong></p>\r\n\r\n<p>Buy 2 get 1 free</p>\r\n\r\n<p>Buy 3 get 2 free</p>\r\n\r\n<p>Spend more than $50 in our Basic Job Posting, you will get one Hot Job Posting for free.</p>\r\n\r\n<p>Employer can refresh their announcement to the front page.</p>\r\n\r\n<p>Free customer care: Our trained Customer Service Officers will be glad to help you. They can advise you on:</p>\r\n\r\n<p>How to post on our Jobsite to attract job seekers you need.</p>\r\n\r\n<p>How to improve your job announcements.</p>\r\n\r\n<p>How to navigate on CAMHR website.</p>\r\n\r\n<p><strong>Basic Sample</strong></p>\r\n'),
(3, 1, 'test', '<p>test</p>\r\n'),
(4, 3, 'contact me', '<p>Tel : 34545835385758374534</p>\r\n\r\n<p>Tel : 34545835385758374534</p>\r\n\r\n<p>Tel : 34545835385758374534</p>\r\n\r\n<p>Tel : 34545835385758374534</p>\r\n\r\n<p>Tel : 34545835385758374534</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` date NOT NULL,
  `user_description` longtext NOT NULL,
  `user_type` int(11) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `full_name`, `password`, `register_date`, `user_description`, `user_type`, `email`) VALUES
(1, 'admin', 'supper administrator', '750ebbb77044187afa7851138dcbb660', '2015-03-20', 'for supper admin', 1, 'bunhor.khim@gmail.com'),
(71, 'admin', 'admin page', '21232f297a57a5a743894a0e4a801fc3', '2015-09-10', 'for admin page', 1, 'info@aseanhr.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_experience`
--

CREATE TABLE IF NOT EXISTS `tbl_work_experience` (
  `work_ex_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `ex_cv_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `to_date` date NOT NULL,
  `position` varchar(255) NOT NULL,
  `des` text NOT NULL,
  PRIMARY KEY (`work_ex_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_work_experience`
--

INSERT INTO `tbl_work_experience` (`work_ex_id`, `job_seeker_id`, `ex_cv_id`, `company`, `start_date`, `to_date`, `position`, `des`) VALUES
(7, 1, 8, '', '0000-00-00', '0000-00-00', '', ''),
(9, 1, 9, '', '0000-00-00', '0000-00-00', '', ''),
(10, 1, 10, 'khemarahost', '2015-08-31', '2015-08-31', 'web develop', ''),
(11, 1, 10, 'asean boss', '2015-09-15', '2015-09-15', 'we develop', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
