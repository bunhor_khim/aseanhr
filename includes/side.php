<?php
if($page=="logout"){
  session_destroy();
  ?>
  <script type="text/javascript">
        window.location.href="/";
  </script>
<?php   
}

if(isset($_SESSION['login_job_seeker']) && ($_SESSION['login_job_seeker']) ){
	
	$job_seeker_log=$_SESSION["login_job_seeker"];
	$job_seeker_name=$_SESSION['job_seeker_name'];
	$job_seeker_id = $_SESSION['job_seeker_id'];
	?>
<!-- login -->
<h3 class="login_home_h3"><?=$lang['welcome'];?> <?=$job_seeker_name;?> !</h3>
<div class="login_home">
    <ul>
    	<li>
        <a href="users/profiles.php">
            <div class="login_profile">
            <img src="images/profile.png" width="40" />
            <h4><?=$lang['Profile'];?></h4>
            <p><?=$lang['info'];?></p>
            </div>
        </a></li>
    	<li>
        <a href="users/">
            <div class="login_profile">
                <img src="images/create_cv.png" width="40" />
                <h4><?=$lang['Create_CV'];?></h4>
                <p><?=$lang['c_cv'];?></p>
            </div>
        </a>
        </li>
        <li>
        <a href="?page=logout">
        	<div class="login_profile">
                <img src="images/logout.png" width="40" />
                <h4><?=$lang['Logout'];?></h4>
                <p><?=$lang['sign_out'];?></p>
            </div>
        </a>
        </li>
    </ul>
</div>
<!-- end login -->
<?php
}elseif(isset($_SESSION['login_employer']) && ($_SESSION['login_employer'])){
	$emp_name=$_SESSION['emp_name'];
	$com_id = $_SESSION['com_id'];
	?>
	<!-- login -->
<h3 class="login_home_h3"><?=$lang['welcome'];?> <?=$emp_name;?> !</h3>
<div class="login_home">
    <ul>
    	<li>
        <a href="employer/company_profile.php">
            <div class="login_profile">
            <img src="images/profile.png" width="40" />
            <h4><?=$lang['Company_profile'];?></h4>
            <p><?=$lang['Profile'];?></p>
            </div>
        </a></li>
    	<li>
        <a href="employer/post_job.php">
            <div class="login_profile">
                <img src="images/post.png" width="40" />
                <h4><?=$lang['post_jobs'];?></h4>
                <p><?=$lang['postj'];?></p>
            </div>
        </a>
        </li>
        <li>
        <a href="?page=logout">
        	<div class="login_profile">
                <img src="images/logout.png" width="40" />
                <h4><?=$lang['Logout'];?></h4>
                <p><?=$lang['sign_out'];?></p>
            </div>
        </a>
        </li>
    </ul>
</div>
<!-- end login -->
<?php
}else{
	?>

<script type="text/javascript" >
	$('document').ready(function(){
		$('#flip-container').quickFlip();
		
		$('#flip-navigation li a').each(function(){
			$(this).click(function(){
				$('#flip-navigation li').each(function(){
					$(this).removeClass('selected');
				});
				$(this).parent().addClass('selected');
				var flipid=$(this).attr('id').substr(4);
				$('#flip-container').quickFlipper({ }, flipid, 1);
				
				return false;
			});
		});
	});
</script>

<!-- login tabs -->
<div class="form-login-tab">
    <div id="flip-tabs" >
        <ul id="flip-navigation" >
            <li class="selected"><a href="#" id="tab-0"><?= $lang['jobseeker'];?></a></li>
            <li><a href="#" id="tab-1" ><?= $lang['employer'];?></a></li>
        </ul>
        <div id="flip-container" >
                <div class="form-login">
                    <form action="login.php" method="post">
                        <ul>
                            <li><input type="text" name="username" id="user_name" placeholder="<?= $lang['username'];?>"/></li>
                            <li><input type="password" name="password" id="password" placeholder="<?= $lang['password'];?>" /></li>
                            <li><input class="myButton" type="button" id="login" value="<?= $lang['login'];?>"/></li>
                            <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/users/register.php"><?= $lang['register'];?></a></li>
                            <li><a href="users/forget_password.php"><?= $lang['forget'];?></a></li>
                        </ul>
                    </form>
                </div>
                <div class="form-login">
                    <form action="" method="post">
                        <ul>
                            <li><input type="text" name="emp_name" id="emp_name" placeholder="<?= $lang['username'];?>"/>
                            <li><input type="password" name="emp_password" id="emp_password" placeholder="<?= $lang['password'];?>" /></li>
                            <li><input class="myButton" type="button" id="employer_login" value="<?= $lang['login'];?>"/></li>
                            <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/employer/register.php"><?= $lang['register'];?></a></li>
                            <li><a href="employer/forget_password.php"><?= $lang['forget'];?></a></li>
                        </ul>
                    </form>
                </div>
        </div>
    </div>
</div>
<!-- end login tabs -->
<?php
}
?>
<!-- alert new job -->
<h3 class="login_home_h3"><?=$lang['job_Urgent'];?></h3>
<div class="alert_new_job">
	<div class="demo5 demof">
        <ul>
        <!--<li><img src="http://www.jqueryscript.net/small/images/jQuery-Plugin-To-Expand-Collapse-Input-Field-Expandable-Input.jpg" alt="Expandable Input" /><a href="#">Expandable Input</a><a href="#"><p>Expandable Input is a minimal jQuery.</p></a>
        </li>-->
        <?php
		$select_all_job=mysqli_query($conn,"select tbl_jobs.job_title,tbl_jobs.job_id,tbl_company.com_id,tbl_company.com_name,tbl_company.com_id,tbl_jobs.job_close_date from tbl_jobs
			inner join tbl_company on
			tbl_jobs.com_id = tbl_company.com_id  
		 where  user_enable=1 and admin_enable =1 and job_alert=2 and job_close_date>Now() order by job_id desc");
		while($row_all_job=mysqli_fetch_array($select_all_job)){
		?>
        <li><a style="float:left;color:#0B54A1;" target="_blank" href="job.php?jobId=<?=$row_all_job['job_id'];?>"><?=$row_all_job['job_title'];?></a><a style="float:left;color:#bd4338;" href="companies.php?companyId=<?=$row_all_job['com_id'];?>">&nbsp;&nbsp;<?=$row_all_job['com_name'];?></a></li>
        <?php
		}
		?>
        </ul>
        </div>
        <!--
        <div class="list-group">
          <a href="#" class="list-group-item btnUp">Move Up</a>
          <a href="#" class="list-group-item btnDown">Move Down</a>
          <a href="#" class="list-group-item btnToggle">Play / Pause</a>
        </div>
        -->
</div>
<!-- end alert new job -->

<!-- adverties image -->

<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container2" style="border:1px solid #CCC;">
        <div class="ws_images">
            <ul>
		<?php
            $sql=mysqli_query($conn,"select * from tbl_banner WHERE cat_id=3 ORDER BY ban_id DESC");
            mysqli_query($conn,'SET NAMES utf8');
            while($row2=mysqli_fetch_array($sql)){

        ?>
                <li><a href="<?php echo $row2['ban_url']; ?>"><img src="images/banner/<?php echo $row2['ban_image']; ?>" id="wows2_0"/></a></li>
        <?php
			}
		?>
            </ul>
        </div>
	</div>
    <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container4" style="margin-top:15px; border:1px solid #CCC;">
        <div class="ws_images">
            <ul>
		<?php
            $sql=mysqli_query($conn,"select * from tbl_banner WHERE cat_id=4");
            mysqli_query($conn,'SET NAMES utf8');
            while($row3=mysqli_fetch_array($sql)){

        ?>
                <li><a href="<?php echo $row3['ban_url']; ?>"><img src="images/banner/<?php echo $row3['ban_image']; ?>" id="wows2_0"/></a></li>
        <?php
			}
		?>
            </ul>
        </div>
	</div>
	<!-- End WOWSlider.com BODY section -->
<!-- visitor counter -->
<?php include('includes/visitor_counter.php');?>
<!-- end visitor counter -->

<!-- end adverties image -->
<script src="js/jquery.easy-ticker.js"></script> 
<script>
$(function(){
	$('.demo3').easyTicker({
		visible: 1,
		interval: 4000
	});
	$('.demo5').easyTicker({
		direction: 'up',
		visible: 10,
		interval: 2500,
		controls: {
			up: '.btnUp',
			down: '.btnDown',
			toggle: '.btnToggle'
		}
	});
});
</script>