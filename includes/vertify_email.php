<div class="register">
	<div class="register_header"><h3>Vertify Email</h3></div>
    <div class="content_register">
    	<form id="vertify_email" method="post">
            <div class="form-item">
            	<label>Email:</label>
                <label class="form-filed"><input type="email" class="inputstyle w200" id="email" name="email" maxlength="30" ><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
                </label>
            </div>
            <div class="form-item clear">
            	<div id="sumbitdiv" class="paddingleft form-submit-btn">
            		<label class="form-button">
						<button id="registersubmit" class="btn btn-blue p30" type="button"> Vertify Email </button>
					</label>
				</div>
            </div>
        </form>
    </div>
</div>