<!-- page next-- -->
<?php
$page_name = "/job_learning.php"; // If you use this code with a different page ( or file ) name then change this

if (isset ( $_GET ['start'] )) {
	
	$start = $_GET ['start'];
} else {
	
	$start = "";
}
if (strlen ( $start ) > 0 and ! is_numeric ( $start )) {
	echo "Data Error";
	exit ();
}

$eu = ($start - 0);
$limit = 20; // No of records to be shown per page.
$this1 = $eu + $limit;
$back = $eu - $limit;
$next = $eu + $limit;

$query2 = "select * from tbl_job_learning order by jl_id desc";
$result2 = mysqli_query ( $conn, $query2 );
echo mysqli_error ( $conn );
$nume = mysqli_num_rows ( $result2 );

?>
<form method="post" enctype="multipart/form-data">
<div class="register" style="width:100%">
	<div class="emp_header"><h3><?=$lang['Job_learning'];?></h3></div>
    <div class="content_register" style="padding-top:0;">
    	<div data-mce-style="line-height: 1.8em; margin: 10px 20px;" style="line-height: 1.8em; margin: 10px 20px;">
    		
            <ul>
			<?php
				$i=0;
				$select_service=mysqli_query($conn,"select * from tbl_job_learning order by jl_id desc  limit $eu, $limit");
				while($row_service=mysqli_fetch_array($select_service)):
				$i++;
			?>
            	<li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/job_learning_detail.php?job_learning_id=<?=$row_service['jl_id'];?>"><?=$i;?> . <?=$row_service['jl_title'];?>?</a></li>
                <?php
				endwhile;
				?>
            </ul>
       </div> 
    </div>    
</div>

</form>

<!-- pagination -->
<div style="text-align: center;">
	<ul class="fancy pagination">
    <?php
												
	if ($back >= 0) {
		print "<li><a href='$page_name?start=$back'>«</a></li>";
	}
	
	$i = 0;
	$l = 1;
	for($i = 0; $i < $nume; $i = $i + $limit) {
		
		if ($i != $eu) {
			echo " <li><a href='$page_name?start=$i'>$l</a></li> ";
		} else {
			
			if ($nume < $limit) {
			} else {
				echo "<li class='active'><a href='#'>$l</a></li>";
			}
		} // / Current page is not displayed as link and given font color red
		
		$l = $l + 1;
	}
													
	if ($this1 < $nume) {
		print "<li><a href='$page_name?start=$next'>»</a></li>";
	}
	?>    
    </ul>
</div>
<!-- pagination -->
