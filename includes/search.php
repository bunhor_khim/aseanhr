<!-- search start -->	
    <div class="search_header">
        <h3><?=$lang['Find_Job_Now'];?></h3>
    </div>
    <div class="job-search-box">
        <form action="search.php" method="get">   
           <table border="0" width="610">
           		<tr>
                	<td width="80"><label><?=$lang['keyword'];?>:</label></td>
                    <td colspan="3">
                    	<div class="job-search-box-text">
                        <input id="keywords" name="keyword" type="text" value="<?php if(isset($_GET['keyword']) && $_GET['keyword']) echo$keyword;?>"/>
                    </div>
                    </td>
                </tr>
                <tr><td colspan="4" height="8"></td></tr>
                <tr>
                	<td width="80"><label><?=$lang['category'];?>:</label></td>
                    <td>
                    <select name="category" class="job-search-box-select-new">
                        <option value=""><?=$lang['function'];?></option>
                        <?php
						mysqli_query($conn,'SET NAMES utf8');
						$select_cate=mysqli_query($conn,"select * from tbl_function order by func_name_en asc");
						while($row_cate=mysqli_fetch_array($select_cate)):
						?>
                            <option <?php if(isset($_GET['category']) && $_GET['category']){if($fun_id==$row_cate['func_id']){
								echo 'selected="selected"';}} ?> value="<?=$row_cate['func_id'];?>" ><?php if(isSet($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row_cate['func_name_en'];else echo $row_cate['func_name_kh'];?></option>
                        <?php
						endwhile;
						?>
                    </select> 
               
                    </td>
                    <td width="80">&nbsp;&nbsp;&nbsp;&nbsp;<label><?=$lang['Location'];?>:</label></td>
                    <td>
                    	<select name="locationId"  class="job-search-box-select-new">
                        	<option value=""><?=$lang['Location'];?></option>
                        <?php
						mysqli_query($conn,'SET NAMES utf8');
						$select_prov=mysqli_query($conn,"select * from tbl_province order by prov_name_en asc");
						while($row_prov=mysqli_fetch_array($select_prov)):
						?>
                            <option <?php if(isset($_GET['locationId']) && $_GET['locationId']){if($locate_id==$row_prov['prov_id']){
								echo 'selected="selected"';}} ?> value="<?=$row_prov['prov_id'];?>" ><?php if(isSet($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row_prov['prov_name_en'];else echo $row_prov['prov_name_kh'];?></option>  
						<?php
						endwhile;
						?>
                    </select>  
                    </td>
                </tr>
                <tr>
                	<td colspan="4">
                    	<div class="job-search-box-btn">
                			<input type="submit" value="<?=$lang['Search']?>" style="cursor: pointer;"/>  
            			</div>
                    </td>
                </tr>
           </table>
        </form>
        
    </div>
<!-- search end -->