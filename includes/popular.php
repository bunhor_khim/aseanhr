<!-- page next-- -->
<?php
$page_name = "/popular.php?popular_search=$popular"; // If you use this code with a different page ( or file ) name then change this

if (isset ( $_GET ['start'] )) {
	
	$start = $_GET ['start'];
} else {
	
	$start = "";
}
if (strlen ( $start ) > 0 and ! is_numeric ( $start )) {
	echo "Data Error";
	exit ();
}

$eu = ($start - 0);
$limit = 50; // No of records to be shown per page.
$this1 = $eu + $limit;
$back = $eu - $limit;
$next = $eu + $limit;

$query2 = "SELECT tbl_jobs.job_title,tbl_jobs.job_id,tbl_jobs.func_id, tbl_jobs.job_close_date,tbl_jobs.job_term,tbl_jobs.job_alert, tbl_company.com_name, tbl_province.prov_name_en,tbl_jobs.job_close_date, tbl_province.prov_name_kh
										FROM tbl_jobs
											INNER JOIN tbl_company ON tbl_jobs.com_id = tbl_company.com_id
											INNER JOIN tbl_province ON tbl_jobs.prov_id = tbl_province.prov_id
										where tbl_jobs.job_term=$popular and user_enable=1 and admin_enable =1 and tbl_jobs.job_alert=1 and tbl_jobs.job_close_date>Now()";
$result2 = mysqli_query ( $conn, $query2 );
echo mysqli_error ( $conn );
$nume = mysqli_num_rows ( $result2 );

?>

<div class="jobsearch-box">
    <?php include('includes/search.php'); ?>
</div>
<div class="category_job_list">
    <table class="main-job-list-tab" border="0">
        <tr>
            <th><?= $lang['Position']; ?></th>
            <th><?= $lang['Company'];?></th>
            <th><?= $lang['Local'];?></th>
            <th><?= $lang['Date'];?></th>
        </tr>
        <?php
		$select_cate=mysqli_query($conn,"SELECT tbl_jobs.job_title,tbl_jobs.job_id,tbl_jobs.func_id,tbl_jobs.job_term, tbl_jobs.job_close_date,tbl_jobs.job_close_date,tbl_jobs.job_alert, tbl_company.com_name,tbl_company.com_id, tbl_province.prov_name_en, tbl_province.prov_name_kh
										FROM tbl_jobs
											INNER JOIN tbl_company ON tbl_jobs.com_id = tbl_company.com_id
											INNER JOIN tbl_province ON tbl_jobs.prov_id = tbl_province.prov_id
										where tbl_jobs.job_term=$popular and user_enable=1 and admin_enable =1 and tbl_jobs.job_close_date>Now() and tbl_jobs.job_alert=1 order by tbl_jobs.job_id desc limit $eu, $limit");
		while($row_cate=mysqli_fetch_array($select_cate)){
		?>
        
        <tr>
            <td><a href="http://<?php echo $_SERVER["HTTP_HOST"];?>/job.php?jobId=<?=$row_cate['job_id'];?>"><?=$row_cate['job_title'];?></a><img src="images/new.gif" alt=""></td>
            <td><a href="http://<?php echo $_SERVER["HTTP_HOST"];?>/companies.php?companyId=<?=$row_cate['com_id'];?>"><?=$row_cate['com_name'];?></a></td>
            <td><?php if(isSet($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_cate['prov_name_kh'];else echo $row_cate['prov_name_en'];?></td>
            <td><?php $date = $row_cate['job_close_date']; echo getDateFormat($date); ?></td>
        </tr>
        <?php
		}
		?>
    </table>
</div>

<!-- pagination -->
<div style="text-align: center;">
	<ul class="fancy pagination">
    <?php
												
	if ($back >= 0) {
		print "<li><a href='$page_name&start=$back'>«</a></li>";
	}
	
	$i = 0;
	$l = 1;
	for($i = 0; $i < $nume; $i = $i + $limit) {
		
		if ($i != $eu) {
			echo " <li><a href='$page_name&start=$i'>$l</a></li> ";
		} else {
			
			if ($nume < $limit) {
			} else {
				echo "<li class='active'><a href='#'>$l</a></li>";
			}
		} // / Current page is not displayed as link and given font color red
		
		$l = $l + 1;
	}
													
	if ($this1 < $nume) {
		print "<li><a href='$page_name&start=$next'>»</a></li>";
	}
	?>    
    </ul>
</div>
<!-- pagination -->