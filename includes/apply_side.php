<?php
if($page=="logout"){
  session_destroy();
  ?>
  <script type="text/javascript">
        window.location.href="/";
  </script>
<?php   
}

if(isset($_SESSION['login_job_seeker']) && ($_SESSION['login_job_seeker']) ){
	
	$job_seeker_log=$_SESSION["login_job_seeker"];
	$job_seeker_name=$_SESSION['job_seeker_name'];
	$job_seeker_id = $_SESSION['job_seeker_id'];
	?>
<!-- login -->
<h3 class="login_home_h3">Welcome to <?=$job_seeker_name;?> !</h3>
<div class="login_home">
    <ul>
    	<li>
        <a href="users/profiles.php">
            <div class="login_profile">
            <img src="images/profile.png" width="40" />
            <h4>Profile</h4>
            <p>To make a standard profile</p>
            </div>
        </a></li>
    	<li>
        <a href="users/cv.php">
            <div class="login_profile">
                <img src="images/create_cv.png" width="40" />
                <h4>Create Your CV</h4>
                <p>Click me to create your CV.</p>
            </div>
        </a>
        </li>
        <li>
        <a href="applynow.php?jobId=<?= $jobId;?>&page=logout">
        	<div class="login_profile">
                <img src="images/logout.png" width="40" />
                <h4>LOG OUT</h4>
                <p>Sign out your account click here.</p>
            </div>
        </a>
        </li>
    </ul>
</div>
<!-- end login -->
<?php
}else{
	?>


<!-- login tabs -->
<div class="form-login-tab">
    <div id="flip-tabs" >
        <ul id="flip-navigation" >
            <li class="selected"><a href="#" id="tab-0"><?= $lang['jobseeker'];?></a></li>
            <!-- <li><a href="#" id="tab-1" ><?php //$lang['employer'];?></a></li> -->
        </ul>
        <div id="flip-container" >
                <div class="form-login">
                    <form action="login.php" method="post">
                        <ul>
                            <li><input type="text" name="username" id="user_name" placeholder="<?= $lang['username'];?>"/></li>
                            <li><input type="password" name="password" id="password" placeholder="<?= $lang['password'];?>" /></li>
                            <li><input class="myButton" type="button" id="login" value="<?= $lang['login'];?>"/></li>
                            <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/users/register.php"><?= $lang['register'];?></a></li>
                            <li><a href="users/forget_password.php"><?= $lang['forget'];?></a></li>
                        </ul>
                    </form>
                </div>
        </div>
    </div>
</div>
<!-- end login tabs -->
<?php
}
?>

<!-- adverties image -->

<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container2" style="border:1px solid #CCC;">
        <div class="ws_images">
            <ul>
		<?php
            $sql=mysqli_query($conn,"select * from tbl_banner WHERE cat_id=3 ORDER BY ban_id DESC");
            mysqli_query($conn,'SET NAMES utf8');
            while($row2=mysqli_fetch_array($sql)){

        ?>
                <li><a href="<?php echo $row2['ban_url']; ?>"><img src="images/banner/<?php echo $row2['ban_image']; ?>" id="wows2_0"/></a></li>
        <?php
			}
		?>
            </ul>
        </div>
	</div>
	<!-- End WOWSlider.com BODY section -->
    <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container4" style="margin-top:15px; border:1px solid #CCC;">
        <div class="ws_images">
            <ul>
		<?php
            $sql=mysqli_query($conn,"select * from tbl_banner WHERE cat_id=4");
            mysqli_query($conn,'SET NAMES utf8');
            while($row3=mysqli_fetch_array($sql)){

        ?>
                <li><a href="<?php echo $row3['ban_url']; ?>"><img src="images/banner/<?php echo $row3['ban_image']; ?>" id="wows2_0"/></a></li>
        <?php
			}
		?>
            </ul>
        </div>
	</div>
	<!-- End WOWSlider.com BODY section --><!-- end adverties image -->
