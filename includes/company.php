<div class="main-job main-job-line"> 
 
<h2 class="main-job-w" id="jobcompany"><?=$row_com['com_name'];?></h2>
<h3 class="main-job-title-h3-2 main-job-w"><?=$lang['Company_profile'];?></h3>
<table border="0" class="main-job-tab">
    <tr>
        <th><p></p></th>
        <td></td> 
        <td style="width: 200px;" rowspan="5" align="right" valign="top">
            	<?php
					if($row_com['com_image']==''){
				?>
                <img src="users/images/no_photo.png" style="max-width: 173px;max-height: 173px"/>
            	<?php }else{?>
                	<img src="employer/images/<?=$row_com['com_image'];?>" style="max-width: 173px;max-height: 173px"/>
                <?php }?>
        </td>
    </tr>
     <tr>
        <th><p><?=$lang['Industry'];?></p></th>
        <td><?php if(isSet($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row_com['ind_name_en'];else echo $row_com['ind_name_kh'];?></td>  
    </tr>
    <tr>
        <th><p><?=$lang['Type'];?></p></th>
        <td><?php if(isSet($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row_com['type_name_en'];else echo $row_com['type_name_kh'];?></td>
    </tr>
    <tr>
        <th><p><?=$lang['Employees'];?></p></th>
        <td><?=$row_com['com_emp'];?></td>
    </tr>
    <tr>
        <th><p><?=$lang['Location'];?></p></th>
        <td><?php if(isSet($_SESSION['lang']) && $_SESSION['lang'] =='en')echo $row_com['prov_name_en'];else echo $row_com['prov_name_kh'];?></td>
    </tr> 
    <tr>
        <th><p><?=$lang['Contact_Name'];?></p></th>
        <td><?=$row_com['emp_name'];?></td>
    </tr>
    
        <tr>
            <th><p><?=$lang['Phone'];?></p></th>
            <td><?=$row_com['com_phone'];?></td>
        </tr> 
        <tr>
            <th><p><?=$lang['Fax'];?></p></th>
            <td><?=$row_com['com_fax'];?></td>
        </tr>  
        <tr>
            <th><p><?=$lang['Email'];?></p></th>
            <td><?=$row_com['com_email'];?></td>
        </tr>
        <tr>
            <th><p><?=$lang['Website'];?></p></th>
            <td>
                <a target="_blank" href="http://<?=$row_com['com_website'];?>"><?=$row_com['com_website'];?></a>      
            </td> 
        </tr>  							
        <tr>
            <th><p><?=$lang['Address'];?></p></th>
            <td colspan="2" id="address"><?=$row_com['com_addr'];?></td> 
        </tr>
      
    <tr>
        <th><p><?=$lang['Company_Description'];?></p></th>
        <td  colspan="2"><p><?=$row_com['com_desc'];?></p></td>
    </tr> 						
</table> 
</div>