<div class="jobsearch-box">
    <?php include('includes/search.php'); ?>
</div>

<div class="search-popular">
    <span><?=$lang['popular_search']?>:</span>
    <ul>
        <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/popular.php?popular=1"><?=$lang['full_time']?></a></li>
        <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/popular.php?popular=2"><?=$lang['Part_time']?></a></li>
        <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/popular.php?popular=3"><?=$lang['Internship']?></a></li>
        <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/popular.php?popular=4"><?=$lang['Volunteer']?></a></li>
        <li><a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/urgent.php?urgent=2" class="top"><?=$lang['Urgent']?></a></li>
    </ul>
</div>

<div class="browe-job">
    <ul id="tabs">
    	
        <li><a href="#" title="tab1"><?= $lang['Section']; ?></a></li>
        <li><a href="#" title="tab2"><?= $lang['Location']; ?></a></li>
        <li><a href="#" title="tab3"><?= $lang['Salary']; ?></a></li> 
        <li style="float:left;padding-left:10px; margin-top:10px;"><label><strong><?= $lang['Browse_Jobs']; ?></strong></label></li>   
    </ul>
    
    <div id="content"> 
       <div id="tab1">
        <?php
		$select_browse=mysqli_query($conn,"select * from tbl_function");
		?>
            <ul>	
            	<?php
				while($row_browse=mysqli_fetch_array($select_browse)){
					$func_id=$row_browse['func_id'];
					$select_func=mysqli_query($conn,"select func_id from tbl_jobs where func_id=$func_id and user_enable=1 and admin_enable =1 and job_close_date>Now()");
					$count_func=mysqli_num_rows($select_func);
				?>
                <li><a href="category.php?categoryId=<?=$row_browse['func_id']?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_browse['func_name_kh'];else echo $row_browse['func_name_en'];?></a><span> (<?=$count_func;?>)</span></li>
                <?php
				}
				?>
            </ul>
        </div>
        <div id="tab2">
			<?php
            $select_locate=mysqli_query($conn,"select * from tbl_province");
            ?>
            <ul>	
                <?php
				while($row_locate=mysqli_fetch_array($select_locate)){
					$locate_id=$row_locate['prov_id'];
					$select_func=mysqli_query($conn,"select prov_id from tbl_jobs where prov_id=$locate_id and user_enable=1 and admin_enable =1 and job_close_date>Now()");
					$count_func=mysqli_num_rows($select_func);
				?>
                <li><a href="location.php?location=<?=$row_locate['prov_id'];?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_locate['prov_name_kh'] ;else echo $row_locate['prov_name_en'];?></a><span> (<?=$count_func;?>)</span></li>
                <?php
				}
				?>

            </ul>
        </div>
        <div id="tab3">
            <ul><?php
					$select_200=mysqli_query($conn,"select job_salary from tbl_jobs where job_salary='200' and user_enable=1 and admin_enable =1 and job_close_date>Now()");
					$count_200=mysqli_num_rows($select_200);
				?>
                <li><a href="salary.php?salary=200"><200</a><a href="#"><span> (<?=$count_200;?>)</span></a></li>
                <?php
					$select_200_500=mysqli_query($conn,"select job_salary from tbl_jobs where job_salary='200-500' and user_enable=1 and admin_enable =1 and job_close_date>Now()");
					$count_200_500=mysqli_num_rows($select_200_500);
				?>
                <li><a href="salary.php?salary=200-500">200-500</a><a href="#"><span> (<?=$count_200_500;?>)</span></a></li>
                
                <?php
					$select_500_1000=mysqli_query($conn,"select job_salary from tbl_jobs where job_salary='500-1000' and user_enable=1 and admin_enable =1 and job_close_date>Now()");
					$count_500_1000=mysqli_num_rows($select_500_1000);
				?>
                <li><a href="salary.php?salary=500-1000">500-1000</a><a href="#"><span> (<?=$count_500_1000;?>)</span></a></li>
                
                <?php
					$select_1000_2000=mysqli_query($conn,"select job_salary from tbl_jobs where job_salary='1000_2000' and user_enable=1 and admin_enable =1 and job_close_date>Now()");
					$count_1000_2000=mysqli_num_rows($select_1000_2000);
				?>
                <li><a href="salary.php?salary=1000-2000">1000-2000</a><a href="#"><span> (<?=$count_1000_2000;?>)</span></a></li>
                
                <?php
					$select_2000=mysqli_query($conn,"select job_salary from tbl_jobs where job_salary='2000' and user_enable=1 and admin_enable =1 and job_close_date>Now()");
					$count_2000=mysqli_num_rows($select_2000);
				?>
                <li><a href="salary.php?salary=2000">>2000</a><a href="#"><span> (<?=$count_2000;?>)</span></a></li>

            </ul>
        </div>
    </div>

</div>
<!-- job list -->
<div class="job-list">
    <div class="CSSTableGenerator">
    	<div class="title_header">
    		<span><?=$lang['Recent_Jobs']; ?></span>
        </div>
        <table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th><?= $lang['Position'];?></th>
						<th><?= $lang['Company'];?></th>
						<th><?= $lang['Location'];?></th>
						<th><?= $lang['Date'];?></th>
					</tr>
				</thead>
                
				<tbody>
                	<?php
					 mysqli_query($conn,'SET NAMES utf8');
					$select_recent_job=mysqli_query($conn,"SELECT tbl_jobs.job_title,tbl_jobs.job_salary,tbl_jobs.job_id, tbl_jobs.job_close_date, tbl_province.prov_name_en, tbl_province.prov_name_kh,tbl_company.com_id,tbl_company.com_name
													FROM tbl_jobs
														INNER JOIN tbl_province ON tbl_jobs.prov_id = tbl_province.prov_id
														INNER JOIN tbl_company ON tbl_jobs.com_id = tbl_company.com_id
													where user_enable=1 and admin_enable =1 and tbl_jobs.job_close_date>Now()
													order by job_id desc ");
					while($row_recent_job=mysqli_fetch_array($select_recent_job)):
					?>
					<tr>
						<td><a style="color:#06f;" target="_blank" href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/job.php?jobId=<?=$row_recent_job['job_id'];?>"><?=$row_recent_job['job_title'];?></a></td>
                        <td><a style="color:#06f;" target="_blank" href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/companies.php?companyId=<?=$row_recent_job['com_id'];?>"><?=$row_recent_job['com_name'];?></a></td>
						
						<td><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_recent_job['prov_name_kh'];else echo $row_recent_job['prov_name_en'];?></td>
						<td><?php $date = $row_recent_job['job_close_date']; echo getDateFormat($date); ?></td>
                        
					</tr>
                    <?php
					endwhile;
					?>
				</tbody>
			</table>
    </div>
</div>
<!-- end job list -->
<!-- Featured Employers -->
<div class="feature_emp">
	<div class="emp_header"><h3>Featured Employers</h3></div>
    <?php
		mysqli_query($conn,'SET NAMES utf8');
		$fe_img=mysqli_query($conn,"SELECT * FROM tbl_feature_emp order by fe_id DESC");
		while($row_img=mysqli_fetch_array($fe_img)):
	?>
    <div class="feature_img">
    	<a href="<?=$row_img['fe_url'];?>" target="_blank" title="<?= $row_img['fe_name'];?>">
    		<img src="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/images/feature/<?= $row_img['fe_image'];?>" width="80" height="60"/>
        </a>
    </div>
     <?php
		endwhile;
	?>
</div>