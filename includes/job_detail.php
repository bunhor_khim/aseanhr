<div class="main-job main-job-line">
	<h2 class="main-job-w-title"><?=$row_job['job_title'];?></h2>					
        <table border="0" class="about-company"> 
            <tr>
                <th style="width:128px"><p><?=$lang['Company'];?></p></th>
                <td><a href="companies.php?companyId=<?=$row_job['com_id'];?>"><?=$row_job['com_name'];?></a></td>
                <td valign="top" align="right" rowspan="5" style="width: 200px;">
                	<img src="employer/images/1585065498.png"  />
                </td>
            </tr>
            <tr>
                <th><p><?=$lang['Type'];?></p></th>
                <?php
				$type_id=$row_job['type_id'];
				mysqli_query($conn,'SET NAMES utf8');
				$select_type_company=mysqli_query($conn,"select * from tbl_company_type where type_id=$type_id");
				$row_type_company=mysqli_fetch_array($select_type_company);
				?>
                <td><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_type_company['type_name_kh'];else echo $row_type_company['type_name_en'];?></td>
            </tr>
            <tr>
                <th><p><?=$lang['Industry'];?></p></th>
                <td><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_job['ind_name_kh'];else echo $row_job['ind_name_en'];?></td>
            </tr> 
            <tr>
                <th><p><?=$lang['Employees'];?></p></th>
                <td><?=$row_job['com_emp'];?></td>
            </tr>
			<?php
				$id=$row_job['com_prov_id'];
				mysqli_query($conn,'SET NAMES utf8');
				$select_prov=mysqli_query($conn,"select * from tbl_province where prov_id=$id");
				$row_prov=mysqli_fetch_array($select_prov);
				?>
            <tr>
                <th><p><?=$lang['Location'];?></p></th>
                <td><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_prov['prov_name_kh'];else echo $row_prov['prov_name_en'];?></td>
            </tr>   
        </table>					
						<!--div style="text-align:right;margin-right: 22px;margin-top: 10px;"> 
							<a style="color: #3F5489;" class="view-job-employer" href="/pages/employer/index.jsp?employerId=10014671">View All Jobs From Employer</a>
						</div-->
		<div class="clear"></div>
            <table class="spec-tbl" border="0"> 
                <tr>
                    <th><p><?=$lang['Term'];?></p></th>
                    <?php $arr_term=array('',$lang['full_time'],$lang['Part_time'],$lang['Internship'],$lang['Volunteer'],$lang['Urgent']);?>
                    <td><?=$arr_term[$row_job['job_term']];?></td>
                    <th><p><?=$lang['Function'];?></p></th>
                    <td><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_job['func_name_kh'];else echo $row_job['func_name_en'];?></td>
                </tr>
                <tr>
                    <th scope="row"><p><?=$lang['Hiring'];?> </p></th>
                    <td><?=$row_job['job_hiring'];?></td>
                    <th scope="row"><p><?=$lang['Industry'];?></p></th>
                    <td><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_job['ind_name_kh'];else echo $row_job['ind_name_en'];?></td>
                </tr>
                <tr>
                    <th><p><?=$lang['Salary'];?></p></th>
                    <td><?=$row_job['job_salary'];?></td>
                    <th scope="row"><p><?=$lang['Qualification'];?></p></th>
                    <?php $arr_qual=array('',$lang['High_School'],$lang['Associate'],$lang['Bachelor'],$lang['Master'],$lang['Doctor'],$lang['Others']);?>
                    <td><?=$arr_qual[$row_job['job_qual']];?></td>
                </tr>
                <tr>
                    <th><p><?=$lang['Age'];?></p></th>
                    <td><?=$row_job['job_age'];?></td>
                     <th><p><?=$lang['Location'];?></p></th>
                    <td><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_job['prov_name_kh'];else echo $row_job['prov_name_en'];?></td>
                </tr>
                <tr>
                    <th><p><?=$lang['language'];?></p></th>
                    <td colspan="3"><?=$row_job['job_lang'];?></td>
                </tr>
                <tr>   
                    <th scope="row"><p><?=$lang['Publish_Date'];?></p></th>
                    <td><?php $date = $row_job['job_pub_date']; echo getDateFormat($date); ?></td>
					<th><p><?=$lang['Closing_Date'];?></p></th>
                    <td colspan="3"><?php $date = $row_job['job_close_date']; echo getDateFormat($date); ?></td>
                </tr> 
                
            </table>
            <?php
			$job_require = preg_replace("/\r\n|\r/", " <br/> ", $row_job["job_require"]);
  			$job_require = trim($job_require);
			
			$job_desc = preg_replace("/\r\n|\r/", " <br/> ", $row_job["job_desc"]);
  			$job_desc = trim($job_desc);
			
			$how_to_apply = preg_replace("/\r\n|\r/", " <br/> ", $row_job["how_to_apply"]);
  			$how_to_apply = trim($how_to_apply);
  			?>
            <h3 class="main-job-title-h3-2 main-job-w"><?=$lang['Company_Description'];?></h3>
            <p style="margin-left:28px; margin-top:12px;line-height:25px"><?=$job_desc;?></p>	
            
            <h3 class="main-job-title-h3-2 main-job-w"><?=$lang['Job_Requirements'];?></h3>				
            <p style="margin-left:28px; margin-top:12px;line-height:25px"><?=$job_require;?></p>
            
            <h3 class="main-job-title-h3-2 main-job-w"><?=$lang['How_to_Apply'];?></h3>
            <p style="margin-left:28px; margin-top:12px;line-height:25px"><?=$how_to_apply;?></p>
         
            <h3 class="main-job-title-h3-2 main-job-w"><?=$lang['Contact_Infomation'];?></h3>
            <table width="100%" border="0" class="main-job-w main-job-tab">
                <tr>
                    <th style="width:128px"><p><?=$lang['Contact_Name'];?></p></th>
                    <td><?=$row_job['contact_person'];?></td> 
                </tr> 
                 
                        <tr>
                            <th><p><?=$lang['Phone'];?></p></th>
                            <td><?=$row_job['phone'];?></td>
                        </tr>  
                        <tr>
                            <th><p><?= $lang['Email'];?></p></th>
                            <td><?=$row_job['email'];?></td>
                        </tr>
                         
                        <tr>
                            <th><p><?=$lang['Website'];?></p></th>
                            <td><a target="_blank" href="http://<?=$row_job['web'];?>"><?=$row_job['web'];?></a></td>
                            
                        </tr> 
                        <tr>
                            <th><p><?=$lang['Address'];?></p></th>
                            <td><?=$row_job['address'];?></td>
                        </tr>
                                                
            </table>
				
                <div class="applynow-btn main-job-w">
                    <a href="applynow.php?jobId=<?php echo $row_job['com_id']; ?>" class="btn btn-orange p30" id="applyjob1"><?=$lang['apply'];?></a>
                </div>
              
               
        </div>