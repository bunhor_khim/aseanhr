<div class="register">
    <div class="emp_header"><h3><?=$lang['Job_seekers'];?></h3></div>
    <div class="content_register">
    	<form id="register" method="post">
        	<div class="form-item">
            	<label><?=$lang['uname'];?>:</label>
                <label class="form-filed"><input  type="text" class="inputstyle w200" id="username" name="username" maxlength="30" validmessage="4~30 letters and numbers" ><span class="valid_info"><i style="color: red;padding-right:5px;">*</i> <?=$lang['a'];?></span>
                </label>
            </div>
            <div class="form-item">
            	<label><?= $lang['Email'];?>:</label>
                <label class="form-filed"><input type="email" class="inputstyle w200" id="email" name="email" maxlength="30" ><span class="valid_info"><i style="color: red;padding-right:5px;">*</i></span>
                </label>
            </div>
            <div class="form-item">
            	<label><?= $lang['password'];?>:</label>
                <label class="form-filed"><input type="password" class="inputstyle w200" id="password" name="password" maxlength="30" ><span class="valid_info"><i style="color: red;">*</i> <?=$lang['b'];?></span>
                </label>
            </div>
            <div class="form-item">
            	<label><?=$lang['conpassword'];?>:</label>
                <label class="form-filed"><input type="password" class="inputstyle w200" id="c_password" name="c_password" maxlength="30" ><span class="valid_info"><i style="color: red;">*</i></span>
                </label>
            </div>
            <div class="review-register paddingleft">
            <p class="colorblue">
				<input id="camhragreement" type="checkbox" value="1" name="camhragreement">
<b><?=$lang['agreement'];?></b>
			</p>
            </div>
            <div class="form-item clear">
            	<div id="sumbitdiv" class="paddingleft form-submit-btn">
            		<label class="form-button">
						<button id="registersubmit" disabled="disabled" class="btn btn-blue p30" type="button"> <?=$lang['Register'];?> </button>
					</label>
				</div>
            </div>
        </form>
    </div>
</div>