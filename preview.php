<?php 
session_start();
require_once("_aseanhr_admin/_config/connect.php");
if(isset($_GET['view']) && $_GET['view']){
	$view=$_GET['view'];
	$select_view=mysqli_query($conn,"select job_seeker_id from tbl_jobseeker where job_seeker_id=$view");
	
	$count_view=mysqli_num_rows($select_view);
	if($count_view>0){
		
	$select_user_profile=mysqli_query($conn ,"SELECT tbl_jobseeker . * , tbl_province.prov_name_en, tbl_province.prov_name_kh,tbl_cv.*
											FROM tbl_jobseeker
												INNER JOIN tbl_province ON tbl_jobseeker.location_of_residence = tbl_province.prov_id
												INNER JOIN tbl_cv ON tbl_jobseeker.job_seeker_id = tbl_cv.job_seeker_id
											WHERE tbl_jobseeker.job_seeker_id ='$view'");
	$row_user_profile=mysqli_fetch_array($select_user_profile);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
		 
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="shortcut icon" href="../images/icon.png" />
   	<link href="users/css/preview.css" rel="stylesheet" type="text/css" /> 
   
	<script src="users/js/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="users/js/base64.js"></script>
	<script type="text/javascript" src="users/js/sprintf.js"></script>
    <script src="users/js/jspdf.js" type="text/javascript"></script>
	<title>CV preview</title> 
	<meta name="description" content="" />
	<meta name="keywords" content="" />
    <script type="text/javascript">
	jQuery(document).ready(function(){
		var doc = new jsPDF();
		var specialElementHandlers = {
			'#resume-preview-content-text': function (element, renderer) {
				return true;
			}
		};
		$('#donwload_pdf').click(function () {
			doc.fromHTML($('#resumepreview').html(), 15, 15, {
				'width': 170,
					'elementHandlers': specialElementHandlers
			});
			doc.save('sample-file.pdf');
		});
	});
    </script>
</head>   

<body>
	
	<div id="preview-wrapper">
		<div class="resume-preview-top"> 
			<div class="print-download">
				<a id="resumeprint" href="#">Print</a>
			</div>
		</div>
         
        <div class="resume-preview-content-top"></div>
		<div id="resumepreview" class="resume-preview-content">
			<div style="margin-bottom: 30px"><img src="../images/asean_logo.png" width="100"/></div> 
			<!-- preview content start -->
			<div class="personal-info">
				<h2><?=$row_user_profile['first_name']?>&nbsp;<?=$row_user_profile['last_name']?></h2>
				<div class="personal-photo">
					
					<img src="../users/images/<?=$row_user_profile['photo']?>" style="max-width:100px;"/>
					
					
				</div>
			</div>
			
			<table border="0" class="resume-preview-tab">
            <?php
			$arr_sex=array('','Male','Female'); 
			?>
                <tr>
                    <th>Sex:</th>
                    <td><?=$arr_sex[$row_user_profile['sex']]?></td>
                </tr>
                <tr>
                    <th>Marital:</th>
                    <td><?=$row_user_profile['marital']?></td>
                </tr>
                <tr>
                    <th>Date of Birth:</th>
                    <td><?=$row_user_profile['date_of_birth']?></td>
                </tr>
                <tr>
                    <th>Location of Residence:</th>
                    <td><?=$row_user_profile['prov_name_en']?></td>
                </tr>
                <tr>
                    <th>Mobile:</th>
                    <td><?=$row_user_profile['phone']?></td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td><?=$row_user_profile['email']?></td>
                </tr>
                <tr>
                    <th>Address:</th>
                    <td><?=$row_user_profile['address']?></td>
                </tr>
			</table>
            
			<h3>Education</h3>
				<?php
			$select_education=mysqli_query($conn,"select * from tbl_education where job_seeker_id='$view'");
			while($row_edu=mysqli_fetch_array($select_education)):
			?>
				<ul class="work-experience bline">
					<li class="clear">
						<strong>Qualification:</strong>
						<p>Bachelor Degree</p>
					</li>
					<li class="clear">
						<strong>From: </strong>
						<p><?=$row_edu['from_date'];?> - <?=$row_edu['to_date'];?></p>
					</li>
					<li class="clear">
						<strong>Institute/University :</strong><p><?=$row_edu['university'];?></p>
					</li>
					<li class="clear">
						<strong>Field of Study:</strong><p><?=$row_edu['f_study'];?></p>
					</li>
					<li class="clear">
						<strong>Description:</strong>
						<p><?=$row_edu['des'];?></p>
					</li>
				</ul>
            <?php
			endwhile;
			?>
            
	        <h3 style="background:#eee;">Language</h3>
	        
				<table border="0" class="resume-preview-tab">
                <?php
				$select_language=mysqli_query($conn,"select * from tbl_language where job_seeker_id='$view'");
				while($row_language=mysqli_fetch_array($select_language)):
				?>
	  				<tr>
	                    <th><?=$row_language['language'];?></th>
                      	<?php $arr_lang_level=array('','Poor','Fair','Good','Excellent'); ?>
	                    <td><?=$arr_lang_level[$row_language['level']];?></td>
	  				</tr>
               	<?php
				endwhile;
				?>
				</table>
	        
	        	        
	        <h3 style="background:#eee;">Work Experience</h3>
			<?php
				$select_company=mysqli_query($conn,"select * from tbl_work_experience where job_seeker_id='$view'");
				while($row_company=mysqli_fetch_array($select_company)):
				?>
				<ul class="work-experience bline">
					<li class="clear">
						<strong>Company: </strong><p><?=$row_company['company'];?></p>
					</li>
                    <li class="clear">
						<strong>Time: </strong>
						<p>From <?=$row_company['start_date'];?> To <?=$row_company['to_date'];?></p>
					</li>
					<li class="clear">
						<strong>Position: </strong><p><?=$row_company['position'];?></p>
					</li>
					<li class="clear">
						<strong>Description: <?=$row_company['des'];?></strong>
						<p></p>
					</li>
				</ul>
	        <?php
			endwhile;
			?>
                    
			<h3 style="background:#eee;">Training</h3>
			<pre class="resume-preview-content-text"><?=$row_user_profile["training"]?></pre>
			
			<h3 style="background:#eee;">Hobby</h3>
			<pre class="resume-preview-content-text"><?=$row_user_profile["hobby"];?></pre>
			
			<h3 style="background:#eee;">Reference</h3>
			<pre class="resume-preview-content-text"><?=$row_user_profile["reference"];?></pre>	         
			<!-- preview content end -->
            <div class="copy-right">
            	<p style="font-size:16px; font-weight:bold; color:#06F;">COPY RIGHT ASEANHR</p>
                <p>Email : info@aseanhr.net</p>
                <p>Phone : 097 980 9697 / 086 639 267</p>
                <p>Address : #.., st.., Sangkat.., Khan.., Phnom Penh Cambodia</p>
            </div>
        </div>
        <div class="resume-preview-content-bottom"></div>
		<div class="resume-preview-close"><a href="javascript:window.close();"><button>Close</button></a></div>
    </div>
    <script type="text/javascript" src="../users/js/jquery.printArea.js"></script> 
    <script type="text/javascript">
    	$(document).ready(function(){ 
        	$("#resumeprint").click(function(){ 
        		$("#resumepreview").printArea(); 
        	});
		});
    </script>
	
</body>
</html>
	<?php
	}else{
		?>
		<script type="text/javascript">
		//window.location.href="/";
	</script>
		<?php
	}

}else{
	?>
	<script type="text/javascript">
	window.location.href="/";
</script>
	<?php
}

?>
