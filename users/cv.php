<?php 
session_start();
include '../langauge/common.php';
require_once("../_aseanhr_admin/_config/connect.php");
if($page=="logout"){
  session_destroy();
  ?>
  <script type="text/javascript">
        window.location.href="/";
  </script>
<?php   
}
if(isset($_SESSION['login_job_seeker']) && $_SESSION['login_job_seeker']){
	
	$job_seeker_log=$_SESSION["login_job_seeker"];
	$job_seeker_name=$_SESSION['job_seeker_name'];
	$job_seeker_id = $_SESSION['job_seeker_id'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../images/icon.png" />
<title>ASEANHR - Create CV</title>
<link href="../css/style1.css" rel="stylesheet" type="text/css" />
<link href="../css/color.css" rel="stylesheet" type="text/css"  />
<!-- multi select -->
<link rel="stylesheet" href="css/bootstrap-3.3.2.min.css" type="text/css">        
<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap-3.3.2.min.js"></script>

<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css">
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#example-getting-started').multiselect();
	});
</script>
<!-- multi select -->
<!-- jquery tab -->
<!--<script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>-->

<!-- date picker -->
<link rel="stylesheet" href="css/BeatPicker.min.css"/>
<script src="js/BeatPicker.min.js"></script>
<!-- end date picker -->
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="../js/engine1/style.css" />
<!-- End WOWSlider.com HEAD section -->
</head>

<body id="home">

<!-- alert message -->
<div class="err warning" id="add_err">
</div>
<div class="err success" id="err_success">
</div>
<!-- end alert message -->

<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('../includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('../includes/menu.php');?>
        </div>
       
        <div class="container">
        <!-- main -->
            <div class="user_main">
            	<div class="main_user">
            	<?php include("../includes/user_main.php");?>
                </div>
            </div>
        <!-- end main -->
            <div class="user_side">
            	<div class="side_user">
            	<?php include("../includes/user_side.php");?>
                </div>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("../includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('../includes/copy-right.php');?>
</div>
<script src="js/add_form.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/engine1/wowslider.js"></script>
<script type="text/javascript" src="../js/engine1/script.js"></script>
</body>
</html>

<?php
    }else{
        ?>
        <script type="text/javascript">
        window.location.href="/";
    </script>
        <?php
    }

?>