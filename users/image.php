<?php
require_once("../_aseanhr_admin/_config/connect.php");
session_start();
$path = "images/";

if(isset($_SESSION['login_job_seeker']) && $_SESSION['login_job_seeker']){
	
	$job_seeker_log=$_SESSION["login_job_seeker"];
	$job_seeker_name=$_SESSION['job_seeker_name'];
	$job_seeker_id = $_SESSION['job_seeker_id'];

	$valid_formats = array("jpg", "png", "gif", "bmp");
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{
			$name = $_FILES['photoimg']['name'];
			$size = $_FILES['photoimg']['size'];
			
			if(strlen($name))
				{
					list($txt, $ext) = explode(".", $name);
					if(in_array($ext,$valid_formats))
					{
					if($size<(1024*1024))
						{
							$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
							$tmp = $_FILES['photoimg']['tmp_name'];
							if(move_uploaded_file($tmp, $path.$actual_image_name))
								{
								$select_image= mysqli_query($conn,"select job_seeker_id,photo from tbl_jobseeker WHERE job_seeker_id='$job_seeker_id'");
								$row_image=mysqli_fetch_array($select_image);
								if($row_image['photo'] ==""){}else{
									unlink($path.$row_image['photo']);
								}
								mysqli_query($conn,"UPDATE tbl_jobseeker SET photo='$actual_image_name' WHERE job_seeker_id='$job_seeker_id'");
									
									echo "<img src='images/".$actual_image_name."'  class='preview'>";
								}
							else
								echo "failed";
						}
						else
						echo "Image file size max 1 MB";					
						}
						else
						echo "Invalid file format..";	
				}
				
			else
				echo "Please select image..!";
				
			exit;
		}
		
    }else{
        ?>
        <script type="text/javascript">
        	window.location.href="/";
    	</script>
        <?php
    }

?>