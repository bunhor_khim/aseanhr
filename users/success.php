<?php 
session_start();
require_once("../_aseanhr_admin/_config/connect.php");
include_once 'langauge/common.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../images/icon.png" />
<title>Jok Seeker Register</title>
<link href="../css/style1.css" rel="stylesheet" type="text/css" />
<link href="../css/color.css" rel="stylesheet" type="text/css"  />
<!-- jquery tab -->
<script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="../js/engine1/style.css" />
<!-- End WOWSlider.com HEAD section -->
</head>

<body  id="home">
<!-- alert message -->
<div class="err warning" id="add_err">
</div>
<div class="err success" id="err_success">
</div>
<!-- end alert message -->
<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('../includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('../includes/menu.php');?>
        </div>
        
        <div class="container">
        <!-- main -->
            <div class="main">
            	<div class="register">
                    <div class="register_header" id="title_verify_email"><h3>Verify Email.</h3></div>
                    <div class="content_register">
                        <h3 id="verify_email" style="width:650px;text-align:center;padding-bottom:20px">Verify Email Success!</h3>
                    </div>
                </div>
            </div>
        <!-- end main -->
            <div class="side">
            	<?php include('../includes/contact_side.php'); ?>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("../includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('../includes/copy-right.php');?>
</div>
<script type="text/javascript" src="../js/engine1/wowslider.js"></script>
<script type="text/javascript" src="../js/engine1/script.js"></script>
</body>
</html>