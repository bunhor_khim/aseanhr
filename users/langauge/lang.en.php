<?php
/* 
-----------------
Language: German
-----------------
*/

$lang = array();
// Menu
$lang['Edit']="Edit";
$lang['welcome']="Welcome to";
$lang['Add']="Add";
$lang['Save']="Save";
$lang['Please_Select']="Please Select";
$lang['and']="and";

$lang['MENU_HOME'] = 'Home';
$lang['MENU_ABOUT_US'] = 'About us';
$lang['Our_Services']= "Our Services";
$lang['MENU_CONTACT'] = 'Contact us';
$lang['Job_seeker']='Job Seeker';
$lang['Job_learning']='Job Learning';

//Job Seeker
$lang['cv']="All CV";
$lang['Create_CV']="Create CV";
$lang['Position']="Position";
$lang['Location']="Location";
$lang['Salary']="Salary";
$lang['update']="Update​ Time";
$lang['Action']="Action";
$lang['Profile']="Profile";
$lang['Logout']="Logout";

// create cv
$lang['Apply_for']="Apply For";
$lang['Section']="Section";
$lang['Personal_Information']="Personal Information";
$lang['First_Name']="First Name";
$lang['Last_Name']="Last Name";
$lang['Sex']="Sex";
$lang['dob']="Date Of Birth";
$lang['place_dob']="Place Of Birth";
$lang['Email']="Email";
$lang['Marital']="Marital";
$lang['Phone']="Phone";
$lang['Location_of_Residence']="Location of Residence";
$lang['Address']="Address";
$lang['Education']="Education";
$lang['Language']="Language";
$lang['Work_Experience']="Work Experience";
$lang['Additional_Skills']="Additional Skills";
$lang['Training']="Training";
$lang['Hobby']="Hobby";
$lang['Reference']="Reference";

$lang['Qualification']="Qualification";
$lang['From']="From";
$lang['To']="To";
$lang['University']="Institute/University";
$lang['Field_of_Study']="Field of Study";
$lang['Description']="Description";
$lang['delete']="Make To Delete";

$lang['Level']="Level";
$lang['Company']='Company';
$lang['Username']="Username";
$lang['Photo']="Photo";
$lang['Browe_Photo']="Browe Photo";
$lang['Upload']="Upload";

$lang['male']="Male";
$lang['female']="Female";
$lang['Single']="Single";
$lang['Married']="Married";

$lang['employer']="Employer";
$lang['Profile']="Profile";
$lang['Create_CV']="Create CV";
$lang['Company_profile']='Company Profile';
$lang['new_Jobs']='Post a new Jobs';
$lang['hotline']="Service hot line";
$lang['em']="Email";
$lang['tels']="Tel";
$lang['Hours']="Operating Hours";
$lang['addr']="Address";

// register
$lang['Job_seekers']='Job Seeker Register';
$lang['uname']="Username";
$lang['password']="Password";
$lang['conpassword']="Confirm Password";
$lang['a']="4~30 letters and numbers";
$lang['b']="The length should be longer than 6";
$lang['agreement']="I have read and agreed to the Terms of Service.";
$lang['Register']="Register";
$lang['forget']="Forget Password";
$lang['send']="Send Email";

$lang['Negotiable']="Negotiable";

//Qualification
$lang['High_School']="High School";
$lang['Associate']="Associate's Degree";
$lang['Bachelor']="Bachelor Degree";
$lang['Master']="Master Degree";
$lang['Doctor']="Doctor Degree";
$lang['Others']="Others";

?>