<?php
/* 
-----------------
Language: German
-----------------
*/

$lang = array();
// Menu

$lang['Edit']="កែប្រែ";
$lang['welcome']="សូមស្វាគមន៍";
$lang['Add']="បន្ថែម";
$lang['Save']="រក្សាទុក";
$lang['Please_Select']="សូមជ្រើសរើស";
$lang['and']="និង";

$lang['MENU_HOME'] = 'ទំព័រដើម';
$lang['MENU_ABOUT_US'] = 'អំពីយើងខ្ញុំ';
$lang['Our_Services']= "សេវាកម្មយើង";
$lang['MENU_CONTACT'] = 'ទំនាក់ទំនងយើងខ្ញុំ';
$lang['Job_seeker']='អ្នកសែ្វងរកការងារ';
$lang['Job_learning']='ការរៀនការងារ';

//Job Seeker
$lang['cv']="ជីវប្រវត្តិ ទាំងអស់";
$lang['Create_CV']="បង្កើតជីវប្រវត្តិ";
$lang['Position']="មុខដំណែង";
$lang['Location']="ទីតាំង";
$lang['Salary']="ប្រាក់ខែ";
$lang['update']="ថ្ងៃកែសម្រួល";
$lang['Action']="សកម្មភាព";
$lang['Profile']="ព័ត៌មានផ្ទាល់ខ្លួន";
$lang['Logout']="ចាកចេញ";

// create cv
$lang['Apply_for']="ដាក់ពាក្យសម្រាប់";
$lang['Section']="មុខងារ";
$lang['Personal_Information']="ពត៏មានផ្ទាល់ខ្លួន";
$lang['First_Name']="នាមត្រគោល";
$lang['Last_Name']="នាមខ្លួន";
$lang['Sex']="ភេទ";
$lang['dob']="ថ្ងៃ ខែ ឆ្នាំកំណើត";
$lang['place_dob']="ទីកន្លែងកំណើត";
$lang['Email']="អ៊ីមែល";
$lang['Marital']="ស្ថានភាពគ្រួសារ";
$lang['Phone']="ទូរស័ព្ទ";
$lang['Location_of_Residence']="អាសយដ្ឋាន បច្ចុប្បន្ន";
$lang['Address']="អាសយដ្ឋាន";
$lang['Education']="ការអប់រំ";
$lang['Language']="ភាសា";
$lang['Work_Experience']="បទពិសោធការងារ";
$lang['Additional_Skills']="ជំនាញបន្ថែម";
$lang['Training']="វគ្គបណ្តុះបណ្តាល";
$lang['Hobby']="ចំណង់ចំណូលចិត្ត";
$lang['Reference']="សេចក្តីយោង";

$lang['Qualification']='កម្រិតវប្បធម៌';
$lang['From']="តាំងពី";
$lang['To']="ទៅ";
$lang['University']="វិទ្យាស្ថាន​/សាកលវិទ្យាល័យ";
$lang['Field_of_Study']="មុខវិជ្ជាសិក្សា";
$lang['Description']="ពិពណ័នា";
$lang['delete']="ធីកដើម្បីលុប";

$lang['Level']="កំរិត";
$lang['Company']='ក្រុមហ៊ុន';
$lang['Username']="ឈ្មោះ";
$lang['Photo']="រូបថត";
$lang['Browe_Photo']="ជ្រើសរើស រូបថត";
$lang['Upload']="ផ្ទុកឡើង";

$lang['male']="ប្រុស";
$lang['female']="ស្រី";
$lang['Single']="នៅលីវ";
$lang['Married']="រៀបការ";

$lang['employer']="និយោជក";
$lang['Profile']="ព័ត៌មានផ្ទាល់ខ្លួន";
$lang['Create_CV']="បង្កើតជីវប្រវត្តិ";
$lang['Company_profile']='ប្រវត្តិក្រុមហ៊ុន';
$lang['new_Jobs']='ផ្សព្វផ្សាយការងារថ្មី';

$lang['hotline']="លេខទូរស័ព្ទទំនាក់ទំនងបន្ទាន់";
$lang['em']="អ៊ីមែល";
$lang['tels']="ទូរស័ព្ទ";
$lang['Hours']="ម៉ោងធើ្វការ";
$lang['addr']="អាសយដ្ឋាន";

// register
$lang['Job_seekers']='អ្នកសែ្វងរកការងារ ចុះឈ្មោះ';
$lang['uname']="ឈ្មោះ";
$lang['password']="លេខសំងាត់";
$lang['conpassword']="បញ្ជាក់លេខសម្ងាត់";
$lang['a']="៤~៣០ អក្សរ និង​លេខ";
$lang['b']="ប្រវែងគួតែវែងជាង​៦តួ​";
$lang['agreement']="ខ្ញុំបានអាននិង យល់ព្រមពី​ ល័ក្ខខ័ណ្ឌនៃសេវាកម្ម។";
$lang['Register']="ចុះឈ្មោះ";
$lang['forget']="ស្នើរសុំលេខសំងាត់ថ្មី";
$lang['send']="ផ្ញើរអ៊ីមែល";

$lang['Negotiable']="តម្លៃចចារបាន";

//Qualification
$lang['High_School']="វិទ្យាល័យ";
$lang['Associate']="ថ្នាក់បរិញ្ញាបត្ររង";
$lang['Bachelor']="ថ្នាក់បរិញ្ញាបត្រ";
$lang['Master']="ថ្នាក់អនុបណ្ទិត";
$lang['Doctor']="ថ្នាក់បណ្ឌិត";
$lang['Others']="ថ្នាក់ផ្សេងៗ";

?>