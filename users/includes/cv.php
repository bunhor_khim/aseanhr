<?php
$select_cv="select * from tbl_cv where tbl_cv.job_seeker_id='$job_seeker_id'";
$cv=$conn->query($select_cv);
?>
<!-- main content start -->
    <div class="main-public-title">
       <div class="emp_header"><h3><?= $lang['cv'];?></h3></div>
        <div class="normal-list-btn">
            <a href="/users/cv.php" class="btn2 btn-blue"><?= $lang['Create_CV'];?></a>
        </div>
        <div class="normal-list">
            <table border="0" class="normal-tab">
                <tr>
                    <th><?= $lang['Position'];?></th> 
                    <!--<th style="width: 90px"><?php // $lang['Location'];?></th>-->
                    <th style="width: 100px"><?= $lang['Salary'];?></th>
                    <th style="width: 90px"><?= $lang['update'];?></th>
                    <th style="width: 100px"><?= $lang['Action'];?></th>
                </tr>
                <?php 
				while($row_cv=mysqli_fetch_array($cv)){
				?>
                    <tr>
                        <td>
                            <?=$row_cv['position'];?>
                        </td> 
                        <!--<td>
                            <strong><?php //$row_cv['local'];?></strong>
                            
                        </td>-->
                        <td>
                            <?=$row_cv['salary'];?> 
                        </td>
                        <td nowrap="nowrap">
                            <?=$row_cv['update_cv'];?>
                        </td>
                        <td nowrap="nowrap" class="action">
                            <a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/users/preview.php?cv_id=<?=$row_cv['cv_id'];?>" target="_blank">Preview</a> |
                            <a title="Edit" href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/users/edit_cv.php?cv_id=<?=$row_cv['cv_id'];?>"><img src="../employer/images/edit.jpg"  /></a>  |
                            <a title="Delete" href="javascript:delete_id(<?=$row_cv['cv_id'];?>)"><img src="../employer/images/b_drop.png"  /></a>
                        </td>
                    </tr>
                <?php
				}
				?>
            </table>
        </div>
    </div>
    <!-- main content end -->