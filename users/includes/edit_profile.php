<?php
$select_province=mysqli_query($conn,"select * from tbl_province order by prov_name_en asc");

$select_profile=mysqli_query($conn,"select * from tbl_jobseeker where job_seeker_id = $job_seeker_id");
$row_profile=mysqli_fetch_array($select_profile);
?>

<!-- main content start -->
<div class="main-public-title"> 
    <div class="emp_header"><h3><?= $lang['Profile'];?></h3></div>
    <div class="form-content clear">
        <form id="usersave" method="post" action="image.php" enctype="multipart/form-data">
            <div class="user-form-item clear">
                <label><?= $lang['First_Name'];?> <?=$lang['and'];?> <?=$lang['Last_Name'];?> </label>
                <label class="form-filed"><input id="firstname" type="text" name="firstname" class="inputstyle w200" value="<?= $row_profile['first_name']?>" maxlength="60" require="true"/></label>

                <label class="form-filed"><input id="lastname" type="text" name="lastname" class="inputstyle w200" value="<?= $row_profile['last_name']?>" maxlength="30" require="true"/></label>

            </div>
            <div class="user-form-item clear">
                <label><?= $lang['Photo'];?>: </label>
                <label class="form-filed">
               
				<?php
				if($row_profile['photo']==""){
				?>
                	<div id='preview'>
                		<img width="140" src="images/no_photo.png" style="border:1px solid #eee;"/>
                    </div>
                <?php
				}else{
				?> 
                	<div id='preview'>
                		<img width="140" src="images/<?= $row_profile['photo']?>"/>
                    </div>
               	<?php
                }
				?>
                </label>
            </div>
            
            <div class="user-form-item clear">
                <label><?= $lang['Browe_Photo'];?> :</label>
                <label class="form-filed">
                    <input type="file" name="photoimg" id="photoimg" />
    				<input type="button" class="btn2 btn-blue p12" id="sumit" name="sumit" value="<?= $lang['Upload'];?>">
                </label>
            </div>
            <div class="user-form-item clear">
                <label>Sex:</label>
                <label class="form-filed">
                    <select id="sex"  name="sex" class="inputstyle w120" require="true">  
                        <option value="" ><?= $lang['Please_Select']; ?></option>  
                        <option <?php if($row_profile['sex']=='1'){echo "selected='selected'";}?> value="1"><?= $lang['male'];?></option>  
                        <option <?php if($row_profile['sex']=='2'){echo "selected='selected'";}?> value="2"><?= $lang['female'];?></option>  
                    </select>
                </label>
            </div>
            
            <div class="user-form-item clear">
                <label><?= $lang['dob'];?>​​​ :</label>
                <label class="form-filed">
                    <input type="text" id="bd" name="bd" data-beatpicker="true"​  value="<?= $row_profile['date_of_birth']?>"/>
                </label>
            </div>
            
            <div class="user-form-item clear">
                <label><?= $lang['place_dob'];?>​​​ :</label>
                <label class="form-filed">
                    <textarea type="text" id="place_of_bd" name="place_of_bd" class="w450h100" style="width:400px; height:80px;"><?= $row_profile['plase_of_bitth']?></textarea>
                </label>
            </div>
            
            <div class="user-form-item clear">
                <label><?= $lang['Email'];?>:</label>
                <label class="form-filed">
                    <strong><?= $row_profile['email']; ?></strong>
                </label>
            </div>
            
            <div class="user-form-item clear">
                <label><?= $lang['Marital'];?>:</label>
                <label class="form-filed">
                    <select id="marital"  name="marital" class="inputstyle w210">  
                        <option value=""><?= $lang['Please_Select']; ?></option>  
                        <option <?php if($row_profile['marital']=='Single'){echo "selected='selected'";}?> value="Single"><?= $lang['Single'];?></option>  
                        <option <?php if($row_profile['marital']=='Married'){echo "selected='selected'";}?>value="Married"><?= $lang['Married'];?></option>
                    </select>
                </label>
            </div>
            
            <div class="user-form-item clear">
                <label><?= $lang['Phone']; ?> :</label>
                <label class="form-filed">
                	<input id="phone" type="text" name="phone" class="inputstyle w200" value="<?= $row_profile['phone']?>" maxlength="30"/>
                </label>

            </div>
            
            <div class="user-form-item clear">
                <label><?= $lang['Location_of_Residence']; ?>:</label> 
                <label class="form-filed">
                    <select id="province"  name="province" class="inputstyle w210" required>
                        <option value="" ><?= $lang['Please_Select']; ?></option>  
                        <?php
						while($row_province=mysqli_fetch_array($select_province)){
						?>
                        <option  <?php if($row_profile['location_of_residence']==$row_province["prov_id"]){echo "selected='selected'";}?> value="<?php echo $row_province["prov_id"]; ?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_province['prov_name_kh'];else echo $row_province['prov_name_en'];?></option>
                        <?php
                        }
						?>
                    </select>
                </label>

            </div>
            <div class="user-form-item clear">
                <label><?= $lang['Address']; ?>:</label>
                <label class="form-filed">
                    <textarea name="address" maxlength="2000" class="w450h100" style="width:400px; height:80px;"><?= $row_profile['address']?></textarea>
              	</label>

            </div>
            <div class="form-item clear">
                <div class="paddingleft">
                    <button  id="edit_profiles" class="btn btn-orange p30" type="button"><?= $lang['Save']; ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- main content end -->