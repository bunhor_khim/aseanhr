<?php
// cv info

$row_cv_info=mysqli_fetch_array($select_cv_info);

//personal info
$select_person_info = mysqli_query($conn,"select tbl_jobseeker.*,tbl_province.prov_name_en,tbl_province.prov_name_kh
	from tbl_jobseeker
	inner join tbl_province
	on tbl_jobseeker.location_of_residence=tbl_province.prov_id 
 	where tbl_jobseeker.job_seeker_id='$job_seeker_id'"); 
$row_person_info=mysqli_fetch_array($select_person_info);


if(!empty($_POST['save_cv'])){
	$position_apply=$_POST['position_apply'];
	$section=$_POST['section'];
	$local=$_POST['local'];
	$salary=$_POST['salary'];

	$skill=$_POST['skill'];
	$train=$_POST['train'];
	$hobby=$_POST['hobby'];
	$reference=$_POST['reference'];
		
	$update_cv = mysqli_query($conn,"update tbl_cv set additional_skills='$skill',training='$train',hobby='$hobby',reference='$reference',position='$position_apply',section='$section',local='$local',salary='$salary',update_cv=now()  where cv_id='$cv_id'");
	
	
	if($update_cv){
		?>
        <script type="text/javascript">
			$("#err_success").css({display:'block'});
			$("#err_success").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Well done!   </strong>thank U");
			setTimeout(function(){$('#err_success').css({display:'none'});}, 5000);
			window.location.href="/users/";
		</script>
        <?php
	}
	
	if(!empty($_POST['local'])){
		mysqli_query($conn,"delete from tbl_cv_local where cv_id=$cv_id");
		
		foreach($local as $locals):
		$sql="insert into tbl_cv_local (province_id,jobseeker_id,cv_id)values('$locals','$job_seeker_id','$cv_id')";		
		$conn->query($sql);
		endforeach;
	}
	
	// first delete the records marked for deletion. Why? Because we don't want to process them in the code below
	if( !empty($_POST['delete_lang']) and is_array($_POST['delete_lang'])) {
		// you can optimize below into a single query, but let's keep it simple and clear for now:
		foreach($_POST['delete_lang'] as $id) {
			$sql = "DELETE FROM tbl_language WHERE lang_id=$id";
			$conn->query($sql);
		}
	}
	
	// now edit them
	$update_language="SELECT * FROM tbl_language where land_cv_id='$cv_id'";
	$result_update_language = $conn->query($update_language);
	while($product = mysqli_fetch_array($result_update_language)) {
		// remember how we constructed the field names above? This was with the idea to access the values easy now
		$sql = "UPDATE tbl_language SET level='".$_POST['level'.$product['lang_id']]."', language='".$_POST['languages'.$product['lang_id']]."'
		WHERE lang_id='$product[lang_id]'";		
		$conn->query($sql);
	}
	// (feel free to optimize this so query is executed only when a product is actually changed)
	
	// adding new language
	if(!empty($_POST['languages'])) {
		foreach($_POST['languages'] as $cnt => $languages) {
			$sql = "INSERT INTO tbl_language (job_seeker_id,land_cv_id, level, language ) VALUES ('$job_seeker_id','$cv_id', '".$_POST['level'][$cnt]."','$languages');";
			$conn->query($sql);
		}
	}
	
	
	// (work experiance)first delete the records marked for deletion. Why? Because we don't want to process them in the code below
	if( !empty($_POST['delete_experiance']) and is_array($_POST['delete_experiance'])) {
		// you can optimize below into a single query, but let's keep it simple and clear for now:
		foreach($_POST['delete_experiance'] as $id) {
			$sql = "DELETE FROM tbl_work_experience WHERE work_ex_id=$id";
			$conn->query($sql);
		}
	}
	
	// now edit Work Experience
	$update_experience="SELECT * FROM tbl_work_experience where ex_cv_id='$cv_id'";
	$result_update_experience = $conn->query($update_experience);
	while($row_experience = mysqli_fetch_array($result_update_experience)) {
		// remember how we constructed the field names above? This was with the idea to access the values easy now
		$sql = "UPDATE tbl_work_experience SET company='".$_POST['work_company'.$row_experience['work_ex_id']]."', start_date='".$_POST['work_time_from'.$row_experience['work_ex_id']]."', to_date='".$_POST['work_time_to'.$row_experience['work_ex_id']]."', position='".$_POST['work_position'.$row_experience['work_ex_id']]."', des='".$_POST['work_description'.$row_experience['work_ex_id']]."'
		WHERE work_ex_id='$row_experience[work_ex_id]'";		
		$conn->query($sql);
	}
	// (feel free to optimize this so query is executed only when a product is actually changed)
	
	
	// adding new Work Experience
	if(!empty($_POST['work_company'])) {
		foreach($_POST['work_company'] as $cnt => $work_company) {
			$sql = "INSERT INTO tbl_work_experience (job_seeker_id,ex_cv_id, company, start_date, to_date, position,des ) VALUES ('$job_seeker_id','$cv_id', '$work_company', '".$_POST['work_time_from'][$cnt]."', '".$_POST['work_time_to'][$cnt]."', '".$_POST['work_position'][$cnt]."', '".$_POST['work_description'][$cnt]."' )";
			$conn->query($sql);
		}
	}
	
	// (Education)first delete the records marked for deletion. Why? Because we don't want to process them in the code below
	if( !empty($_POST['delete_edu']) and is_array($_POST['delete_edu'])) {
		// you can optimize below into a single query, but let's keep it simple and clear for now:
		foreach($_POST['delete_edu'] as $id) {
			$sql = "DELETE FROM tbl_education WHERE edu_id=$id";
			$conn->query($sql);
		}
	}
	
	// now edit Education
	$update_education="SELECT * FROM tbl_education where edu_cv_id='$cv_id'";
	$result_update_education = $conn->query($update_education);
	while($row_education = mysqli_fetch_array($result_update_education)) {
		// remember how we constructed the field names above? This was with the idea to access the values easy now
		$sql = "UPDATE tbl_education SET qualification='".$_POST['qualification'.$row_education['edu_id']]."', from_date='".$_POST['edu_time_from'.$row_education['edu_id']]."', to_date='".$_POST['edu_time_to'.$row_education['edu_id']]."', university='".$_POST['school'.$row_education['edu_id']]."',f_study='".$_POST['major'.$row_education['edu_id']]."', des='".$_POST['edu_des'.$row_education['edu_id']]."'
		WHERE edu_id='$row_education[edu_id]'";		
		$conn->query($sql);
	}
	// (feel free to optimize this so query is executed only when a product is actually changed)
	
	
	// adding new Education
	if(!empty($_POST['school'])) {
		foreach($_POST['school'] as $cnt => $school) {
			$sql = "INSERT INTO tbl_education (job_seeker_id, edu_cv_id, qualification, from_date, to_date, university, f_study, des ) VALUES ('$job_seeker_id','$cv_id','".$_POST['qualification'][$cnt]."', '".$_POST['edu_time_from'][$cnt]."', '".$_POST['edu_time_to'][$cnt]."', 'school', '".$_POST['major'][$cnt]."', '".$_POST['edu_des'][$cnt]."' )";
			$conn->query($sql);
		}
	}

}

// select language
$language="SELECT * FROM tbl_language where job_seeker_id='$job_seeker_id' and land_cv_id='$cv_id' ORDER BY lang_id";
$result_language = $conn->query($language);

// select tbl_education
$education="SELECT * FROM tbl_education where job_seeker_id='$job_seeker_id' and edu_cv_id='$cv_id' ORDER BY edu_id";
$result_education = $conn->query($education);

?>
<form method="post" enctype="multipart/form-data"> 
	
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Apply_for'];?></h3>
        <div id="apply_for" class="resume-edit-content clear">
        
            <div id="cv_language" class="form-content" style="border-top:1px solid #e1e1e1;">
                <div class="user-form-item clear">
                    <label><?= $lang['Position'];?>:</label>
                    <label class="form-filed">
                        <input type="text" id="postion_apply" name="position_apply"  class="inputstyle w200" value="<?=$row_cv_info['position'];?>" required="required"/>
                    </label>

                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Section']; ?>:</label>
                    <label class="form-filed">
                        <select id="section"  name="section" class="inputstyle w210" required>  
                            <option value=""><?= $lang['Please_Select']; ?></option>  
                             <?php 
							$select_section="select * from tbl_function order by func_name_en asc";
							$section=$conn->query($select_section);
							while($row_section=mysqli_fetch_array($section)):
							?>
                            <option <?php if($row_cv_info['section']==$row_section['func_id']){echo 'selected="selected"';}?> value="<?=$row_section['func_id'];?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_section['func_name_kh'];else echo $row_section['func_name_en'];?></option>
                            <?php
							endwhile;
							?>
                        </select>
                    </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Location']; ?>:</label>
                    <label class="form-filed">                    <ul class="multi_check_box">
                    <?php 
                    $select_local="select * from tbl_province order by prov_name_en asc";
                    $local=$conn->query($select_local);
                    while($row_local=mysqli_fetch_array($local)):
					
					$select_multi_local=mysqli_query($conn,"select * from tbl_cv_local where jobseeker_id = $job_seeker_id and cv_id=$cv_id");
					?>
                        <li>
                            <a tabindex="0">
                                <label class="checkbox">
                                    <input type="checkbox" name="local[]" <?php while($row_multi_local=mysqli_fetch_array($select_multi_local)){if($row_multi_local['province_id']==$row_local['prov_id']){echo"checked='checked'";} }?> value="<?=$row_local['prov_id'];?>"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo $row_local['prov_name_kh'];else echo $row_local['prov_name_en'];?>
                                </label>
                            </a>
                        </li>
                    <?php
                    endwhile;
                    ?>
                    </ul>
                        </select>
                    </label>

                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Salary']; ?>:</label>
                    <label class="form-filed">
                        <input type="text" id="salary" name="salary" value="<?=$row_cv_info['salary'];?>"  class="inputstyle w200" required="required"/>
                    </label>
                </div>
                
            </div>
            
        </div> 
    </div>
    <!-- end column cv -->
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Personal_Information']; ?></h3>
        <div class="resume-edit-content">
            <div class="form-content">
                <?php
					if($row_person_info['photo'] ==""){
				?>
                    <div class="personal-photo">
                        <img src="images/no_photo.png" alt="Photo" width="140">
                    </div>
                <?php }else{ ?>
                    <div class="personal-photo">
                        <img src="images/<?= $row_person_info['photo']; ?>" alt="Photo" width="140">
                    </div>
                <?php } ?>
                <div class="user-form-item clear">
                    <label><?= $lang['First_Name']; ?> <?=$lang['and']?> <?= $lang['Last_Name']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['first_name']; ?> </label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['last_name']; ?> </label>
                </div>
                <?php
				
				$arr_sex=array("",$lang['male'],$lang['female']) 
				?>
                <div class="user-form-item clear">

                    <label><?= $lang['Sex']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?php if($row_person_info['sex']!="") echo $arr_sex[$row_person_info['sex']]; ?> </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['dob']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['date_of_birth']; ?> </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['place_dob']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['plase_of_bitth']; ?> </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Email']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['email']; ?> </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Marital']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['marital']; ?> </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Phone']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['phone']; ?> </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Location_of_Residence']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['prov_name_en']; ?> </label>
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Address']; ?> :</label>
                    <label id="firstname" class="form-filed"> <?= $row_person_info['address']; ?> </label>
                </div>
                
            </div>
            <div class="resume-add-item">
            	<a href="http://<?php echo $_SERVER["HTTP_HOST"]; ?>/users/profiles.php"><?= $lang['Edit']; ?></a>
            </div>
        </div>
    </div>
    <!-- end column cv -->
    
    
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Education']; ?></h3>
        <div id="cv_education_div" class="resume-edit-content clear">
        	<?php while($row_edu = mysqli_fetch_array($result_education)):?>
            <div id="cv_education" class="form-content" style="border-top:1px solid #e1e1e1;">
                
                <div class="user-form-item clear">
                    <label><?= $lang['Qualification'];?> :</label>
                    <label class="form-filed">
                        <select id="qualification"  name="qualification<?=$row_edu['edu_id'];?>" class="inputstyle w250"  required>  
                            <option value=""><?= $lang['Please_Select']; ?></option>  
                            <option <?php if($row_edu['qualification']=='1'){echo 'selected="selected"';}?> value="1"><?=$lang['High_School'];?></option>  
                            <option <?php if($row_edu['qualification']=='2'){echo 'selected="selected"';}?> value="2"><?=$lang['Associate'];?></option>  
                            <option <?php if($row_edu['qualification']=='3'){echo 'selected="selected"';}?> value="3"><?=$lang['Bachelor'];?></option>  
                            <option <?php if($row_edu['qualification']=='4'){echo 'selected="selected"';}?> value="4"><?=$lang['Master'];?></option>
                            <option <?php if($row_edu['qualification']=='5'){echo 'selected="selected"';}?> value="5"><?=$lang['Doctor'];?></option>    
                            <option <?php if($row_edu['qualification']=='6'){echo 'selected="selected"';}?> value="6"><?=$lang['Others'];?></option>
                        </select>
                    </label>
                </div>
                
                <div class="user-form-item clear">
                    <label><?= $lang['From'];?> :</label>
                    <label class="form-filed">
                    	<input type="text" id="cv-form-edu-time-from" name="edu_time_from<?=$row_edu['edu_id'];?>" data-beatpicker="true"​ placeholder="yyyy/mm/dd" value="<?=$row_edu['from_date'];?>" required="required" style="width:100px;"/>
                    </label>
                    <label class="form-filed"><?= $lang['To'];?> :</label>
                    <label class="form-filed">
                    	<input type="text" id="cv-form-edu-time-to" name="edu_time_to<?=$row_edu['edu_id'];?>" data-beatpicker="true"​ placeholder="yyyy/mm/dd" value="<?=$row_edu['to_date'];?>" required="required" style="width:100px;"/>
                    </label>
                </div> 
                <div class="user-form-item clear">
                    <label><?= $lang['University'];?> :</label>
                    <label class="form-filed">
                    	<input id="edu_school" type="text" name="school<?=$row_edu['edu_id'];?>" class="inputstyle w250" value="<?=$row_edu['university'];?>" maxlength="250" require="true" required="required"/>
                    </label>
    
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Field_of_Study'];?> :</label>
                    <label class="form-filed">
                    	<input id="jymajor" type="text" name="major<?=$row_edu['edu_id'];?>" class="inputstyle w250" value="<?=$row_edu['f_study'];?>" maxlength="120" required="required"/>
                    </label>
    
                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Description'];?>:</label>
                    <label class="form-filed">
                        <textarea name="edu_des<?=$row_edu['edu_id'];?>" maxlength="2000" class="w450h100" style="width:255px; height:100px;"><?=$row_edu['des'];?></textarea>
                    </label>
                </div>
                <div class="user-form-item clear">
                <label><?= $lang['delete'];?>: </label>
                <label class="form-filed">
                	<input type="checkbox"  class="inputstyle w180" name="delete_edu[]" value="<?=$row_edu['edu_id']?>">
                </label>
                </div>
            </div>
         	<?php endwhile;?>
            <div id="edu_new_rows"></div>
            
        <div class="resume-add-item" id="cv_education"><a style="cursor:pointer" id="cv_education_add">+ <?= $lang['Add']; ?></a></div>
    </div> 
    </div> 
    <!-- end column cv -->
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Language']; ?></h3>
        <div id="cv_language_div" class="resume-edit-content clear">
        
            <?php while($row_language = mysqli_fetch_array($result_language)):?>
			
            <div id="cv_language" class="form-content" style="border-top:1px solid #e1e1e1;">
                <input type="hidden" id="lang_id_10186003" name="lang_id" value="10186003"/>
                <div class="user-form-item clear">
                    <label><?= $lang['Language']; ?> :</label>
                    <label class="form-filed">
                        <input type="text" class="inputstyle w200" id="languages" name="languages<?= $row_language['lang_id'];?>" value="<?= $row_language['language'];?>" required="required"/>
                    </label>

                </div>
                <div class="user-form-item clear">
                    <label><?= $lang['Level']; ?> :</label>
                    <label class="form-filed">
                        <select id="languagelevel"  name="level<?= $row_language['lang_id'];?>" class="inputstyle w180" required>  
                            <option value=""><?= $lang['Please_Select']; ?></option>  
                            <option <?php if($row_language['level']=='1'){echo 'selected="selected"';}?>value="1"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo "ខ្សោយ";else echo "Poor​";?></option>
                            <option  <?php if($row_language['level']=='2'){echo 'selected="selected"';}?>value="2"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo "មធ្យម";else echo "Fair​";?></option>  
                            <option  <?php if($row_language['level']=='3'){echo 'selected="selected"';}?>value="3"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo "ល្អ";else echo "Good​";?></option>  
                            <option  <?php if($row_language['level']=='4'){echo 'selected="selected"';}?>value="4"><?php if(isset($_SESSION['lang']) && $_SESSION['lang'] =='km')echo "ល្អណាស់";else echo "Excellent";?></option>
                        </select>
                    </label>
                </div>
                <div class="user-form-item clear">
                <label><?= $lang['delete'];?>​ :</label>
                <label class="form-filed">
                	<input type="checkbox"  class="inputstyle w180" name="delete_lang[]" value="<?=$row_language['lang_id']?>">
                </label>
                </div>
            </div>
			<?php endwhile;?>
            <div id="language_new_rows"></div>
            <div class="resume-add-item"><a style="cursor:pointer;" id="cv_language_add">+ <?= $lang['Add']; ?></a></div>
        </div> 
    </div>
    <!-- end column cv -->
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Work_Experience']; ?></h3>
        <div id="cv_work_experience_div" class="resume-edit-content clear">
            <?php
			// select work experiance
			$work_ex="SELECT * FROM `tbl_work_experience` where job_seeker_id='$job_seeker_id' and ex_cv_id='$cv_id' ORDER BY work_ex_id";
			$result_work = $conn->query($work_ex);
			while($row_worl_ex = mysqli_fetch_array ($result_work) ): 
			?>
            <div id="cv_workexperience_" class="form-content" style="border-top:1px solid #e1e1e1;">
                <div class="user-form-item clear">
                    <label><?= $lang['Company'];?>:</label>
                    <label class="form-filed">
                    	<input id="work_company" type="text" name="work_company<?=$row_worl_ex['work_ex_id']?>" class="inputstyle w250"  maxlength="200" value="<?=$row_worl_ex['company']?>" required="required"/>
                    </label>
                </div>
                
                <div class="user-form-item clear">
                    <label><?= $lang['From'];?> :</label>
                    <label class="form-filed">
                    	<input type="text" id="cv-form-work-time-from" name="work_time_from<?=$row_worl_ex['work_ex_id']?>" data-beatpicker="true"​ placeholder="yyyy/mm/dd" value="<?=$row_worl_ex['start_date']?>" required="required" style="width:100px;"/>
                    </label> 
                    <label class="form-filed"><?= $lang['To'];?> :</label>
                    <label class="form-filed">
                    	<input type="text" id="cv-form-work-time-to" name="work_time_to<?=$row_worl_ex['work_ex_id']?>" data-beatpicker="true"​ placeholder="yyyy/mm/dd" value="<?=$row_worl_ex['to_date']?>" required="required" style="width:100px;"/>
                    </label>
                </div>
                
                <div class="user-form-item clear">
                    <label><?= $lang['Position'];?>:</label>
                    <label class="form-filed">
                    <input id="work-position-10207263" type="text" name="work_position<?=$row_worl_ex['work_ex_id']?>" class="inputstyle w250" value="<?=$row_worl_ex['position']?>" maxlength="100" required="required"/>
                    </label>
    
                </div>
                
                <div class="user-form-item clear">
                    <label><?= $lang['Description'];?>:</label>
                    <label class="form-filed">
                        <textarea name="work_description<?=$row_worl_ex['work_ex_id']?>" maxlength="1000" class="w450h100" style="width:255px; height:100px;"><?=$row_worl_ex['des']?></textarea>
                    </label>
                </div>
                <div class="user-form-item clear">
                <label><?= $lang['delete'];?>:</label>
                <label class="form-filed">
                	<input type="checkbox"  class="inputstyle w180" name="delete_experiance[]" value="<?=$row_worl_ex['work_ex_id']?>">
                </label>
                </div>
            </div>
            <?php endwhile;?>
            <div id="work_experiance_new_rows"></div>
    		<div class="resume-add-item" id="cv_work_experience"><a style="cursor:pointer;" id="cv_work_experience_add">+ <?= $lang['Add']; ?></a></div>
    	</div>           
    </div>
    <!-- end column cv -->
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Additional_Skills']; ?></h3>
        <div class="resume-edit-content">
            <div id="cv_personal_div" class="resume-edit-content clear">
                <div class="form-item-other">
                    <textarea name="skill" id="skill" maxlength="2000" style="width: 95%;height: 100px;" required="required"><?= $row_cv_info['additional_skills']; ?></textarea>
                </div>
            </div> 
        </div>
    </div>
    <!-- end column cv -->
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Training']; ?></h3>
        
        <div id="cv_training_div" class="resume-edit-content clear"> 
            <div class="form-item-other">
                <textarea name="train" id="train" style="width: 95%;height: 100px;"><?= $row_cv_info['training']; ?></textarea>
            </div>
      	</div>
    </div>
    <!-- end column cv -->
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Hobby']; ?></h3>
        <div id="cv_hobby_div" class="resume-edit-content clear"> 
            <div class="form-item-other">
                <textarea name="hobby" id="hobby" style="width: 95%;height: 100px;"><?= $row_cv_info['hobby']; ?></textarea>
            </div>
        </div>
    </div>
    <!-- end column cv -->
    
    <!-- column cv -->
    <div class="resume-edit-list">
        <h3><?= $lang['Reference']; ?></h3>
        <div class="resume-edit-content">
            <div id="cv_personal_div" class="resume-edit-content clear">
                <div class="form-item-other">
                    <textarea name="reference" id="reference" style="width: 95%;height: 100px;" required="required"><?= $row_cv_info['reference']; ?></textarea>
                </div>
            </div> 
        </div>
    </div>
    <!-- end column cv -->
    
    <div id="editlast" class="form-item mtop">
    	<div class="paddingleft">
        	<input type="submit" class="btn1 btn-orange p12" id="save_cv" name="save_cv" value="<?= $lang['Save']; ?>">
        </div>
    </div>
</form>