// JavaScript Document

var rowNum = 0;
// add education
$("#cv_education_add").click(function(){
	rowNum ++;
	
	var row = '';
	row +='<div id="cv_education_'+rowNum+'" class="form-content" style="border-top:1px solid #e1e1e1;">';
                row +='<span class="form-item-close" onclick="remove_eduction('+rowNum+');"></span>';
                
                row +='<div class="user-form-item clear">';
                    row +='<label>Qualification:</label>';
                    row +='<label class="form-filed">';
                        row +='<select id="qualification"  name="qualification[]" class="inputstyle w250" required >'; 
                            row +='<option value="" >--Please Select--</option>';
                            row +='<option value="1">High School</option>';  
							row +='<option value="2">Associate Degree</option>';  
                            row +='<option value="3">Bachelor Degree</option>';
                            row +='<option value="4">Master Degree</option>'; 
                            row +='<option value="5">Doctor Degree</option>';
                            row +='<option value="6">Others</option>';
                        row +='</select>';
                    row +='</label>';
                row +='</div>';
                
                row +='<div class="user-form-item clear">';
                    row +='<label>From:</label>';
                    row +='<label class="form-filed">';
                        row +='<input require="true" type="text" id="cv-form-edu-time-from-10150935" name="edu_time_from[]" data-beatpicker="true"​ placeholder="yyyy-mm-dd" class="Wdate" onfocus="" style="width:108px;"/>';
                    row +='</label>';
                    row +='<label class="form-filed">To:</label>';
                    row +='<label class="form-filed">';
                        row +='<input type="text" id="cv-form-edu-time-to-10150935" name="edu_time_to[]" data-beatpicker="true"​ placeholder="yyyy-mm-dd" class="Wdate" onfocus="" style="width:108px;"/>';
                    row +='</label>';
                row +='</div>';
                row +='<div class="user-form-item clear">';
                    row +='<label>Institute/University :</label>';
                    row +='<label class="form-filed"><input id="edu_school" type="text" name="school[]" class="inputstyle w250" value="" maxlength="250" require="true"/></label>';
    
                row +='</div>';
                row +='<div class="user-form-item clear">';
                    row +='<label>Field of Study:</label>';
                    row +='<label class="form-filed"><input id="jymajor" type="text" name="major[]" class="inputstyle w250 ui-autocomplete-input" value="" maxlength="120" require="true"/></label>';
                row +='</div>';
				
                row +='<div class="user-form-item clear">';
                    row +='<label>Description:</label>';
                    row +='<label class="form-filed">';
                        row +='<textarea name="edu_des[]" maxlength="2000" class="w450h100" style="width:255px; height:100px;"></textarea>';
                    row +='</label>';
                row +='</div>';
            row +='</div>';
	
	jQuery('#edu_new_rows').append(row);
});
function remove_eduction(rnum) {
	jQuery('#cv_education_'+rnum).remove();
}


// add language
$("#cv_language_add").click(function(){
	rowNum ++;
	
	var row = '';
	row +='<div id="cv_language_'+rowNum+'" class="form-content" style="border-top:1px solid #e1e1e1;">';
                row +='<input type="hidden" id="lang_id_10186003" name="lang_id" value="10186003"/>';
                row +='<span class="form-item-close" onclick="remove_language('+rowNum+');"></span>';
                row +='<div class="user-form-item clear">';
                    row +='<label>Language:</label>';
                    row +='<label class="form-filed">';
                   		row +='<input type="text" class="inputstyle w200" id="languages" name="languages[]" value="" required="required"/>';
                    row +='</label>';
                row +='</div>';
                
				row +='<div class="user-form-item clear">';
                    row +='<label>Level:</label>';
                    row +='<label class="form-filed">';
                        row +='<select id="level"  name="level[]" class="inputstyle w180" required="true">';  
                            row +='<option value="">--Please Select--</option>';  
                            row +='<option value="1">Poor</option>';  
                            row +='<option value="2">Fair</option>';  
                            row +='<option value="3">Good</option>';  
                            row +='<option value="4">Excellent</option>';
                        row +='</select>';
                    row +='</label>';
                row +='</div>';
            row +='</div>';
			
jQuery('#language_new_rows').append(row);
});

function remove_language(rnum) {
	jQuery('#cv_language_'+rnum).remove();
}

// add experiance
$("#cv_work_experience_add").click(function(){
	rowNum ++;
	
	var row = '';
	row +='<div id="cv_workexperience_'+rowNum+'" class="form-content" style="border-top:1px solid #e1e1e1;">';
                row+='<span class="form-item-close" onclick="remove_experiance('+rowNum+');"></span>';
                row+='<div class="user-form-item clear">';
                    row+='<label>Company:</label>';
                    row+='<label class="form-filed"><input id="work_company" type="text" name="work_company[]" class="inputstyle w250" value="" maxlength="200" require="true"/></label>';
                row+='</div>';
                
                row+='<div class="user-form-item clear">';
                    row+='<label>From:</label>';
                    row+='<label class="form-filed">';
                    	row+='<input type="text" id="cv-form-work-time-from" name="work_time_from[]" data-beatpicker="true"​ placeholder="yyyy-mm-dd" style="width:108px;"/>';
                    row+='</label>';
                    row+='<label class="form-filed">To:</label>';
                    row+='<label class="form-filed">';
                    	row+='<input type="text" id="cv-form-work-time-to" name="work_time_to[]" data-beatpicker="true"​ placeholder="yyyy-mm-dd" style="width:108px;"/>';
                    row+='</label>';
                row+='</div>';
                
                row+='<div class="user-form-item clear">';
                    row+='<label>Position:</label>';
                    row+='<label class="form-filed"><input id="work-position" type="text" name="work_position[]" class="inputstyle w250" value="" maxlength="100"/></label>';
                row+='</div>';
                
                row+='<div class="user-form-item clear">';
                    row+='<label>Description:</label>';
                    row+='<label class="form-filed">';
                        row+='<textarea name="work_description[]" maxlength="1000" class="w450h100" style="width:255px; height:100px;"></textarea>';
                    row+='</label>';
                row+='</div>';
            row+='</div>';
			
jQuery('#work_experiance_new_rows').append(row);
});

function remove_experiance(rnum) {
	jQuery('#cv_workexperience_'+rnum).remove();
}
