// JavaScript Document
$(document).ready(function(){
		$(document).on('click', 'a.close1, #add_err', function(){ 
		  $('#add_err').fadeOut(300 , function() {
			$('#add_err').css({display:'none'});  
		}); 
		return false;
		});
		
			$("#close_form_login").click(function(){
			$('#mask , .login-popup').fadeOut(300 , function() {
				$('#mask').remove();			
					});
				return false;
			});
	
	
	$("#edit_profiles").click(function(){
		profile_id=$("#profile_id").val();
		firstname=$("#firstname").val();
		lastname=$("#lastname").val();
		sex=$("#sex").val();
		bd=$("#bd").val();
		marital=$("#marital").val();
		phone=$("#phone").val();
		address=$("#address").val();
		province=$("#province").val();
		
		$.ajax({
			type: "POST",
			url: "ajax/edit_profile.php",
			data:  $("form").serialize(),
			success: function(html){
			  if(html=='true'){
				 $("#err_success").css({display:'block'});
				 $("#err_success").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Well done!   </strong>Update Successful!");
				 setTimeout(function(){$('#err_success').css({display:'none'});}, 5000);
				 window.location.href="/users/cv.php";
			  }else{
			   	$("#add_err").css({display:'block'});
			   	$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Unsuccessful");
			   	setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
			  }
			}
		 });
		 return false;
	});
});