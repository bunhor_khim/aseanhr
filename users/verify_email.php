
<?php 
session_start();
require_once("../_aseanhr_admin/_config/connect.php");
include_once 'langauge/common.php';

if(isset($_SESSION['login_job_seeker']) && $_SESSION['login_job_seeker']){?>
<script type="text/javascript">
	window.location.href="/users/";
</script>
<?php
}else{

if(isset($_GET['resetkey'])){
	
	$resetkey=$_GET['resetkey'];
	$select_resetkey=mysqli_query($conn,"select reset_key from tbl_jobseeker where reset_key='$resetkey'");
	$count_resetkey=mysqli_num_rows($select_resetkey);
	if($count_resetkey>0){
		$update_status=mysqli_query($conn,"update tbl_jobseeker set status=1,reset_key='' where reset_key='$resetkey'");
		if($update_status){
			?>
            <script>
				window.location.href="/users/success.php";
			</script>
            <?php
		}else{
			?>
            <script>
				window.location.href="/users/unsuccess.php";
			</script>
            <?php
		}
	}else{
		?>
            <script>
				window.location.href="/users/email_already.php";
			</script>
            <?php
	}

}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../images/icon.png" />
<title>Jok Seeker Register</title>
<link href="../css/style1.css" rel="stylesheet" type="text/css" />
<link href="../css/color.css" rel="stylesheet" type="text/css"  />
<!-- jquery tab -->
<script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="../js/engine1/style.css" />
<!-- End WOWSlider.com HEAD section -->
</head>

<body  id="home">
<!-- alert message -->
<div class="err warning" id="add_err">
</div>
<div class="err success" id="err_success">
</div>
<!-- end alert message -->
<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('../includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('../includes/menu.php');?>
        </div>
        
        <div class="container">
        <!-- main -->
            <div class="main">
            	<?php include('includes/success.php');?>
            </div>
        <!-- end main -->
            <div class="side">
            	<?php include('../includes/contact_side.php'); ?>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("../includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('../includes/copy-right.php');?>
</div>
<script type="text/javascript" src="../js/engine1/wowslider.js"></script>
<script type="text/javascript" src="../js/engine1/script.js"></script>
</body>
</html>
<?php
    }
?>