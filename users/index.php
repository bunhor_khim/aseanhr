<?php session_start();
include 'langauge/common.php';
require_once("../_aseanhr_admin/_config/connect.php");

if($page=="logout"){
  session_destroy();
  ?>
  <script type="text/javascript">
        window.location.href="/";
  </script>
<?php   
}
if(isset($_SESSION['login_job_seeker']) && $_SESSION['login_job_seeker']){
	
	$job_seeker_log=$_SESSION["login_job_seeker"];
	$job_seeker_name=$_SESSION['job_seeker_name'];
	$job_seeker_id = $_SESSION['job_seeker_id'];
	
	if(isset($_GET['delete_id']))
	{
		$tbl_cv="DELETE FROM tbl_cv WHERE cv_id=".$_GET['delete_id'];
		mysqli_query($conn,$tbl_cv);
		
		$tbl_language="DELETE FROM tbl_language WHERE land_cv_id=".$_GET['delete_id'];
		mysqli_query($conn,$tbl_language);
		
		$tbl_education="DELETE FROM tbl_education WHERE edu_cv_id=".$_GET['delete_id'];
		mysqli_query($conn,$tbl_education);
		
		$tbl_work_experience="DELETE FROM tbl_work_experience WHERE job_seeker_id=".$_GET['delete_id'];
		mysqli_query($conn,$tbl_work_experience);
		
		$tbl_education="DELETE FROM tbl_education WHERE ex_cv_id=".$_GET['delete_id'];
		mysqli_query($conn,$tbl_education);
		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../images/icon.png" />
<title>ASEANHR - User Profiles</title>
<link href="../css/style1.css" rel="stylesheet" type="text/css" />
<link href="../css/color.css" rel="stylesheet" type="text/css"  />

<!-- jquery tab -->
<!--<script src="http://code.jquery.com/jquery-1.6.3.min.js" type="text/javascript"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="../js/engine1/style.css" />
<!-- End WOWSlider.com HEAD section -->
<!-- date picker -->
<link rel="stylesheet" href="css/BeatPicker.min.css"/>
<script src="js/BeatPicker.min.js"></script>
<!-- end date picker -->
<script type="text/javascript">
function delete_id(id)
{
	if(confirm('Sure To Remove This Job ?'))
	{
		window.location.href='index.php?delete_id='+id;
	}
}
</script>
</head>

<body id="home">

<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('../includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('../includes/menu.php');?>
        </div>
       
        <div class="container">
        <!-- main -->
            <div class="user_main">
            	<div class="main_user">
            	<?php include("includes/cv.php");?>
                </div>
            </div>
        <!-- end main -->
            <div class="user_side">
            	<div class="side_user">
            	<?php include("../includes/user_side.php");?>
                </div>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("../includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('../includes/copy-right.php');?>
</div>
<script type="text/javascript" src="../js/engine1/wowslider.js"></script>
<script type="text/javascript" src="../js/engine1/script.js"></script>
<script src="js/add_form.js" type="text/javascript"></script>
</body>
</html>
	<?php
    }else{
        ?>
        <script type="text/javascript">
        window.location.href="/";
    </script>
        <?php
    }

?>