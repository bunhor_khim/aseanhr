<?php 
session_start();
require_once("../_aseanhr_admin/_config/connect.php");
	

if(isset($_GET['cv_id']) && $_GET['cv_id']){
$cv_id=$_GET['cv_id'];

$select_cv_info = mysqli_query($conn,"select * from tbl_cv where cv_id=$cv_id"); 
$count_cv=mysqli_num_rows($select_cv_info);
if($count_cv>0){
	
if(isset($_SESSION['login_job_seeker']) && $_SESSION['login_job_seeker']){
	
	$job_seeker_id = $_SESSION['job_seeker_id'];
	
	$select_user_profile=mysqli_query($conn ,"SELECT tbl_jobseeker . * ,tbl_cv.*
											FROM tbl_jobseeker
												INNER JOIN tbl_cv ON tbl_jobseeker.job_seeker_id = tbl_cv.job_seeker_id
											WHERE tbl_jobseeker.job_seeker_id ='$job_seeker_id'");
	$row_user_profile=mysqli_fetch_array($select_user_profile);
	$row_cv=mysqli_fetch_array($select_cv_info);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
		 
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="shortcut icon" href="../images/icon.png" />
   	<link href="css/preview.css" rel="stylesheet" type="text/css" /> 
   
	<script src="js/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/base64.js"></script>
	<script type="text/javascript" src="js/sprintf.js"></script>
    <script src="js/jspdf.js" type="text/javascript"></script>
	<title>Resume preview</title> 
	<meta name="description" content="" />
	<meta name="keywords" content="" />
    <script type="text/javascript">
	jQuery(document).ready(function(){
		var doc = new jsPDF();
		var specialElementHandlers = {
			'#resume-preview-content-text': function (element, renderer) {
				return true;
			}
		};
		$('#donwload_pdf').click(function () {
			doc.fromHTML($('#resumepreview').html(), 15, 15, {
				'width': 170,
					'elementHandlers': specialElementHandlers
			});
			doc.save('sample-file.pdf');
		});
	});
    </script>
</head>   

<body>
	
	<div id="preview-wrapper">
		<div class="resume-preview-top"> 
			<div class="print-download">
				<a id="resumeprint" href="#">Print</a>
			</div>
		</div>
         
        <div class="resume-preview-content-top"></div>
		<div id="resumepreview" class="resume-preview-content">
			<!-- preview content start -->
			<div class="personal-info">
				<h2><?=$row_user_profile['first_name']?>&nbsp;<?=$row_user_profile['last_name']?></h2>
				<div class="personal-photo">
				<?php
				if($row_user_profile['photo']==""){
				?>
                	<div id='preview'>
                		<img width="140" src="images/no_photo.png" style="border:1px solid #eee;"/>
                    </div>
                <?php
				}else{
				?> 	
					<img src="images/<?=$row_user_profile['photo']?>" style="max-width:100px;"/>
				<?php 
				}
				?>	
				</div>
			</div>
			
			<table border="0" class="resume-preview-tab">
                <tr>
                    <th>Applying for position:</th>
                    <td><?=$row_user_profile['position']?></td>
                </tr>
                <tr>
                    <th>Location:</th>
                    <?php
					$cv_prov=mysqli_query($conn,"select * from tbl_cv_local where cv_id=$cv_id");
					?>
                    <td><?php 
					while($row_cv_prov=mysqli_fetch_array($cv_prov)){
						$province_id= $row_cv_prov['province_id'];
						$prov_name=mysqli_query($conn,"select * from tbl_province where prov_id=$province_id");
						$row_prov_name=mysqli_fetch_array($prov_name);
						echo $row_prov_name['prov_name_en'].",&nbsp;";
					}
						?>
                   </td>
                </tr>
                <tr>
                    <th>Salary:</th>
                    <td>$ <?=$row_user_profile['salary']?></td>
                </tr>
			</table>
            
          <h3>Personal Information</h3>
            	<ul class="work-experience bline">
                	<?php
					$arr_sex=array('','Male','Female'); 
					?>
					<li class="clear">
						<strong>Sex:</strong>
						<p><?=$arr_sex[$row_user_profile['sex']];?></p>
				  </li>
                    <li class="clear">
						<strong>Marital:</strong>
						<p><?=$row_user_profile['marital'];?></p>
					</li>
                    <li class="clear">
						<strong>Date of Birth:</strong>
						<p><?=$row_user_profile['date_of_birth']?></p>
					</li>
                    <?php
                    $prov_id=$row_user_profile['location_of_residence'];
					$select_prov=mysqli_query($conn,"select * from tbl_province where prov_id=$prov_id");
					$row_pro_name=mysqli_fetch_array($select_prov);
					?>
                    <li class="clear">
						<strong>Location of Residence:</strong>
						<p><?=$row_pro_name['prov_name_en']?></p>
					</li>
                    <li class="clear">
						<strong>Mobile:</strong>
						<p><?=$row_user_profile['phone']?></p>
					</li>
                    <li class="clear">
						<strong>Email:</strong>
						<p><?=$row_user_profile['email']?></p>
					</li>
                    <li class="clear">
						<strong>Address:</strong>
						<p><?=$row_user_profile['address']?></p>
					</li>
       	  </ul>
    <h3>Education</h3>
				<?php
			$select_education=mysqli_query($conn,"select * from tbl_education where edu_cv_id='$cv_id'");
			while($row_edu=mysqli_fetch_array($select_education)):
			?>
				<ul class="work-experience bline">
					<li class="clear">
						<strong>Qualification:</strong>
						<p>
                        <?php
						if($row_edu['qualification']="1"){echo "High School";}
						elseif($row_edu['qualification']="2"){echo "Associate's Degree";}
						elseif($row_edu['qualification']="3"){echo "Bachelor Degree";}
						elseif($row_edu['qualification']="4"){echo "Master Degree";}
						elseif($row_edu['qualification']="5"){echo "Doctor Degree";}
						elseif($row_edu['qualification']="6"){echo "Others";}
						?>
                      </p>
					</li>
					<li class="clear">
						<strong>From: </strong>
						<p><?=$row_edu['from_date'];?> - <?=$row_edu['to_date'];?></p>
					</li>
					<li class="clear">
						<strong>Institute/University :</strong><p><?=$row_edu['university'];?></p>
					</li>
					<li class="clear">
						<strong>Field of Study:</strong><p><?=$row_edu['f_study'];?></p>
					</li>
					<li class="clear">
						<strong>Description:</strong>
						<p><?=$row_edu['des'];?></p>
					</li>
				</ul>
            <?php
			endwhile;
			?>
            
	        <h3 style="background:#eee;">Language</h3>
	        
				<table border="0" class="resume-preview-tab">
                <?php
				$select_language=mysqli_query($conn,"select * from tbl_language where land_cv_id='$cv_id'");
				while($row_language=mysqli_fetch_array($select_language)):
				?>
	  				<tr>
	                    <th><?=$row_language['language'];?></th>
                      	<?php $arr_lang_level=array('','Poor','Fair','Good','Excellent'); ?>
	                    <td><?=$arr_lang_level[$row_language['level']];?></td>
	  				</tr>
               	<?php
				endwhile;
				?>
				</table>
	        
	        	        
	        <h3 style="background:#eee;">Work Experience</h3>
			<?php
				$select_company=mysqli_query($conn,"select * from tbl_work_experience where ex_cv_id='$cv_id'");
				while($row_company=mysqli_fetch_array($select_company)):
				?>
				<ul class="work-experience bline">
                	<li class="clear">
						<strong>Company: </strong><p><?=$row_company['company'];?></p>
					</li>
					<li class="clear">
						<strong>Time: </strong>
						<p>From <?=$row_company['start_date'];?> To <?=$row_company['to_date'];?></p>
					</li>
					<li class="clear">
						<strong>Position: </strong><p><?=$row_company['position'];?></p>
					</li>
					<li class="clear">
						<strong>Description: <?=$row_company['des'];?></strong>
						<p></p>
					</li>
				</ul>
	        <?php
			endwhile;
			?>
            <?php
			
			$training = preg_replace("/\r\n|\r/", " <br/> ", $row_cv["training"]);
  			$training = trim($training);
			
			
			$hobby = preg_replace("/\r\n|\r/", " <br/> ", $row_cv["hobby"]);
  			$hobby = trim($hobby);

			$reference = preg_replace("/\r\n|\r/", " <br/> ", $row_cv["reference"]);
  			$reference = trim($reference);
            
			?>
			
            <h3 style="background:#eee;">Training</h3>
			<p style="margin-top:12px;line-height:25px" class="resume-preview-content-text"><?=$training;?></p>
            
			<h3 style="background:#eee;">Hobby</h3>
			<p style="margin-top:12px;line-height:25px" class="resume-preview-content-text"><?=$hobby;?></p>
			
			<h3 style="background:#eee;">Reference</h3>
			<p style="margin-top:12px;line-height:25px" class="resume-preview-content-text"><?=$reference;?></p>	         
			<!-- preview content end -->
            <div class="copy-right">
             <p style="font-size:16px; font-weight:bold; color:#06F;">COPY RIGHT ASEANHR</p>
                <p>Email : info@aseanhr.net</p>
                <p>Phone : 097 980 9697 / 086 639 267</p>
                <p>Address : #.., st.., Sangkat.., Khan.., Phnom Penh Cambodia</p>
            </div>
        </div>
        <div class="resume-preview-content-bottom"></div>
		<div class="resume-preview-close"><a href="javascript:window.close();"><button>Close</button></a></div>
    </div>
    <script type="text/javascript" src="js/jquery.printArea.js"></script> 
    <script type="text/javascript">
    	$(document).ready(function(){ 
        	$("#resumeprint").click(function(){ 
        		$("#resumepreview").printArea(); 
        	});
		});
    </script>
	
</body>
</html>

<?php
		}else{
		?>
		<script type="text/javascript">
			window.location.href="/";
		</script>
			<?php
		}
	}else{
	?>
	<script type="text/javascript">
		window.location.href="/";
	</script>
		<?php
	}

}else{
	?>
	<script type="text/javascript">
	window.location.href="/";
</script>
	<?php
}

?>
