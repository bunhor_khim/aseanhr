<?php
/* 
-----------------
Language: German
-----------------
*/

$lang = array();
// Menu

$lang['welcome']='សូមស្វាគមន៏';

$lang['MENU_HOME'] = 'ទំព័រដើម';
$lang['MENU_ABOUT_US'] = 'អំពីយើងខ្ញុំ';
$lang['Our_Services']= "សេវាកម្មយើង";
$lang['MENU_CONTACT'] = 'ទំនាក់ទំនងយើងខ្ញុំ';
$lang['Job_seeker']='អ្នកសែ្វងរកការងារ';
$lang['Job_learning']='ការរៀនការងារ';

// search
$lang['Find_Job_Now']='ស្វែងរកការងារ';
$lang['keyword']='ពាក្យគន្លឹះ';
$lang['category']='ប្រភេទ';
$lang['function']='មុខងារ';
$lang['Local']='ទីតាំងការងារ';
$lang['Search']='ស្វែងរក';

$lang['Browse_Jobs']="ពិនិត្យមើលការងារ";
$lang['Salary']="បា្រក់ខែ";
$lang['Location']="ទីតាំង";
$lang['Section']="ផ្នែក";

$lang['Recent_Jobs']="ការងារចុងក្រោយ";
$lang['Position']="មុខដំណែង";
$lang['Date']='ថ្ងៃផុតកំណត់';
$lang['apply']="ដាក់ពាក្យ";
$lang['apply_now']="ដាក់ពាក្យឥលូវ";
$lang['jobseeker']="អ្នកសែ្វងរកការងារ";
$lang['employer']="និយោជក";
$lang['username']="ឈ្មោះអ្នកប្រើប្រាស់ / អ៊ីមែល";
$lang['password']="លេខសំងាត់";
$lang['login']="ចូលគណនី";
$lang['register']="ចុះឈ្មោះ";
$lang['forget']="ភ្លេចលេខសំងាត់";

$lang['Company']='ក្រុមហ៊ុន';
$lang['Profile']="ព័ត៌មានផ្ទាល់ខ្លួន";
$lang['Create_CV']="បង្កើតជីវប្រវត្តិ";
$lang['Company_profile']='ប្រវត្តិក្រុមហ៊ុន';
$lang['new_Jobs']='ផ្សព្វផ្សាយការងារថ្មី';


$lang['male']="ប្រុស";
$lang['female']="ស្រី";
$lang['Both']="ទាំងពីរភេទ";

$lang['Single']="នៅលីវ";
$lang['Married']="រៀបការ";

//Qualification
$lang['High_School']="វិទ្យាល័យ";
$lang['Associate']="ថ្នាក់បរិញ្ញាបត្ររង";
$lang['Bachelor']="ថ្នាក់បរិញ្ញាបត្រ";
$lang['Master']="ថ្នាក់អនុបណ្ទិត";
$lang['Doctor']="ថ្នាក់បណ្ឌិត";
$lang['Others']="ថ្នាក់ផ្សេងៗ";

//Level
$lang['Entry_Level']="កម្រិតទាប";
$lang['Middle']="កម្រិតមធ្យម";
$lang['Senior']="កម្រិតខ្ពស់";
$lang['Top_Level']="កម្រិតខ្ពស់បំផុត";
$lang['Fresh_Graduate']="ទើបបញ្ចប់ការសិក្សា";

$lang['hotline']="លេខទូរស័ព្ទទំនាក់ទំនងបន្ទាន់";

$lang['hotline']="លេខទូរស័ព្ទទំនាក់ទំនងបន្ទាន់";
$lang['em']="អ៊ីមែល";
$lang['tels']="ទូរស័ព្ទ";
$lang['Hours']="ម៉ោងធើ្វការ";
$lang['addr']="អាសយដ្ឋាន";

// job term
$lang['full_time']="ពេញម៉ោង";
$lang['Part_time']="ក្រៅម៉ោង";
$lang['Internship']="កម្មសិក្សា";
$lang['Volunteer']="ស្មគ្រ័ចិត្ត";
$lang['Urgent']="បន្ទាន់";
$lang['job_Urgent']="ការងារបន្ទាន់";
$lang['popular_search']="ស្វែងរកការងារពេញនិយម";

//post jobs
$lang['post_jobs']='ផ្សព្វផ្សាយការងារ ';
$lang['posts_jobs']='ការងារ';
$lang['All_Jobs']='ការងារទាំងអស់';
$lang['new_Jobs']='ផ្សព្វផ្សាយការងារថ្មី';

$lang['account']="អំពីគណនី";
$lang['change_pass']="ប្តូរលេខសំងាត់";
$lang['Logout']="ចាកចេញ";

$lang['Company_profile']='ប្រវត្តិក្រុមហ៊ុន';
$lang['Job_Description']="ពណ៌នាការងារ";
$lang['Company']='ក្រុមហ៊ុន';
$lang['Industry']='វិស័យ';
$lang['Type']='ប្រភេទក្រុមហ៊ុន';
$lang['Employees']='ចំនួននិយោជិក';
$lang['Location']='ទីតាំង';
$lang['Website']='គេហទំព័រ';
$lang['Address']='អាសយដា្ឋន';
$lang['Company_Description']='ពិព័ណនាពីក្រុមហ៊ុន';
$lang['Job_Title']='មុខតំណែង';
$lang['Publish_Date']='កាលបរិច្ឆទចុះផ្សាយ';
$lang['Closing_Date']='ថ្ងៃផុតកំណត់';
$lang['Hiring']='ចំនួនជ្រើសរើស';
$lang['Function']='មុខងារ';
$lang['Salary']='បា្រក់ខែ';
$lang['Description']='ការពិពណ័នា';
$lang['Job_Level']='កម្រិតការងារ';
$lang['Qualification']='កម្រិតវប្បធម៌';
$lang['Job_Term']='ប្រភេទការងារ';
$lang['Year_of_Exp']='បទពិសោធន៍​ (ឆ្នំា)';
$lang['Age']='អាយុ';
$lang['Sex']='ភេទ';
$lang['Job_Requirements']='តម្រូវការការងារ';
$lang['How_to_Apply']='របៀបក្នុងការដាក់ពាក្យ';
$lang['Save']='រក្សាទុក';

$lang['Level']="កំរិត";
$lang['Term']='ប្រភេទការងារ';
$lang['Contact_Infomation']="ពត៏មានទំនាក់ទំនង";
$lang['Contact_Name']="ឈ្មោះអ្នកទំនាក់ទំនង";
$lang['Phone']="ទូរស័ព្ទ";
$lang['Email']="អ៊ីម៉ែល";
$lang['apply']="ដាក់ពាក្យឥលូវនេះ";

$lang['Please_Select']="សូមជ្រើសរើស";
$lang['Negotiable']="តម្លៃចចារបាន";
$lang['Fax']="ទូរសារ";
$lang['Please_Select']="សូមជ្រើសរើស";

$lang['job_alert']="ជូនដំណឹងការងារ";
$lang['visitor_count']="ចំនួនអ្នកចូលទស្សនា";
$lang['info']="ដើម្បីបង្កើតទម្រង់ស្តង់ដា";
$lang['c_cv']="ចុចទីនេះដើម្បីបង្កើត​ CV";
$lang['sign_out']="ដើម្បីចាកចេញពីគណនីចុចទីនេះ";
$lang['postj']="ចុចទីនេះដើម្បី ជ្រើសរើសបុគ្គលិក";

//visitor counter
$lang['this_week']="សប្តាហ៍នេះ";
$lang['this_month']="ខែ​នេះ";
$lang['this_year']="ឆ្នាំនេះ";
$lang['Total']="សរុប";

$lang['language']="ភាសា";
?>