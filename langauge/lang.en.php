<?php
/* 
-----------------
Language: German
-----------------
*/

$lang = array();
// Menu
$lang['welcome']='welcome to';
$lang['MENU_HOME'] = 'Home';
$lang['MENU_ABOUT_US'] = 'About us';
$lang['Our_Services']= "Our Services";
$lang['MENU_CONTACT'] = 'Contact us';
$lang['Job_seeker']='Job Seekers';
$lang['Job_learning']='Job Learning';

// search
$lang['Find_Job_Now']='Find Job Now';
$lang['keyword']='Keyword';
$lang['category']='Category';
$lang['function']='Function';
$lang['Local']='Location';
$lang['Search']='Search';

$lang['Browse_Jobs']="Browse Jobs";
$lang['Salary']="Salary";
$lang['Location']="Location";
$lang['Section']="Section";

$lang['Recent_Jobs']="Recent Jobs";
$lang['Position']="Position";
$lang['Date']="Closing Date";
$lang['apply']="Apply";
$lang['apply_now']="Apply Now";
$lang['jobseeker']="JOB SEEKERS";
$lang['employer']="EMPLOYER";
$lang['username']="Username / Email";
$lang['password']="Password";
$lang['login']="Login";
$lang['register']="Register";
$lang['forget']="Forgot Password";

$lang['Company']='Company';
$lang['Create_CV']="Create CV";
$lang['Profile']="Profile";
$lang['Company_profile']='Company Profile';
$lang['new_Jobs']='Post a new Jobs';


$lang['male']="Male";
$lang['female']="Female";
$lang['Both']="Both";

$lang['Single']="Single";
$lang['Married']="Married";

//Qualification
$lang['High_School']="High School";
$lang['Associate']="Associate's Degree";
$lang['Bachelor']="Bachelor Degree";
$lang['Master']="Master Degree";
$lang['Doctor']="Doctor Degree";
$lang['Others']="Others";

//Level
$lang['Entry_Level']="Entry";
$lang['Middle']="Middle";
$lang['Senior']="Senior";
$lang['Top_Level']="Top";
$lang['Fresh_Graduate']="Fresh Graduate";

$lang['hotline']="Service hot line";
$lang['em']="Email";
$lang['tels']="Tel";
$lang['Hours']="Operating Hours";
$lang['addr']="Address";

// job term
$lang['full_time']="Full Time";
$lang['Part_time']="Part Time";
$lang['Internship']="Internship";
$lang['Volunteer']="Volunteer";
$lang['Urgent']="Urgent";
$lang['job_Urgent']="Job Urgent";
$lang['popular_search']="Popular Search";

//post jobs
$lang['post_jobs']='Post Job';
$lang['posts_jobs']='Jobs';
$lang['All_Jobs']='All Jobs';
$lang['new_Jobs']='Post a new Jobs';

$lang['account']="Account Setting";
$lang['change_pass']="Change Password";
$lang['Logout']="Logout";

$lang['Company_profile']='Company Profile';
$lang['Job_Description']="Job Description"; 
$lang['Company']='Company';
$lang['Industry']='Industry';
$lang['Type']='Type';
$lang['Employees']='Employees';
$lang['Location']='Location';
$lang['Website']='Website';
$lang['Address']='Address';
$lang['Company_Description']='Company Description';
$lang['Job_Title']='Job Title';
$lang['Publish_Date']='Publish Date';
$lang['Closing_Date']='Closing Date';
$lang['Hiring']='Hiring';
$lang['Function']='Function';
$lang['Salary']='Salary';
$lang['Description']='Description';
$lang['Job_Level']='Job Level';
$lang['Qualification']='Qualification';
$lang['Job_Term']='Job Term';
$lang['Year_of_Exp']='Year of Exp';
$lang['Age']='Age';
$lang['Sex']='Sex';
$lang['Job_Requirements']='Job Requirements';
$lang['How_to_Apply']='How to Apply';
$lang['Save']='Save';

$lang['Level']="Level";
$lang['Term']='Term';
$lang['Contact_Infomation']="Contact Infomation";
$lang['Contact_Name']="Contact Persion";
$lang['Phone']="Phone";
$lang['Email']="Email";
$lang['apply']="Apply Now";

$lang['Please_Select']="Please Select";
$lang['Negotiable']="Negotiable";
$lang['Fax']="Fax";
$lang['Please_Select']="Please Select";

$lang['job_alert']="Job Alert";
$lang['visitor_count']="Visitor Counter";
$lang['info']="To make a standard profile";
$lang['c_cv']="Click Here to create your CV";
$lang['sign_out']="Sign out your account click here";
$lang['postj']="Click to post new job";

//visitor counter
$lang['this_week']="This Week";
$lang['this_month']="This Month";
$lang['this_year']="This Year";
$lang['Total']="Total";
$lang['language']="Language";


?>