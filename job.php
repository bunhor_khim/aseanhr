<?php
if(isset($_GET['jobId']) && ($_GET['jobId'])){
	
session_start();
include("_aseanhr_admin/_config/connect.php");
include_once 'langauge/common.php';
$job_id=$_GET['jobId'];
$select_count_job=mysqli_query($conn,"select job_id from tbl_jobs where job_id=$job_id");
$count_job=mysqli_num_rows($select_count_job);

	if($count_job>0){

mysqli_query($conn,'SET NAMES utf8');
$select_job_id=mysqli_query($conn,"SELECT tbl_jobs . * , tbl_province . * , tbl_function . * , tbl_company . *,tbl_company.prov_id as com_prov_id,tbl_industry. *
									FROM tbl_jobs
										INNER JOIN tbl_province ON tbl_jobs.prov_id = tbl_province.prov_id
										INNER JOIN tbl_function ON tbl_jobs.func_id = tbl_function.func_id
										INNER JOIN tbl_company ON tbl_jobs.com_id = tbl_company.com_id
										INNER JOIN tbl_industry ON tbl_jobs.ind_id = tbl_industry.ind_id
									where job_id=$job_id
							");

$row_job=mysqli_fetch_array($select_job_id);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/icon.png" />
<title>ASEANHR - <?=$row_job['job_title']?></title>
<link href="css/style1.css" rel="stylesheet" type="text/css" />
<link href="css/color.css" rel="stylesheet" type="text/css"  />
<!-- Start WOWSlider.com HEAD section -->

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
<!-- wow slide2-->
<link rel="stylesheet" type="text/css" href="js/engine1/style.css" />
<link rel="stylesheet" type="text/css" href="js/engine2/style.css" />
<link rel="stylesheet" type="text/css" href="js/engine4/style.css" />
<!-- wow slide2-->
</head>

<body id="home">

<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
            <?php include('includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('includes/menu.php');?>
        </div>
        
        <div class="container">
        <!-- main -->
            <div class="main">
            	<?php include('includes/job_detail.php');?>
            </div>
        <!-- end main -->
            <div class="side">
            	<?php include('includes/contact_side.php'); ?>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('includes/copy-right.php');?>
</div>
<script type="text/javascript" src="js/engine1/wowslider.js"></script>
<script type="text/javascript" src="js/engine1/script.js"></script>
<script type="text/javascript" src="js/engine2/wowslider.js"></script>
<script type="text/javascript" src="js/engine2/script.js"></script>
<script type="text/javascript" src="js/engine4/wowslider.js"></script>
<script type="text/javascript" src="js/engine4/script.js"></script>
</body>
</html>

<?php
		
	}else{
		?>
		<script type="text/javascript">
            window.location.href="/";
        </script>
        <?php
	}
}else{
	?>
	<script type="text/javascript">
		window.location.href="/";
	</script>
	<?php
}

?>