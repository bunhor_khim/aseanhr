<?php
session_start();
    require_once("_config/connect.php");
if(isset($_POST['btnsubmit'])){

$uname=mysqli_real_escape_string($conn,$_POST['txtuname']);
$pwd=md5($_POST['txtpwd']);
$sele=mysqli_query($conn,"SELECT * FROM tbl_user WHERE `username`='$uname' AND `password`='$pwd'");
    $query=mysqli_fetch_array($sele);
    $count_str=mysqli_num_rows($sele);
    
    if($count_str > 0){ 

        $_SESSION["login"]="OK";
        $_SESSION["name"]=$query['full_name'];
        $_SESSION["user_id"]=$query['user_id'];
		$_SESSION["type"]=$query['user_type'];
        ?>
			<script type="text/javascript">
                window.location.href="_sys_admin";
            </script>
        <?php
    }
    else{
        //header("location:index.php");
        $msg="<font style='font-family:Arial;color:red; font-size:16px;'>Invalid Username and Password</font>";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Asean HR </title>
    <!-- Core CSS - Include with every page -->
    <link href="_sys_admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="_sys_admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="_sys_admin/css/sb-admin.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
        		
            <div class="col-md-4 col-md-offset-4">
            <div class="col-md-12" style="border:0px solid #F00; text-align:center">
            	<img src="../images/asean_logo.png" height="80"/>
            </div>
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ASEAN HR LOGIN</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" enctype="multipart/form-data">
                            <fieldset>	                    
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="txtuname" class="form-control" placeholder="UserName" required="true">
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" name="txtpwd" class="form-control" placeholder="Password" required="true">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" name="btnsubmit" value="Login" class="btn btn-lg btn-success btn-block" style="line-height:15px;"/>
                            </fieldset>
                        </form>
                    </div>
                    <div style="border:0px solid #F00; text-align:center; margin-top:-10px; margin-bottom:10px;">
						<?php
                            if(!empty($msg)){
                            echo $msg;
                            }
                        ?>
                   </div>
                </div>
                <div class="col-md-12" style="border:0px solid #F00; text-align:center;">
            		<h1 style="font-size:14px !important;">Copyright &copy; ASEANHR | Power By <a href="http://khemarahost.com" target="_blank">Khemara Host</a></h1>
            </div>
            </div>
        </div>
    </div>

    <!-- Core Scripts - Include with every page -->
    <script src="_sys_admin/js/jquery-1.10.2.js"></script>
    <script src="_sys_admin/js/bootstrap.min.js"></script>
    <script src="_sys_admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="_sys_admin/js/sb-admin.js"></script>

</body>

</html>
