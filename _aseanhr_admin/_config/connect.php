<?php
	date_default_timezone_set("Asia/Phnom_Penh");
	if(isset($_GET['page'])){
		$page=$_GET['page'];
	}else
		$page="";
		
	/* Database setup information */
    $dbhost = 'localhost';  // Database Host
    $dbuser = 'root';       // Database Username
    $dbpass = '';           // Database Password
    $dbname = 'aseanhr_db';      // Database Name

    /* Connect to the database and select database */
    $conn = mysqli_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
    mysqli_select_db($conn,$dbname);
 
$base_url = "/aseanhr.com";
//http: $_SERVER["HTTP_HOST"];
  
//get full url
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

function getKhmerMonth($num){
        $months = array();
        $months[1] = "មករា" ;
        $months[2] = "កុម្ភៈ";
        $months[3] = "មីនា";
        $months[4] = "មេសា";
        $months[5] = "ឧសភា";
        $months[6] = "មិថុនា";
        $months[7] = "កក្កដា";
        $months[8] = "សីហា";
        $months[9] = "កញ្ញា";
        $months[10] = "តុលា";
        $months[11] = "វិច្ឆិកា";
        $months[12] = "ធ្នូ";

        return $months[$num];
    }
function getEnglishMonth($num){
        $months = array();
        $months[1] = "Jan" ;
        $months[2] = "Feb";
        $months[3] = "Mar";
        $months[4] = "Apr";
        $months[5] = "May";
        $months[6] = "Jun";
        $months[7] = "Jul";
        $months[8] = "Aug";
        $months[9] = "Sep";
        $months[10] = "Oct";
        $months[11] = "Nov";
        $months[12] = "Dec";

        return $months[$num];
    }

 function getDateFormat($timestamp){
		
        $date = new DateTime($timestamp);
        if(isSet($_SESSION['lang']) && $_SESSION['lang'] =='km'){
			$month = $date->format('n');
            $monthStr = getKhmerMonth($month);
            $dat =$date->format('d');
            return $dat." ".$monthStr." ".$date->format('Y');
		} else{
			$month = $date->format('n');
            $monthStr = getEnglishMonth($month);
            $dat =$date->format('d');
            return $dat." ".$monthStr." ".$date->format('Y');
		}
    }

 function check_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
  return $data;
}
?>
