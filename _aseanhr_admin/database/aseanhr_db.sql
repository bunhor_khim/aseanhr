-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2015 at 04:00 AM
-- Server version: 5.0.51b-community-nt-log
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aseanhr_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE IF NOT EXISTS `tbl_company` (
  `com_id` int(10) NOT NULL auto_increment,
  `ind_id` int(10) NOT NULL,
  `type_id` int(10) NOT NULL,
  `prov_id` int(10) NOT NULL,
  `com_emp` varchar(20) NOT NULL,
  `com_name` varchar(200) NOT NULL,
  `com_contact` varchar(100) NOT NULL,
  `com_phone` varchar(100) NOT NULL,
  `com_fax` varchar(100) NOT NULL,
  `com_email` varchar(100) NOT NULL,
  `com_website` varchar(100) NOT NULL,
  `com_addr` text NOT NULL,
  `com_desc` text NOT NULL,
  `com_image` varchar(100) NOT NULL,
  PRIMARY KEY  (`com_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`com_id`, `ind_id`, `type_id`, `prov_id`, `com_emp`, `com_name`, `com_contact`, `com_phone`, `com_fax`, `com_email`, `com_website`, `com_addr`, `com_desc`, `com_image`) VALUES
(1, 1, 1, 5, '11', 'Pathmazing Inc', 'Mr.Vutha', '012334221', '023494553', 'vutha@gmail.com', 'www.google.com', '<p>#33-34,St.114,Monorom,7Makara,Phnom Penh.</p>\r\n', '<p>Pathmazing is a global software solution provider in pursuit of extraordinary mobile application</p>\r\n', '1113161005.jpg'),
(3, 1, 1, 1, '10', 'ABC co., Ltd.', 'Mr.Vutha', '0123342214', '023494553', 'vutha@gmail.com', 'www.google.com', '<p>erterte</p>\r\n', '<p>ertrtrtertre</p>\r\n', '1398377058.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_type`
--

CREATE TABLE IF NOT EXISTS `tbl_company_type` (
  `type_id` int(10) NOT NULL auto_increment,
  `type_name_en` varchar(100) NOT NULL,
  `type_name_kh` varchar(100) NOT NULL,
  PRIMARY KEY  (`type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_company_type`
--

INSERT INTO `tbl_company_type` (`type_id`, `type_name_en`, `type_name_kh`) VALUES
(1, 'Private Limited Company', 'ក្រុមហ៊ុនឯកជនមានកំណត់');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_function`
--

CREATE TABLE IF NOT EXISTS `tbl_function` (
  `func_id` int(11) NOT NULL auto_increment,
  `func_name_en` varchar(50) NOT NULL,
  `func_name_kh` varchar(50) character set ucs2 NOT NULL,
  PRIMARY KEY  (`func_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_function`
--

INSERT INTO `tbl_function` (`func_id`, `func_name_en`, `func_name_kh`) VALUES
(1, ' Accounting', 'គណនេយ្យ'),
(2, 'Administration', 'រដ្ឋបាល');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_industry`
--

CREATE TABLE IF NOT EXISTS `tbl_industry` (
  `ind_id` int(11) NOT NULL auto_increment,
  `ind_name_en` varchar(100) NOT NULL,
  `ind_name_kh` varchar(100) NOT NULL,
  PRIMARY KEY  (`ind_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_industry`
--

INSERT INTO `tbl_industry` (`ind_id`, `ind_name_en`, `ind_name_kh`) VALUES
(1, 'Accounting/Audit/Tax Services', 'គណនេយ្យ'),
(2, ' Administration', 'រដ្ឋបាល');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobs`
--

CREATE TABLE IF NOT EXISTS `tbl_jobs` (
  `job_id` int(10) NOT NULL auto_increment,
  `com_id` int(11) NOT NULL,
  `func_id` int(10) NOT NULL,
  `job_title` varchar(250) NOT NULL,
  `job_exp` varchar(250) NOT NULL,
  `job_level` varchar(250) NOT NULL,
  `job_term` varchar(250) NOT NULL,
  `job_hiring` varchar(250) NOT NULL,
  `job_salary` varchar(250) NOT NULL,
  `job_qual` varchar(250) NOT NULL,
  `job_sex` varchar(50) NOT NULL,
  `job_lang` varchar(100) NOT NULL,
  `job_age` varchar(10) NOT NULL,
  `job_pub_date` date NOT NULL,
  `job_close_date` date NOT NULL,
  `job_desc` text NOT NULL,
  `job_require` text NOT NULL,
  PRIMARY KEY  (`job_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_jobs`
--

INSERT INTO `tbl_jobs` (`job_id`, `com_id`, `func_id`, `job_title`, `job_exp`, `job_level`, `job_term`, `job_hiring`, `job_salary`, `job_qual`, `job_sex`, `job_lang`, `job_age`, `job_pub_date`, `job_close_date`, `job_desc`, `job_require`) VALUES
(3, 3, 1, 'Web Developers', '2 Years', 'Entrys', 'Full Times', '3', '400$', 'Bachelor Degrees', 'Male/Female', 'English-goods', '23-35', '2015-09-10', '2015-09-29', '<p>- Have a creative ideas in website<br />\r\n- Creating a site or pages<br />\r\n- Design, layout and coding, this can mean working on a brand new website or updating an already existing site.</p>\r\n', '<p>- Cambodia citizen<br />\r\n- Experience of working in relates to this field is preferable<br />\r\n- Good command in English</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `m_id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL,
  `m_name_en` varchar(50) NOT NULL,
  `m_name_kh` varchar(50) NOT NULL,
  `m_url` varchar(500) NOT NULL,
  PRIMARY KEY  (`m_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`m_id`, `parent_id`, `m_name_en`, `m_name_kh`, `m_url`) VALUES
(0, 0, 'Main Menu', 'មីនុយដើម', '0'),
(2, 0, 'Our Services', 'សេវាកម្ម​ របស់យើង', 'financeplustraining.com'),
(3, 2, 'Basic Jobs', 'ការងារចំបង', 'http://google.com'),
(4, 5, 'Urgent Jobs', 'ការងារ​ ប្រញាប់', '3'),
(5, 0, 'About us', 'អំពីយើង', '4'),
(7, 5, 'Promotion', 'បញ្ចុះតំលៃ', 'financeplustraining.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_province`
--

CREATE TABLE IF NOT EXISTS `tbl_province` (
  `prov_id` int(10) NOT NULL auto_increment,
  `prov_name_en` varchar(50) NOT NULL,
  `prov_name_kh` varchar(50) NOT NULL,
  PRIMARY KEY  (`prov_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_province`
--

INSERT INTO `tbl_province` (`prov_id`, `prov_name_en`, `prov_name_kh`) VALUES
(1, 'Phnom Penh', 'ភ្នំពេញ'),
(3, 'Kompong Cham', 'កំពង់ចាម'),
(4, 'kandal', 'កណ្តាល'),
(5, 'Prey Veng', 'ព្រៃវែង'),
(6, 'Siem Reap', 'សៀមរាប'),
(7, 'Kompong Thom', 'កំពង់ធំ'),
(8, 'Batambong', 'បាត់ដំបង'),
(9, 'kompong Chnang', 'កំពង់ឆ្នាំង');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` date NOT NULL,
  `user_description` longtext NOT NULL,
  `user_type` int(11) NOT NULL,
  `email` varchar(100) character set utf8 NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `full_name`, `password`, `register_date`, `user_description`, `user_type`, `email`) VALUES
(1, 'admin', 'supper administrator', '750ebbb77044187afa7851138dcbb660', '2015-03-20', 'for supper admin', 1, 'bunhor.khim@gmail.com'),
(73, 'normal', 'normal', '827ccb0eea8a706c4c34a16891f84e7b', '2015-09-10', 'normal', 2, 'normal@gmail.com'),
(71, 'aseanhr', 'admin page', '827ccb0eea8a706c4c34a16891f84e7b', '2015-09-10', 'for admin page', 1, 'info@aseanhr.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
