<?php session_start();
require_once("../_config/connect.php");
if($page=="logout"){
  session_destroy();
  ?>
  <script type="text/javascript">
        window.location.href="../";
  </script>
<?php   
}

if(isset($_SESSION['login']) && $_SESSION['login']){
	$userlog=$_SESSION['login'];
	$user_id=$_SESSION["user_id"];
	$type=$_SESSION["type"];
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ASEAN HR System Administrator</title>

    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <script src="js/ck/ckeditor.js"></script>
    <link rel="stylesheet" type="text/css" href="css/datepicker.css"/>
    
</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">MENU</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ASEAN HR ADMINISTRATOR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li><a style='color:#FFF'>&nbsp;<i class="fa fa-calendar fa-fw"></i> <?php echo date("d / F / Y");?></a></li>
                <li><a style='color:#FFF'>&nbsp;<i class="fa fa-clock-o fa-fw"></i> <?php echo date("h:i:s A") ?></a></li>
                <li class="dropdown hidden-xs">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='color:#FFF'>
                        <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span>
                        <i class="fa fa-caret-down"></i>
                    </a>
    			  	<ul class="dropdown-menu dropdown-user">
                         <li><a href="#"><i class="fa fa-user fa-fw" style='color:#1b69b3'></i> <b style='color:#1b69b3'><?php echo $_SESSION['name'];?></b></a>
                        <li><a href="?page=logout"><i class="fa fa-sign-out fa-fw" style='color:#ff414c;'></i><b style='color:#ff414c;'>Logout</b></a>
                        </li>
                    </ul>
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <?php include('include/navigation.php');?>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <?php
                if($page==''){
                    include('include/dashbord.php');                
                }else if($page=='location'){
                    include('include/location/location.php');
                }else if($page=='editlocation'){
                    include('include/location/edit_location.php');
                }else if($page=='menu'){
                    include('include/menu/menu.php');
                }else if($page=='editmenu'){
                    include('include/menu/edit_menu.php');
                }else if($page=='industry'){
                    include('include/industry/industry.php');
                }else if($page=='editindustry'){
                    include('include/industry/edit_industry.php');
                }else if($page=='function'){
                    include('include/function/function.php');
                }else if($page=='editfunction'){
                    include('include/function/edit_function.php');
                }else if($page=='com_type'){
                    include('include/company_type/com_type.php');
                }else if($page=='editcom_type'){
                    include('include/company_type/edit_com_type.php');
                }else if($page=='com_pro'){
                    include('include/company_profile/com_pro.php');
                }else if($page=='editcom_pro'){
                    include('include/company_profile/edit_com_pro.php');
                }else if($page=='jobs'){
                    include('include/jobs/jobs.php');
                }else if($page=='editjob'){
                    include('include/jobs/edit_jobs.php');
                }else if($page=='user'){
                    include('include/user/user.php');
                }else if($page=='edituser'){
                    include('include/user/edit_user.php');
                }else if($page=='resetpassword'){
                    include('include/user/reset_password.php');
                }else if($page=='banner'){
                    include('include/banner/banner.php');
                }else if($page=='edit_banner'){
                    include('include/banner/edit_banner.php');
                }else if($page=='edit_contact_per'){
                    include('include/jobs/edit_contact_per.php');
                }else if($page=='job_learning'){
                    include('include/job_learning/job_learning.php');
                }else if($page=='edit_job_learning'){
                    include('include/job_learning/edit_job_learning.php');
                }else if($page=='services'){
                    include('include/services/services.php');
                }else if($page=='edit_services'){
                    include('include/services/edit_services.php');
                }else if($page=='feature'){
                    include('include/feature/feature.php');
                }else if($page=='edit_feature'){
                    include('include/feature/edit_feature.php');
                }else if($page=='package'){
                    include('include/package/package.php');
                }else if($page=='edit_package'){
                    include('include/package/edit_package.php');
                }else if($page=='payment'){
                    include('include/payment/payment.php');
                }else if($page=='active_job'){
                    include('include/payment/active_jobs.php');
                }

            ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    	<center>
    	<span style="line-height:30px; text-align:center; color:#FFF;">All Rights Reserved By ASEANHR</span> &nbsp;&nbsp;|
       &nbsp;&nbsp;<span style="line-height:30px; text-align:center; color:#FFF;"> Develop By <a href="http://khemarahost.com" target="_blank" style="color:#F00;">Khemara Host</a></span>
    	</center>
    </nav>

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });

    </script>
    <script type="text/javascript">
    $('.input-group').datepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01",
        endDate: "2060-01-01",
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true
    });  
    </script> 
    <script>
        CKEDITOR.replace( 'editor1',
            {
                filebrowserBrowseUrl : 'js/ck/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : 'js/ck/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : 'js/ck/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : 'js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : 'js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : 'js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            }
            );
        CKEDITOR.replace( 'editor2',
            {
                filebrowserBrowseUrl : 'js/ck/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : 'js/ck/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : 'js/ck/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : 'js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : 'js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : 'js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            }
            );
            
    </script>
</body>

</html>
<?php
    }else{
?>
    <script type="text/javascript">
        window.location.href="../";
    </script>
<?php
    }
?>