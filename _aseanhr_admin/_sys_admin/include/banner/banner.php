<?php

if($_SESSION["type"] != 1){
    echo "No permission to access"; die();
}
	
function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}
$upload_dir = "../../images/banner/";   
if (isset($_POST['btsave'])) {
    $cat_id = $_POST['cat_id'];
    $ban_url = $_POST['ban_url'];

    if (isset($_FILES["file"])) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Error: " . $_FILES["file"]["error"] . "<br>";
        } else {
            //===function random name
            $image = $_FILES['file']['name'];
            $tempext = getExtension($image);
            $extfile = mt_rand() . "." . $tempext;
            //===function random name
            move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir . $extfile);
            //echo "Uploaded File :" . $_FILES["myfile"]["name"];
            $url=$extfile;  
                        
        }
    }
//======end upload file
    mysqli_query($conn,'SET NAMES utf8');
    mysqli_query($conn,"insert into `tbl_banner` values('',$cat_id,'$url','$ban_url')");
    ?>
    <script type="text/javascript">
        window.location.href="?page=banner";
    </script>
    <?php   
}   
?> 
 

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Banner</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                            <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                                <div class="form-group">
                                   <input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;"/>
                                    <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;" disabled="disabled"/>       </div>
                          </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Banner Type</label>
                                <select class="form-control" name="cat_id" required>
                                        <option value=""></option>
                                        <option value="1">Head Banner</option>
                                        <option value="2">Banner Slider</option>
                                        <option value="3">Left Advertising</option>
                                </select>
                            </div>  
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Url Link</label>
                                <input class="form-control" placeholder="Url Link" name="ban_url"/>
                            </div>  
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">   
                            <div class="form-group">
                                <label style="color:#0099ff;">Company logo</label>
                                <input type="file" name="file" required>
                            </div>
                        </div>
                </div>
            </form>
            </div>
       </div>
    </div>

</div>  
       <!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
                                <th>Categoty</th>
                                <th>Images</th>
		                        <th>Url Link</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"SELECT * FROM tbl_banner ORDER BY cat_id ASC")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
                            ?>
		                    <tr>
		                        <td><?=$i;?></td>
                                <td><?php if($row['cat_id']==1){echo "Head Slider";}else if($row['cat_id']==2){echo "Banner Slider";}else if($row['cat_id']==3){echo "Left Advertising";}?></td>
                                <td><img src="../../images/banner/<?= $row['ban_image'];?>" width='300'/></td>
		                        <td><?= $row['ban_url'];?></td>                                
                                <td align="center"><a href="?page=edit_banner&ban_id=<?php echo $row['ban_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['ban_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['ban_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <img src="../../images/banner/<?= $row['ban_image'];?>" width='300'/>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/banner/delete_banner.php<?php echo '?ban_id='.$row['ban_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>