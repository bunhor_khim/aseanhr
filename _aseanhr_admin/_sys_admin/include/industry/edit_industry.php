<?php

if($_SESSION["type"] != 1){
	echo "No permission to access"; die();
} 
    $ind_id=$_GET['ind_id'];

    if (isset($_POST['btupdate'])) {
        $name_en  = $_POST['name_en'];
        $name_kh  = $_POST['name_kh'];
        
        mysqli_query($conn,'SET NAMES utf8');
        $sql=mysqli_query($conn,"update `tbl_industry` SET `ind_name_en`='$name_en',`ind_name_kh`='$name_kh' where ind_id=$ind_id") or die(mysql_error());
        ?>
        <script type="text/javascript">
            window.location.href="?page=industry";
        </script>
        <?php   
    }   
?> 

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Industry</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <?php
						mysqli_query($conn,'SET NAMES utf8');
                        $sql=mysqli_query($conn,"select * from tbl_industry where ind_id=$ind_id");
                        while($row2=mysqli_fetch_array($sql)){
                    ?>
                    <form method="post" enctype="multipart/form-data">
                        <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                            <div class="form-group">
                            	<input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;" disabled="disabled"/>
                                <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;"/>
                            </div>
                       	</div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">English Title</label>
                                <input class="form-control" placeholder="English Title" name="name_en" value="<?php echo $row2['ind_name_en'] ?>"/>
                            </div>  
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                            	<label style="color:#0099ff;">Khmer Title</label>
                            	<input class="form-control" placeholder="Khmer Title" name="name_kh" value="<?php echo $row2['ind_name_kh'] ?>"/>
                        	</div>
                        </div>                    
                    </form>
                    <?php } ?>
                </div>
            </div>
       </div>
    </div>
</div>  
<!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th width='30'>ID</th>
                                <th>Industry EN Name</th>
                                <th>Industry KH Name</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"select * from tbl_industry")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
                            ?>
                            <tr>
                                <td><?=$i;?></td>
                                <td><?= $row['ind_name_en'];?></td>
                                <td><?= $row['ind_name_kh'];?></td>
                                <td align="center"><a href="?page=editindustry&ind_id=<?php echo $row['ind_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['ind_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
                            </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['ind_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['ind_name_en']; ?>&nbsp; > &nbsp;<?php echo $row['ind_name_kh']; ?></label>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/industry/ddelete_industry.php<?php echo '?ind_id='.$row['ind_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>