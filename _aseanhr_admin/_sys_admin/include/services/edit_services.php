<?php
if($_SESSION["type"] != 1){
	echo "No permission to access"; die();
}   
    $ser_id=$_GET['ser_id'];

    if (isset($_POST['btupdate'])) {
        $ser_cate = $_POST['ser_cate'];
        $ser_title  = $_POST['ser_title'];
        $ser_desc  = $_POST['ser_desc'];
        
        mysqli_query($conn,'SET NAMES utf8');
        $sql=mysqli_query($conn,"update `tbl_services` SET `ser_cate`=$ser_cate,`ser_title`='$ser_title',`ser_desc`='$ser_desc' where ser_id=$ser_id") or die(mysql_error());
        ?>
        <script type="text/javascript">
            window.location.href="?page=services";
        </script>
        <?php   
    }   
?> 
<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Services</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                	 <?php
                        $sql=mysqli_query($conn,"select * from tbl_services where ser_id=$ser_id");
                        mysqli_query($conn,'SET NAMES utf8');
                        while($row2=mysqli_fetch_array($sql)){
                    ?>
                    <form method="post" enctype="multipart/form-data">
                    <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                        <div class="form-group">
                        	<input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;" disabled="disabled"/>
                            <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;" />                        
                        </div>
                   	</div>
                     <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Category</label>
                            <select class="form-control" name="ser_cate">
                                    <option <?php if($row2['ser_cate']==1){echo 'selected="selected"';}?> value="1">Services</option>
                                    <option <?php if($row2['ser_cate']==2){echo 'selected="selected"';}?> value="2">About us</option>
                                    <option <?php if($row2['ser_cate']==3){echo 'selected="selected"';}?> value="3">Contact us</option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Title</label>
                            <input class="form-control" placeholder="Services Title" name="ser_title" value="<?=$row2['ser_title'];?>"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px; width:100%;">
                        <div class="form-group">
                            <label style="color:#0099ff;">job Description</label>
                            <textarea class="ckeditor" cols="80" id="editor1" name="ser_desc" rows="20"><?= $row2['ser_desc'];?></textarea>
                        </div>  
                    </div>
                </form>
                <?php } ?>
                </div>
            </div>
       </div>
    </div>
</div>  
       <!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='80'>Services ID</th>
                                <th>Services Category</th>
		                        <th>Services title</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"select * from tbl_services order by ser_id DESC")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
                            ?>
		                    <tr>
		                        <td><?=$row['ser_id'];?></td>
                                 <td><?php if($row['ser_cate']==1){echo "Services";}else if($row['ser_cate']==2){echo "About us";}else if($row['ser_cate']==3){echo "Contact us";}?></td>
		                        <td><?= $row['ser_title'];?></td>
                                <td align="center"><a href="?page=edit_services&ser_id=<?php echo $row['ser_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['ser_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['ser_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['ser_title']; ?></label>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/services/delete_services.php<?php echo '?ser_id='.$row['ser_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>