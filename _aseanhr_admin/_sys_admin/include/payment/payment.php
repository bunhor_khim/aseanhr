<?php
if($_SESSION["type"] != 1){
	echo "No permission to access"; die();
}
   
    if (isset($_POST['btsave'])) {
        $pk_name     = $_POST['pk_name'];
        $pk_price    = $_POST['pk_price'];
		$pk_post     = $_POST['pk_post'];
		$pk_duration = $_POST['pk_duration'];
		$pk_desc     = $_POST['pk_desc'];
		
        $sql="insert into `tbl_package` values('','$pk_name',$pk_price,$pk_duration,$pk_post,'$pk_desc')";
            //echo $sql;die();
        mysqli_query($conn,'SET NAMES utf8');
        mysqli_query($conn,$sql);
        ?>
        <script type="text/javascript">
            window.location.href="?page=package";
        </script>
        <?php   
    }   
?> 

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Payments</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
 
<!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
		                        <th>Package Name</th>
		                        <th>Comapny Name</th>
                                <th>Pay From</th>
                                <th>Payment Code</th>
                                <th>Payment Date</th>
                                <th width='90'>Active Jobs</th>
                                <th width='90'>Paid / Unpaid</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                               $c_id = 0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"select * from tbl_payment 
								LEFT JOIN tbl_package ON tbl_payment.pk_id=tbl_package.pk_id
								LEFT JOIN tbl_company ON tbl_payment.com_id=tbl_company.com_id
								ORDER BY tbl_company.com_id DESC")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $newDate = date("d-m-Y", strtotime($row['pm_date']));
								 
								 $id = $row['com_id'];
								                            
															
							?>
		                    <tr>
		                        <td><?=$row['pm_id'];?></td>
		                        <td><?= $row['pk_name'];?></td>
		                        <td><?= $row['com_name'];?></td>
                                <td><?= $row['pm_from'];?></td>
                                <td><?= $row['pm_code'];?></td>
                                 <td><?= $newDate;?></td>
                                <td align="center">
								<?php
                                	if($id == $c_id ){
								  echo "&nbsp;";
								 }
								 else{
								  $c_id = $id;
								 ?>
                                 <a href="?page=active_job&pk_id=<?= $row['pk_id'];?>&com_id=<?= $row['com_id'];?>" title="Active Jobs"><b style="color:#00F;">Active Jobs</b></a>
                                 <?php
								 }
								?>
                                </td>
                                <td align="center">
                                	<?php
										if($row['pm_type'] !=1){
									?>
                                    <a href="#myModal<?php echo $row['pm_id']; ?>" data-toggle="modal" title="Inactive">
                                    <b style='color:#F00;'>Unpaid</b>
                                    </a>
                                    <?php
										}
									?>
                                	<?php
										if($row['pm_type'] !=0){
									?>
                                    <a href="#myModal1<?php echo $row['pm_id']; ?>" data-toggle="modal" title="Active">
                                    <b style='color:#0C3;'>Paid</b></a>
                                    <?php
										}
									?>
                                </td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['pm_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure this user has been paid?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['pk_name']; ?>&nbsp; ></label> 
                                        <label style="font-size:18px;"><?php echo $row['com_name']; ?>&nbsp; ></label> 
                                        <label style="font-size:18px;"><?php echo $row['pm_from']; ?></label>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/payment/active_payment.php<?php echo '?pm_id='.$row['pm_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            
                            <div class="modal small fade" id="myModal1<?php echo $row['pm_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure unpaid this user?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['pk_name']; ?>&nbsp; ></label> 
                                        <label style="font-size:18px;"><?php echo $row['com_name']; ?>&nbsp; ></label> 
                                        <label style="font-size:18px;"><?php echo $row['pm_from']; ?></label>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/payment/inactive_payment.php<?php echo '?pm_id='.$row['pm_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>