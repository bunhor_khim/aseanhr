<?php
if($_SESSION["type"] != 1){
	echo "No permission to access"; die();
}   
    $pk_id=$_GET['pk_id'];
	$com_id=$_GET['com_id'];
?>

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Active Jobs</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
                                <th>Position</th>
                                <th>Company Name</th>
		                        <th>Function Name</th>
                                <th>Job Term</th>
                                <th>Job Hiring</th>
                                <th>Job Alert</th>
                                <th>Publish Date</th>
                                <th>Closing Date</th>
                                <th width='150' style="text-align:center;">Active / Inactive</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"SELECT t1.*,t2.func_id,t2.func_name_en,t3.com_id,t3.com_name FROM tbl_jobs t1 LEFT JOIN tbl_function t2 ON t1.func_id=t2.func_id LEFT JOIN tbl_company t3 ON t1.com_id=t3.com_id where t3.com_id = $com_id AND job_package=$pk_id ")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
								$pubDate = date("d-m-Y", strtotime($row['job_pub_date']));
								$closeDate = date("d-m-Y", strtotime($row['job_close_date']));
							?>
		                    <tr>
		                        <td><?=$i;?></td>
                                <td><?= $row['job_title'];?></td>
                                <td><?= $row['com_name'];?></td>
		                        <td><?= $row['func_name_en'];?></td>
                                <td><?php if($row['job_term']==1){echo "Full Time";}else if($row['job_term']==2){ echo "Part Time";}else if($row['job_term']==3){ echo "Internship";}else if($row['job_term']==4){ echo "Volunteer";}?></td>    
                                <td align="center"><?= $row['job_hiring'];?></td>
                                <td><?php if($row['job_alert']==1){echo "New";}else if($row['job_alert']==2){echo "<b style='color:#F00;';>Urgent</b>";}?></td>
                                <td><?= $pubDate;?></td>
                                <td><?= $closeDate;?></td>
                                <td align="center">
                                	<?php
										if($row['admin_enable'] !=1){
									?>
                                    <a href="#myModal<?php echo $row['job_id']; ?>" data-toggle="modal" title="Inactive">
                                    <b style='color:#F00;'>Inactive</b>
                                    </a>
                                    <?php
										}
									?>
                                	<?php
										if($row['admin_enable'] !=0){
									?>
                                    <a href="#myModal1<?php echo $row['job_id']; ?>" data-toggle="modal" title="Active">
                                    <b style='color:#0C3;'>Active</b></a>
                                    <?php
										}
									?>
                                </td>
                                
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['job_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure to public this jobs?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['job_title']; ?></label><br/>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/payment/enable_admin.php<?php echo '?job_id='.$row['job_id'];?><?php echo '&pk_id='.$row['job_package'];?><?php echo '&com_id='.$row['com_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>


							<div class="modal small fade" id="myModal1<?php echo $row['job_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure to Unpublic this jobs?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['job_title']; ?></label><br/>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/payment/disable_admin.php<?php echo '?job_id='.$row['job_id'];?><?php echo '&pk_id='.$row['job_package'];?><?php echo '&com_id='.$row['com_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>