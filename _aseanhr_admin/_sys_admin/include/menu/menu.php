<?php 
if($_SESSION["type"] != 1){
	echo "No permission to access"; die();
}  
    if (isset($_POST['btsave'])) {
        $m_id     = $_POST['m_id'];
        $name_en  = $_POST['name_en'];
        $name_kh  = $_POST['name_kh'];
        $url = $_POST['url'];
		$order_no =$_POST['order_no'];
        $sql="insert into `tbl_menu` values('',$m_id,'$name_en','$name_kh','$url',$order_no)";
            //echo $sql;die();
        mysqli_query($conn,'SET NAMES utf8');
        mysqli_query($conn,$sql);
        ?>
        <script type="text/javascript">
            window.location.href="?page=menu";
        </script>
        <?php   
    }   
?> 

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Menu</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <form method="post" enctype="multipart/form-data">
                    <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                        <div class="form-group">
                        	<input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;"/>
                            <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;" disabled="disabled"/>                        </div>
                   	    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Main Menu</label>
                            <select class="form-control" name="m_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_menu where parent_id = 0 ");
                                        while($row=mysqli_fetch_array($sql)){
                                            $m_id=$row['m_id'];
                                            $m_name = $row['m_name_en'];
                                    ?>  
                                        <option style="font-weight:bold;" value="<?php echo $row['m_id'];?>"><?php echo $m_name;?></option>
                                        <?php 
                                            $sql1=mysqli_query($conn,"select * from tbl_menu where parent_id = $m_id AND m_id !=0 AND parent_id !=0");
                                            while($row1=mysqli_fetch_array($sql1)){
                                                $m_name1 = $row1['m_name_en'];
                                        ?> 
                                            <option value="<?php echo $row1['m_id'];?>" style="font-weight:normal;">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $m_name1;?></option>
                                        <?php } ?>
                                   <?php } ?>
                                </select>
                        </div>  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Menu Name EN</label>
                            <input class="form-control" placeholder="Menu Name EN" name="name_en" required/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        	<label style="color:#0099ff;">Menu Name KH</label>
                        	<input class="form-control" placeholder="Khmer Title" name="name_kh" required/>
                    	</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Url Link</label>
                            <input class="form-control" placeholder="Url Link" name="url" required/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Order No</label>
                            <input class="form-control" placeholder="Order No" name="order_no" required/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label><b style="color:#F00;">*</b> Services : <span style="font-weight:normal;">services.php?ser_id=<b style="color:#F00;">id</b></span></label><br/>
                            <label><b style="color:#F00;">*</b> About us : <span style="font-weight:normal;">aboutus.php?ser_id=<b style="color:#F00;">id</b></span></label><br/>
                        </div>
                    </div>
                </form>
                </div>
            </div>
       </div>
    </div>
</div>  
       <!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
		                        <th>Main Menu</th>
		                        <th>English Menu</th>
                                <th>Khmer Menu</th>
                                <th>Url Link</th>
                                <th>Order No</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                function getmenu($parentId){                             
                                    global $conn;
                                    $sql = "select m_name_en from tbl_menu where m_id=$parentId";
                                    $result = mysqli_query($conn, $sql);
                                    $row = mysqli_fetch_array($result);
                                    return $row['m_name_en'];
                                }   
                            ?>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"select * from tbl_menu where m_id !=0 order by parent_id ASC")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
                            ?>
		                    <tr>
		                        <td><?=$i;?></td>
		                        <td><b style="color:#F00"><?php echo getmenu($row['parent_id']); ?></b></td>
                                <td><?php echo $row['m_name_en']; ?></td>
                                <td><?php echo $row['m_name_kh']; ?></td>
                                <td><?php echo $row['m_url']; ?></td>
                                <td><?php echo $row['order_no']; ?></td>
                                <td align="center"><a href="?page=editmenu&m_id=<?php echo $row['m_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['m_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['m_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><b style="color:#F00"><?php echo getmenu($row['parent_id']); ?></b> > <?php echo $row['m_name_en']; ?></label>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/menu/delete_menu.php<?php echo '?m_id='.$row['m_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>