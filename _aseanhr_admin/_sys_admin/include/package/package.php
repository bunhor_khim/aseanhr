<?php
if($_SESSION["type"] != 1){
	echo "No permission to access"; die();
}
   
    if (isset($_POST['btsave'])) {
        $pk_name     = $_POST['pk_name'];
        $pk_price    = $_POST['pk_price'];
		$pk_post     = $_POST['pk_post'];
		$pk_duration = $_POST['pk_duration'];
		$pk_desc     = $_POST['pk_desc'];
		
        $sql="insert into `tbl_package` values('','$pk_name',$pk_price,$pk_duration,$pk_post,'$pk_desc')";
            //echo $sql;die();
        mysqli_query($conn,'SET NAMES utf8');
        mysqli_query($conn,$sql);
        ?>
        <script type="text/javascript">
            window.location.href="?page=package";
        </script>
        <?php   
    }   
?> 

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Packages</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <form method="post" enctype="multipart/form-data">
                    <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                        <div class="form-group">
                        	<input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;"/>
                            <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;" disabled="disabled"/>                        </div>
                   	    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Package Name</label>
                            <input class="form-control" placeholder="Package Name" name="pk_name" required/>
                        </div>  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        	<label style="color:#0099ff;">Package Price</label>
                        	<input class="form-control" placeholder="Package Price" name="pk_price" required/>
                    	</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        	<label style="color:#0099ff;">Package Post</label>
                        	<input class="form-control" placeholder="Package Post" name="pk_post" required/>
                    	</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        	<label style="color:#0099ff;">Package Duration</label>
                        	<input class="form-control" placeholder="Package Duration" name="pk_duration" required/>
                    	</div>
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px; width:100%">
                        <div class="form-group">
                            <label style="color:#0099ff;">Package Description</label>
                            <textarea class="form-control" cols="80" name="pk_desc" rows="7"></textarea>
                        </div>  
                    </div>
                </form>
                </div>
            </div>
       </div>
    </div>
</div>  
       <!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
		                        <th>Package Namee</th>
		                        <th>Package Price</th>
                                <th>Package Post</th>
                                <th>Package Duration</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"select * from tbl_package")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
                            ?>
		                    <tr>
		                        <td><?=$i;?></td>
		                        <td><?= $row['pk_name'];?></td>
		                        <td>$ <?= $row['pk_price'];?></td>
                                <td><?= $row['pk_post'];?> Post</td>
                                <td><?= $row['pk_duration'];?> Day</td>
                                <td align="center"><a href="?page=edit_package&pk_id=<?php echo $row['pk_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['pk_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['pk_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['pk_name']; ?></label>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/package/delete_package.php<?php echo '?pk_id='.$row['pk_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>