<ul class="nav" id="side-menu" style="background:#f1efef;">
    <li>
        <a href="index.php" style="background:#06F; color:#FFF;"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
    </li>
    <?php
        if($type==1){?>
			<li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'banner' || isset($_GET['page']) && $_GET['page'] == 'editbanner') echo "class='active'"; ?> href='?page=banner'><i class='fa fa-picture-o fa-fw'></i> Banner</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'feature' || isset($_GET['page']) && $_GET['page'] == 'edit_feature') echo "class='active'"; ?> href='?page=feature'><i class='fa fa-picture-o fa-fw'></i> Feature Images</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'job_learning' || isset($_GET['page']) && $_GET['page'] == 'edit_job_learning') echo "class='active'"; ?> href='?page=job_learning'><i class='fa fa-exclamation-circle fa-fw'></i> Job Learning</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'services' || isset($_GET['page']) && $_GET['page'] == 'edit_services') echo "class='active'"; ?> href='?page=services'><i class='fa fa-pencil fa-fw'></i> Services</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'menu' || isset($_GET['page']) && $_GET['page'] == 'editmenu') echo "class='active'"; ?> href='?page=menu'><i class='fa fa-folder-o fa-fw'></i> Menu</a></li>   
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'location' || isset($_GET['page']) && $_GET['page'] == 'editlocation') echo "class='active'"; ?> href='?page=location'><i class='fa fa-location-arrow fa-fw'></i> Location</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'industry' || isset($_GET['page']) && $_GET['page'] == 'editindustry') echo "class='active'"; ?> href='?page=industry'><i class='fa fa-list-alt fa-fw'></i> Industry</a></li></li>
			<li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'function' || isset($_GET['page']) && $_GET['page'] == 'editfunction') echo "class='active'"; ?> href='?page=function'><i class='fa fa-list-alt fa-fw'></i> Function</a></li></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'com_type' || isset($_GET['page']) && $_GET['page'] == 'editcom_type') echo "class='active'"; ?> href='?page=com_type'><i class='fa fa-book fa-fw'></i> Company Type</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'com_pro' || isset($_GET['page']) && $_GET['page'] == 'editcom_pro' ) echo "class='active'"; ?> href='?page=com_pro'><i class='fa fa-retweet fa-fw'></i> Company Profile</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'jobs' || isset($_GET['page']) && $_GET['page'] == 'editjob') echo "class='active'"; ?> href='?page=jobs'><i class='fa fa-upload fa-fw'></i> Jobs</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'package' || isset($_GET['page']) && $_GET['page'] == 'edit_package') echo "class='active'"; ?> href='?page=package'><i class='fa fa-paperclip fa-fw'></i> Packages</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'payment' || isset($_GET['page']) && $_GET['page'] == 'active_job') echo "class='active'"; ?> href='?page=payment'><i class='fa fa-money fa-fw'></i> Payments</a></li>
            <li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'user' || isset($_GET['page']) && $_GET['page'] == 'edituser') echo "class='active'"; ?> href='?page=user'><i class='fa fa-fw fa-user'></i> User Management</a></li>
        <?php
        }else{
	  	?>
        	<li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'com_pro' || isset($_GET['page']) && $_GET['page'] == 'editcom_pro' ) echo "class='active'"; ?> href='?page=com_pro'><i class='fa fa-retweet fa-fw'></i> Company Profile</a></li>
			<li><a <?php if(isset($_GET['page']) && $_GET['page'] == 'jobs' || isset($_GET['page']) && $_GET['page'] == 'editjob') echo "class='active'"; ?> href='?page=jobs'><i class='fa fa-upload fa-fw'></i> Jobs</a></li>
	  	<?php  
			}
	  	?>
</ul>
<!-- /#side-menu -->