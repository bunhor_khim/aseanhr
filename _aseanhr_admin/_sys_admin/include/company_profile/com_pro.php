<?php

function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}
$upload_dir = "../../employer/images/";   
if (isset($_POST['btsave'])) {
    $ind_id = $_POST['ind_id'];
    $type_id = $_POST['type_id'];
    $prov_id = $_POST['prov_id'];
    $employee= $_POST['employee'];
    $com_name= $_POST['com_name'];
    $contact_per= $_POST['contact_per'];
    $contact_phone= $_POST['contact_phone'];
    $contact_fax= $_POST['contact_fax'];
    $contact_email= $_POST['contact_email'];
    $contact_web= $_POST['contact_web'];
    $contact_addr= $_POST['contact_addr'];
    $contact_desc= $_POST['contact_desc'];

    if (isset($_FILES["file"])) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Error: " . $_FILES["file"]["error"] . "<br>";
        } else {
            //===function random name
            $image = $_FILES['file']['name'];
            $tempext = getExtension($image);
            $extfile = mt_rand() . "." . $tempext;
            //===function random name
            move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir . $extfile);
            //echo "Uploaded File :" . $_FILES["myfile"]["name"];
            $url=$extfile;  
                        
        }
    }
//======end upload file
    mysqli_query($conn,'SET NAMES utf8');
    mysqli_query($conn,"insert into `tbl_company` (com_id,ind_id,type_id,prov_id,com_emp,com_name,com_contact,com_phone,com_fax,com_email,com_website,com_addr,com_desc,com_image) values('',$ind_id,$type_id,$prov_id,'$employee','$com_name','$contact_per','$contact_phone','$contact_fax','$contact_email','$contact_web','$contact_addr','$contact_desc','$url')");
    ?>
    <script type="text/javascript">
        window.location.href="?page=com_pro";
    </script>
    <?php   
}   
?> 
 

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Company Profile</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                            <div class="form-group">
                        	   <input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;"/>
                                <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;" disabled="disabled"/>                        </div>
                   	        </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Industry Type</label>
                            <select class="form-control" name="ind_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_industry");
                                        while($row=mysqli_fetch_array($sql)){
                                            $m_name = $row['ind_name_en'];
                                    ?>  
                                        <option value="<?php echo $row['ind_id'];?>"><?php echo $m_name;?></option>
                                   <?php } ?>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Company Type</label>
                            <select class="form-control" name="type_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_company_type");
                                        while($row=mysqli_fetch_array($sql)){
                                            $type_name = $row['type_name_en'];
                                    ?>  
                                        <option value="<?php echo $row['type_id'];?>"><?php echo $type_name;?></option>
                                   <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Location</label>
                            <select class="form-control" name="prov_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_province");
                                        while($row=mysqli_fetch_array($sql)){
                                            $prov_name = $row['prov_name_en'];
                                    ?>  
                                        <option value="<?php echo $row['prov_id'];?>"><?php echo $prov_name;?></option>
                                   <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">employees</label>
                            <input class="form-control" placeholder="employee" name="employee"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Company Name</label>
                            <input class="form-control" placeholder="Company Name" name="com_name" required/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Contact Person</label>
                            <input class="form-control" placeholder="Contact Person" name="contact_per"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Contact Phone</label>
                            <input class="form-control" placeholder="Contact Phone" name="contact_phone"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Fax</label>
                            <input class="form-control" placeholder="Fax" name="contact_fax"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Contact Email</label>
                            <input type="email" class="form-control" placeholder="Contact Email" name="contact_email"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">WebSite</label>
                            <input class="form-control" placeholder="WebSite" name="contact_web"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Contact Address</label>
                            <textarea class="form-control" cols="80" name="contact_addr" rows="5"></textarea>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Contact Description</label>
                            <textarea class="form-control" cols="80" name="contact_desc" rows="5"></textarea>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">   
                        <div class="form-group">
                            <label style="color:#0099ff;">Company logo</label>
                            <input type="file" name="file" required>
                        </div>
                    </div>
                </div>
            </form>
            </div>
       </div>
    </div>

</div>  
       <!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
                                <th>Company Name</th>
		                        <th>Industry Type</th>
		                        <th>Company Type</th>
                                <th>Location</th>
                                <th>Contact Person</th>
                                <th>Contact Phone</th>
                                <th>Contact Email</th>
                                <th>Logo</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"SELECT t1.*,t2.*,t3.*,t4.* FROM tbl_company t1 LEFT JOIN tbl_industry t2 ON t1.ind_id=t2.ind_id LEFT JOIN tbl_company_type t3 ON t1.type_id=t3.type_id LEFT JOIN tbl_province t4 ON t1.prov_id=t4.prov_id")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
                            ?>
		                    <tr>
		                        <td><?=$i;?></td>
                                <td><?= $row['com_name'];?></td>
		                        <td><?= $row['ind_name_en'];?></td>
		                        <td><?= $row['type_name_en'];?></td>
                                <td><?= $row['prov_name_en'];?></td>
                                <td><?= $row['com_contact'];?></td>
                                <td><?= $row['com_phone'];?></td>
                                <td><?= $row['com_email'];?></td>
                                <td><img src="../../employer/images/<?= $row['com_image'];?>" width='50'/></td>
                                <td align="center"><a href="?page=editcom_pro&com_id=<?php echo $row['com_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['com_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['com_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['com_name']; ?></label><br/>
                                        <img src="../../employer/images/<?= $row['com_image'];?>" width='150'/>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/company_profile/delete_com_pro.php<?php echo '?com_id='.$row['com_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>