<?php

if($_SESSION["type"] != 1){
    echo "No permission to access"; die();
    }

?>
<div class="row">
    <div class="col-lg-12" style="margin-top:-28px;margin-bottom:-13px;">
        <h1 class="page-header" style="font-size:19px; color:#09F;"><i class="fa fa-user fa-fw"></i>&nbsp;User Account</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php       
    if (isset($_POST['btsave'])) {
        $full_name = $_POST['fullname'];
        $username  = $_POST['username'];
        $password  = md5($_POST['password']);
        $desc      = $_POST['desc'];
        $utype     = $_POST['type'];
        $email     = $_POST['email'];
        $date = date('Y-m-d', time());
        mysqli_query($conn,"SET NAMES utf8");
        mysqli_query($conn,"insert into tbl_user values('','$username','$full_name','$password','$date','$desc',$utype,'$email')") or die(mysql_error());
        
        ?>
            <script type="text/javascript">
              window.location.href="?page=user";
            </script>
       <?php 
    }   
?> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <form method="post" enctype="multipart/form-data">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#0099ff;">Full Name</label>
                                <input class="form-control" placeholder="Full Name" name="fullname" required />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#0099ff;">User Name</label>
                                <input class="form-control" placeholder="User Name" name="username" autocomplete="off" required/>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#0099ff;">Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="email" required/>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group" style="padding-bottom:10px;">
                                <label style="color:#0099ff;">User Type</label>
                                <select class="form-control" name="type" required>
                                    <option value="">Please Select</option>
                                    <option value="1">Administrator</option>
                                    <option value="2">Normal User</option>
                            	</select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#0099ff;">Password</label>
                                <input  type="password" class="form-control" placeholder="Password" name="password" autocomplete="off" required/>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group" style="margin-bottom:0px;">
                                <label style="color:#0099ff;">Description</label>
                                <textarea class="form-control" rows="3" name="desc" placeholder="Description"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label></label><br/>
                                <input type="submit" name="btsave" class="btn btn-success" value="&nbsp;Save&nbsp;" title="Save"/>
                                 
                            </div> 
                        </div>
                           
                    </form>
                </div>
            </div>
       </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" style="width:100%;">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th style="text-align:center">No</th>
                                <th style="text-align:center">User Name</th>
                                <th style="text-align:center">Full Name</th>
                                <th style="text-align:center">Email</th>
                                <th style="text-align:center">Register Date</th>
                                <th style="text-align:center">User Type</th>
                                <th style="text-align:center">Edit</th>
                                <th style="text-align:center">Reset</th>
                                <th style="text-align:center">Delete</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $user_id=$_SESSION["user_id"];
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql="select * from tbl_user where user_id !=1";
                                $res=mysqli_query($conn,$sql);
                                $i=0;
                                while($row=mysqli_fetch_array($res)){
                                        
                            ?> 
                            <tr>
                                <td style="font-size:12px;" align="center"><?php echo $i+=1; ?></td>
                                <td style="font-size:12px;"><?php echo $row['username']; ?></td>
                                <td style="font-size:12px;"><?php echo $row['full_name'];?></td>
                                <td style="font-size:12px;"><?php echo $row['email'];?></td>
                                <td style="font-size:12px;"><?php echo $row['register_date']; ?></td>
                                <td style="font-size:12px; width:100px"><?php if($row['user_type']==1){echo"Admin";}else{echo"Normal";}?></td>
                                          
                                <td align="center"><a href="?page=edituser&user_id=<?php echo $row['user_id'] ?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="?page=resetpassword&user_id=<?php echo $row['user_id'] ?>" title="Reset Password"><div class="fa fa-user-md fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['user_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
                            </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['user_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">>Are you sure you want to delete this User?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:19px; color:#09F;">User Name : <?php echo $row['username']; ?></label>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/user/delete_user.php<?php echo '?user_id='.$row['user_id'];?>" ><i class="icon-check"></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <?php } ?>
                           
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->