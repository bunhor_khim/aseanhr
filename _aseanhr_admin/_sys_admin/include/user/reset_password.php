<div class="row">
    <div class="col-lg-12" style="margin-top:-28px;margin-bottom:-13px;">
        <h1 class="page-header" style="font-size:19px; color:#09F;"><i class="fa fa-user fa-fw"></i>&nbsp;Reset Password User</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
if($_SESSION["type"] != 1){
	echo "No permission to access"; die();
	}

 ?>
<?php

		$user_id=$_GET['user_id'];
			
	if (isset($_POST['btupdate'])) {
		$password  = md5($_POST['password']);
		
		mysqli_query($conn,"SET NAMES utf8");
		mysqli_query($conn,"update tbl_user set password='$password' where user_id=$user_id") or die(mysql_error());
		
		//header('location:?page=user');
		?>
		<script type="text/javascript">
            window.location.href="?page=user";
        </script>
        <?php	
	}
?> 

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" style="width:100%;">
            <div class="panel-body">
                <div class="row">
                    
                    	<?php
							$sql="select * from tbl_user where user_id=$user_id";
							mysqli_query($conn,'SET NAMES utf8');
							$query=mysqli_query($conn,$sql);
							while($row=mysqli_fetch_array($query)){
						  ?>
                    <form method="post" enctype="multipart/form-data">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#3a3a3a;">Full Name</label>
                                <input class="form-control" placeholder="Full Name" name="fullname" value="<?php echo $row['full_name'];?>" required readonly />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#3a3a3a;">User Name</label>
                                <input class="form-control" placeholder="User Name" name="username" value="<?php echo $row['username'];?>" required readonly/>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#3a3a3a;"v>Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $row['email'];?>" readonly/>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style="color:#3a3a3a;">Password</label>
                                <input  type="password" class="form-control" placeholder="Password" name="password" required/>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group" style="margin-top:20px;">
                                <label></label>
                                <input type="submit" name="btupdate" class="btn btn-success" value="&nbsp;Update&nbsp;" title="Update" style="margin-bottom:20px; margin-left:0px; width:100px;"/>
                                &nbsp;
                                <a href="?page=user">
                                <input type="button" name="btsave" class="btn btn-primary" value="&nbsp;Back&nbsp;" title="Back" style="margin-bottom:20px; margin-left:0px; width:100px;"/>
                                </a>
                            </div>
                        </div>
                    </form>                        
                         <?php } ?>
                </div>
            </div>
       </div>
   	</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" style="width:100%;">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th style="text-align:center">No</th>
                                <th style="text-align:center">User Name</th>
                                <th style="text-align:center">Full Name</th>
                                <th style="text-align:center">Email</th>
                                <th style="text-align:center">Register Date</th>
                                <th style="text-align:center">User Type</th>
                                <th style="text-align:center">Edit</th>
                                <th style="text-align:center">Reset</th>
                                <th style="text-align:center">Delete</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $user_id=$_SESSION["user_id"];
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql="select * from tbl_user where user_id !=1";
                                $res=mysqli_query($conn,$sql);
                                $i=0;
                                while($row=mysqli_fetch_array($res)){
                                        
                            ?> 
                            <tr>
                                <td style="font-size:12px;" align="center"><?php echo $i+=1; ?></td>
                                <td style="font-size:12px;"><?php echo $row['username']; ?></td>
                                <td style="font-size:12px;"><?php echo $row['full_name'];?></td>
                                <td style="font-size:12px;"><?php echo $row['email'];?></td>
                                <td style="font-size:12px;"><?php echo $row['register_date']; ?></td>
                                <td style="font-size:12px; width:100px"><?php if($row['user_type']==1){echo"Admin";}else{echo"Normal";}?></td>
                                          
                                <td align="center"><a href="?page=edituser&user_id=<?php echo $row['user_id'] ?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="?page=resetpassword&user_id=<?php echo $row['user_id'] ?>" title="Reset Password"><div class="fa fa-user-md fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['user_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
                            </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['user_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">>Are you sure you want to delete this User?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:19px; color:#09F;">User Name : <?php echo $row['username']; ?></label>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/user/delete_user.php<?php echo '?user_id='.$row['user_id'];?>" ><i class="icon-check"></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <?php } ?>
                           
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
