<?php
	$job_id = $_GET['job_id'];
	
	if (isset($_POST['btupdate'])) {
    	$contact_person = $_POST['contact_person'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$web = $_POST['web'];
		$address = $_POST['address'];
		
		 mysqli_query($conn,'SET NAMES utf8');
   		 mysqli_query($conn,"update tbl_jobs SET `contact_person`='$contact_person',`phone`='$phone',`email`='$email',`web`='$web',`address`='$address' WHERE job_id = $job_id");
    ?>
    <script type="text/javascript">
        window.location.href="?page=jobs";
    </script>
    <?php 
	}	
?>
<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Contact Persion</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            	<?php
                    $sql=mysqli_query($conn,"select * from tbl_jobs where job_id=$job_id");
                    mysqli_query($conn,'SET NAMES utf8');
                    while($row2=mysqli_fetch_array($sql)){
                ?>
                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                            <div class="form-group">
                        	   <input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;" disabled="disabled"/>
                                <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;"/>                        
                            </div>
                   	    </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Company Name</label>
                                <select class="form-control" name="com_id" disabled>
                                        <option value=""></option>
                                        <?php 
                                            $sql=mysqli_query($conn,"select * from tbl_company ORDER BY com_name ASC");
                                            while($row=mysqli_fetch_array($sql)){
                                                $com_name = $row['com_name'];
                                                if($row['com_id']== $row2['com_id'])
                                                        $select="selected";
                                                        else
                                                            $select="";
                                            ?>  
                                                <option value="<?php echo $row['com_id'];?>" <?php echo $select;?>><?php echo $com_name;?></option>
                                           <?php } ?>
                                    </select>
							</div>  
						</div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Job Position</label>
                                <input class="form-control" placeholder="Jobs Title" name="job_title" value="<?=$row2['job_title']?>" disabled/>
                            </div>  
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Contact Person</label>
                                <input class="form-control" placeholder="Contact Persion" name="contact_person" value="<?= $row2['contact_person']; ?>"/>
                            </div>  
                        </div>
                    	<div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Contact Phone</label>
                                <input class="form-control" placeholder="Contact Phone" name="phone" value="<?= $row2['phone']; ?>"/>
                            </div>  
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Contact Email</label>
                                <input class="form-control" placeholder="Contact Email" name="email" value="<?= $row2['email']; ?>"/>
                            </div>  
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Web Site</label>
                                <input class="form-control" placeholder="Web Site" name="web" value="<?= $row2['web']; ?>"/>
                            </div>  
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Address</label>  
                            <textarea class="form-control" cols="80" name="address" rows="5"><?=$row2['address']?></textarea>
                        </div>  
                    </div>
                    </div>
                </form>
                <?php } ?>
            </div>
         </div>
	</div>
</div>

<!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
                                <th>Position</th>
                                <th>Company Name</th>
		                        <th>Function Name</th>
                                <th>Job Term</th>
                                <th>Job Hiring</th>
                                <th>Job Salary</th>
                                <th>Publish Date</th>
                                <th>Closing Date</th>
                                <th>C Per</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"SELECT t1.*,t2.func_id,t2.func_name_en,t3.com_id,t3.com_name FROM tbl_jobs t1 LEFT JOIN tbl_function t2 ON t1.func_id=t2.func_id LEFT JOIN tbl_company t3 ON t1.com_id=t3.com_id where user_enable=1 OR admin_enable=1")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
								$pubDate = date("d-m-Y", strtotime($row['job_pub_date']));
								$closeDate = date("d-m-Y", strtotime($row['job_close_date']));
                            ?>
		                    <tr>
		                        <td><?=$i;?></td>
                                <td><?= $row['job_title'];?></td>
                                <td><?= $row['com_name'];?></td>
		                        <td><?= $row['func_name_en'];?></td>
                                <td><?php if($row['job_term']==1){echo "Full Time";}else if($row['job_term']==2){ echo "Part Time";}else if($row['job_term']==3){ echo "Internship";}else if($row['job_term']==4){ echo "Volunteer";}else if($row['job_term']==5){ echo "Urgent";}?></td>    
                                <td align="center"><?= $row['job_hiring'];?></td>
                                <td><?= $row['job_salary'];?></td>
                                <td><?= $pubDate;?></td>
                                <td><?= $closeDate;?></td>
                                <td align="center">
                                	<?php
										if($row['contact_person']==''){
									?>
                                    	<a href="?page=edit_contact_per&job_id=<?php echo $row['job_id'];?>" title="Add new Contact Persion"><div class="fa fa-user fa-fw" style='color:#F00;'></div></a>
                                    <?php }else{?>
                                  		<a href="?page=edit_contact_per&job_id=<?php echo $row['job_id'];?>" title="Add/edit Contact Persion"><div class="fa fa-user fa-fw" style='color:#666;'></div></a>
                                    <?php }?>
                                </td>
                                <td align="center"><a href="?page=editjob&job_id=<?php echo $row['job_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['job_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['job_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['job_title']; ?></label><br/>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/jobs/delete_jobs.php<?php echo '?job_id='.$row['job_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>