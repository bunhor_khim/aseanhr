<?php 
$job_id = $_GET['job_id'];

if (isset($_POST['btupdate'])) {
    
	$com_id = $_POST['com_id'];
    $func_id = $_POST['func_id'];
	$ind_id = $_POST['ind_id'];
    $prov_id = $_POST['prov_id'];
	
    $job_title = $_POST['job_title'];
    $job_term= $_POST['job_term'];
    $job_hiring= $_POST['job_hiring'];
    $job_salary= $_POST['job_salary'];
    $job_qual= $_POST['job_qual'];
    $job_sex= $_POST['job_sex'];
    $job_lang= $_POST['job_lang'];
    $job_age= $_POST['job_age'];
    $job_pub_date= $_POST['job_pub_date'];
    $job_close_date= $_POST['job_close_date'];
    $job_desc= $_POST['job_desc'];
	$job_alert = $_POST['job_alert']; 
    $job_require= $_POST['job_require'];
	$how_to_apply= $_POST['how-to-apply'];
	$admin_enable= $_POST['admin_enable'];

    mysqli_query($conn,'SET NAMES utf8');
   mysqli_query($conn,"update tbl_jobs SET `com_id`=$com_id,`func_id`=$func_id,`prov_id`=$prov_id,`ind_id`=$ind_id,`job_title`='$job_title',`job_term`='$job_term',`job_hiring`='$job_hiring',`job_salary`='$job_salary',`job_qual`='$job_qual',`job_sex`='$job_sex',`job_lang`='$job_lang',`job_age`='$job_age',`job_pub_date`='$job_pub_date',`job_close_date`='$job_close_date',`job_desc`='$job_desc',`job_require`='$job_require',`how_to_apply`='$how_to_apply',`job_alert`=$job_alert,`admin_enable`=$admin_enable WHERE job_id = $job_id");
    ?>
    <script type="text/javascript">
        window.location.href="?page=jobs";
    </script>
    <?php   
}   
?> 
<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Jobs</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                    $sql=mysqli_query($conn,"select * from tbl_jobs where job_id=$job_id");
                    mysqli_query($conn,'SET NAMES utf8');
                    while($row2=mysqli_fetch_array($sql)){
                ?>
                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                            <div class="form-group">
                               <input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;" disabled="disabled"/>
                                <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;" />                        
                            </div>
                        </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Company Name</label>
                            <select class="form-control" name="com_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_company ORDER BY com_name ASC");
                                        while($row=mysqli_fetch_array($sql)){
                                            $com_name = $row['com_name'];
                                            if($row['com_id']== $row2['com_id'])
                                                    $select="selected";
                                                else
                                                    $select="";
                                    ?>  
                                        <option value="<?php echo $row['com_id'];?>" <?php echo $select;?>><?php echo $com_name;?></option>
                                   <?php } ?>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Function Type</label>
                            <select class="form-control" name="func_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_function");
                                        while($row=mysqli_fetch_array($sql)){
                                            $func_name = $row['func_name_en'];
                                            if($row['func_id']== $row2['func_id'])
                                                    $select="selected";
                                                else
                                                    $select="";
                                    ?>  
                                        <option value="<?php echo $row['func_id'];?>" <?php echo $select;?>><?php echo $func_name;?></option>
                                   <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label style="color:#0099ff;">Location</label>
                            <select class="form-control" name="prov_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_province");
                                        while($row=mysqli_fetch_array($sql)){
                                            $prov_name = $row['prov_name_en'];
                                            if($row['prov_id']== $row2['prov_id'])
                                                    $select="selected";
                                                else
                                                    $select="";
                                    ?>  
                                        <option value="<?php echo $row['prov_id'];?>" <?php echo $select;?>><?php echo $prov_name;?></option>
                                   <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Industry Type</label>
                            <select class="form-control" name="ind_id" required>
                                    <option value=""></option>
                                    <?php 
                                        $sql=mysqli_query($conn,"select * from tbl_industry");
                                        while($row=mysqli_fetch_array($sql)){
                                            $m_name = $row['ind_name_en'];
                                            if($row['ind_id']== $row2['ind_id'])
                                                    $select="selected";
                                                else
                                                    $select="";
                                    ?>  
                                        <option value="<?php echo $row['ind_id'];?>" <?php echo $select;?>><?php echo $m_name;?></option>
                                   <?php } ?>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Title</label>
                            <input class="form-control" placeholder="Jobs Title" name="job_title" value="<?=$row2['job_title']?>" required/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Term</label>
                            <select class="form-control" name="job_term">
                                    <option <?php if($row2['job_term']==1){echo 'selected="selected"';}?> value="1">Full Time</option>
                                    <option <?php if($row2['job_term']==2){echo 'selected="selected"';}?> value="2">Part Time</option>
                                    <option <?php if($row2['job_term']==3){echo 'selected="selected"';}?> value="3">Intership</option>
                                    <option <?php if($row2['job_term']==4){echo 'selected="selected"';}?> value="4">Volunteer</option>
                                    <option <?php if($row2['job_term']==5){echo 'selected="selected"';}?> value="5">Urgent</option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Hiring</label>
                            <input class="form-control" placeholder="Job Hiring" name="job_hiring" value="<?=$row2['job_hiring']?>"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Salary</label>
                            <select class="form-control" name="job_salary">
                                <option <?php if($row2['job_salary']=='negotiable'){echo 'selected="selected"';}?> value="negotiable">Negotiable</option>
                                <option <?php if($row2['job_salary']=='200'){echo 'selected="selected"';}?> value="200"><200</option>
                                <option <?php if($row2['job_salary']=='200-500'){echo 'selected="selected"';}?> value="200-500">200-500</option>
                                <option <?php if($row2['job_salary']=='500-1000'){echo 'selected="selected"';}?> value="500-1000">500-1000</option>
                                <option <?php if($row2['job_salary']=='1000-2000'){echo 'selected="selected"';}?> value="1000-2000">100-200</option>
                                <option <?php if($row2['job_salary']=='2000'){echo 'selected="selected"';}?> value="2000">>2000</option>
                            </select>   
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Qualification</label>
                             <select class="form-control" name="job_qual">
                                    <option <?php if($row2['job_qual']==1){echo 'selected="selected"';}?> value="1">High School</option>
                                    <option <?php if($row2['job_qual']==2){echo 'selected="selected"';}?> value="2">Associate's Degree</option>
                                    <option <?php if($row2['job_qual']==3){echo 'selected="selected"';}?> value="3">Bachelor Degree</option>
                                    <option <?php if($row2['job_qual']==4){echo 'selected="selected"';}?> value="4">Master Degree</option>
                                    <option <?php if($row2['job_qual']==5){echo 'selected="selected"';}?> value="5">Doctor Degree</option>
                                    <option <?php if($row2['job_qual']==6){echo 'selected="selected"';}?> value="6">Others</option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Sex</label>
                             <select class="form-control" name="job_sex">
                                    <option <?php if($row2['job_sex']==1){echo 'selected="selected"';}?> value="1">Male</option>
                                    <option <?php if($row2['job_sex']==2){echo 'selected="selected"';}?> value="2">Female</option>
                                    <option <?php if($row2['job_sex']==3){echo 'selected="selected"';}?> value="3">Both</option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job language</label>
                            <input class="form-control" placeholder="Job language" name="job_lang" value="<?=$row2['job_lang']?>"/>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Age</label>
                            <input class="form-control" placeholder="Job Age" name="job_age" value="<?=$row2['job_age']?>"/>
                        </div>  
                    </div>
                    <div class='col-sm-6' style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="font-weight:bold; color:#0099ff;">Publish Date</label>
                            <div class="input-group date">
                                <input type="text" name="job_pub_date" placeholder="Publish Date" class="form-control" value="<?=$row2['job_pub_date']?>"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-6' style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="font-weight:bold; color:#0099ff;">Closing Date</label>
                            <div class="input-group date">
                                <input type="text" name="job_close_date" placeholder="Closing Date" class="form-control" value="<?=$row2['job_close_date']?>"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#F00;">Active/Deactive Job Post</label>
                            <select class="form-control" name="admin_enable" required>
                                    <option <?php if($row2['admin_enable']==1){echo 'selected="selected"';}?> value="1">Active</option>
                                    <option <?php if($row2['admin_enable']==0){echo 'selected="selected"';}?> value="0">Deactive</option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                           <label style="color:#F00;">Job Alert</label>
                            <select class="form-control" name="job_alert">
                                    <option <?php if($row2['job_alert']==1){echo 'selected="selected"';}?> value="1">New</option>
                                    <option <?php if($row2['job_alert']==2){echo 'selected="selected"';}?> value="2">Urgent</option>
                            </select>
                            
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Description</label>  
                            <textarea class="form-control" cols="80" name="job_desc" rows="5"><?=$row2['job_desc']?></textarea>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">Job Require</label>
                            <textarea class="form-control" cols="80" name="job_require" rows="5"><?=$row2['job_require']?></textarea>
                        </div>  
                    </div>
                    <div class="col-lg-6" style="margin-bottom:0px;">
                        <div class="form-group">
                            <label style="color:#0099ff;">How to Apply</label>
                            <textarea class="form-control" cols="80" name="how-to-apply" rows="5"><?=$row2['how_to_apply']?></textarea>
                        </div>  
                    </div>
                </div>
            </form>
            <?php } ?>
            </div>
       </div>
    </div>

</div>  
<!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
                                <th>Position</th>
                                <th>Company Name</th>
		                        <th>Function Name</th>
                                <th>Job Term</th>
                                <th>Job Hiring</th>
                                <th>Job Alert</th>
                                <th>Publish Date</th>
                                <th>Closing Date</th>
                                <th>C Per</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"SELECT t1.*,t2.func_id,t2.func_name_en,t3.com_id,t3.com_name FROM tbl_jobs t1 LEFT JOIN tbl_function t2 ON t1.func_id=t2.func_id LEFT JOIN tbl_company t3 ON t1.com_id=t3.com_id where user_enable=1 OR admin_enable=1")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
								$pubDate = date("d-m-Y", strtotime($row['job_pub_date']));
								$closeDate = date("d-m-Y", strtotime($row['job_close_date']));
                            ?>
		                    <tr>
		                        <td><?=$i;?></td>
                                <td><?= $row['job_title'];?></td>
                                <td><?= $row['com_name'];?></td>
		                        <td><?= $row['func_name_en'];?></td>
                                <td><?php if($row['job_term']==1){echo "Full Time";}else if($row['job_term']==2){ echo "Part Time";}else if($row['job_term']==3){ echo "Internship";}else if($row['job_term']==4){ echo "Volunteer";}?></td>    
                                <td align="center"><?= $row['job_hiring'];?></td>
                                <td><?php if($row['job_alert']==1){echo "New";}else if($row['job_alert']==2){echo "<b style='color:#F00;';>Urgent</b>";}?></td>
                                <td><?= $pubDate;?></td>
                                <td><?= $closeDate;?></td>
                                <td align="center">
                                	<?php
										if($row['contact_person']==''){
									?>
                                    	<a href="?page=edit_contact_per&job_id=<?php echo $row['job_id'];?>" title="Add new Contact Persion"><div class="fa fa-user fa-fw" style='color:#F00;'></div></a>
                                    <?php }else{?>
                                  		<a href="?page=edit_contact_per&job_id=<?php echo $row['job_id'];?>" title="Add/edit Contact Persion"><div class="fa fa-user fa-fw" style='color:#666;'></div></a>
                                    <?php }?>
                                </td>
                                <td align="center"><a href="?page=editjob&job_id=<?php echo $row['job_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['job_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['job_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label style="font-size:18px;"><?php echo $row['job_title']; ?></label><br/>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/jobs/delete_jobs.php<?php echo '?job_id='.$row['job_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>