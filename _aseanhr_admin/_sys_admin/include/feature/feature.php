<?php

if($_SESSION["type"] != 1){
    echo "No permission to access"; die();
}
	
function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}
$upload_dir = "../../images/feature/";   
if (isset($_POST['btsave'])) {
    $fe_title = $_POST['fe_title'];
    $fe_url = $_POST['fe_url'];

    if (isset($_FILES["file"])) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Error: " . $_FILES["file"]["error"] . "<br>";
        } else {
            //===function random name
            $image = $_FILES['file']['name'];
            $tempext = getExtension($image);
            $extfile = mt_rand() . "." . $tempext;
            //===function random name
            move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir . $extfile);
            //echo "Uploaded File :" . $_FILES["myfile"]["name"];
            $url=$extfile;  
                        
        }
    }
//======end upload file
    mysqli_query($conn,'SET NAMES utf8');
    mysqli_query($conn,"insert into `tbl_feature_emp` values('','$fe_title','$url','$fe_url')");
    ?>
    <script type="text/javascript">
        window.location.href="?page=feature";
    </script>
    <?php   
}   
?> 
 

<div class="row">
    <div class="col-lg-12" style="margin-top: -28px; margin-bottom: -13px;">
        <h1 class="page-header" style="font-size: 19px; color: #09F;">
            <i class="fa fa-table fa-fw"></i>&nbsp;Feature Images</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                            <div class="col-lg-6" style="width:100%; margin-top:0px; margin-bottom:-10px;">
                                <div class="form-group">
                                   <input type="submit" name="btsave" class="btn btn-success" value="Save" title="Save" style="width:80px;"/>
                                    <input type="submit" name="btupdate" class="btn btn-success" value="Update" title="Update" style="width:80px;" disabled="disabled"/>       </div>
                          </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Feature Name</label>
                                <input class="form-control" placeholder="Feature Name" name="fe_title"/>
                            </div> 
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">
                            <div class="form-group">
                                <label style="color:#0099ff;">Feature Link</label>
                                <input class="form-control" placeholder="Feature Link" name="fe_url"/>
                            </div>  
                        </div>
                        <div class="col-lg-6" style="margin-bottom:0px;">   
                            <div class="form-group">
                                <label style="color:#0099ff;">Feature logo</label>
                                <input type="file" name="file" required>
                            </div>
                        </div>
                </div>
            </form>
            </div>
       </div>
    </div>

</div>  
       <!-- data -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                <thead>
		                    <tr>
		                        <th width='30'>ID</th>
                                <th>Title</th>
                                <th>Images</th>
		                        <th>Url Link</th>
                                <th width='50'>Edit</th>
                                <th width='50'>Delete</th>
		                    </tr>
		                </thead>
		                <tbody>
                            <?php
                                $i=0;
                                mysqli_query($conn,'SET NAMES utf8');
                                $sql=mysqli_query($conn,"SELECT * FROM tbl_feature_emp ORDER BY fe_id DESC")or die(mysql_error());
                                while($row=mysqli_fetch_array($sql)){
                                $i++;
                            ?>
		                    <tr>
		                        <td><?=$i;?></td>
                                <td><?= $row['fe_name'];?></td>
                                <td><img src="../../images/feature/<?= $row['fe_image'];?>" width='80'/></td>
		                        <td><?= $row['fe_url'];?></td>                                
                                <td align="center"><a href="?page=edit_feature&fe_id=<?php echo $row['fe_id'];?>" title="edit"><div class="fa fa-edit fa-fw" style='color:#000;'></div></a></td>
                                <td align="center"><a href="#myModal<?php echo $row['fe_id']; ?>" data-toggle="modal" title="delete"><div class="fa fa-trash-o fa-fw" style='color:#000;'></div></a></td>
		                    </tr>
                            <div class="modal small fade" id="myModal<?php echo $row['fe_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h3 id="myModalLabel">Are you sure you want to delete?</h3>
                                    </div>
                                    <div class="modal-body">
                                        <label><?= $row['fe_name'];?></label><br/>
                                        <img src="../../images/feature/<?= $row['fe_image'];?>" width='80'/>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger" href="include/feature/delete_feature.php<?php echo '?fe_id='.$row['fe_id'];?>" ><i class="icon-check" style='color:#000;'></i>&nbsp;Yes</a>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-remove" style='color:#000;'></i> Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <?php } ?>
		                </tbody>
		            </table>
		    	</div>
			</div>
		</div>
	</div>
</div>