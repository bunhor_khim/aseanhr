<?php
session_start();
include("_aseanhr_admin/_config/connect.php");
include_once 'langauge/common.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/icon.png" />
<meta name="title" content="Aseanhr.com" />
<meta name="keywords" content="ASEANHR , Cambodia Jobs, jobs in cambodia,Jobs,Employer,Seeker,Recruitment,HR,Work,Employment,Education"/>
<meta name="description" content="ASEANHR" />

<title>ASEANHR | Cambodia Jobs|Jobs|Employer|Seeker|Recruitment|HR|Work|Employment|Education</title>
<link href="css/style1.css" rel="stylesheet" type="text/css" />
<link href="css/color.css" rel="stylesheet" type="text/css"  />
<link href='http://fonts.googleapis.com/css?family=Khmer' rel='stylesheet' type='text/css'>

<!-- jquery tab -->
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.quickflip.js" ></script>

<!-- Insert to your webpage before the </head> -->
<script src="js/sliderengine/amazingslider.js"></script>
<script src="js/sliderengine/initslider-1.js"></script>
<!-- End of head section HTML codes -->

<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="js/engine1/style.css" />
<link rel="stylesheet" type="text/css" href="js/engine2/style.css" />
<link rel="stylesheet" type="text/css" href="js/engine4/style.css" />
<!-- End WOWSlider.com HEAD section -->

<!-- alert message --> 
<script src="js/sweetalert-dev.js"></script>
<link rel="stylesheet" href="css/sweetalert.css">
<!-- end alert message --> 
<script>
jQuery(document).ready(function(){
  $("#content div").hide(); // Initially hide all content
	$("#tabs li:first").attr("id","current"); // Activate first tab
	$("#content div:first").fadeIn(); // Show first tab content
    
    $('#tabs a').click(function(e) {
        e.preventDefault();        
        $("#content div").hide(); //Hide all content
        $("#tabs li").attr("id",""); //Reset id's
        $(this).parent().attr("id","current"); // Activate this
        $('#' + $(this).attr('title')).fadeIn(); // Show content for current tab
    })
});
</script>
<!-- end jquery tab -->
<!-- data table -->

	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="css/shCore.css">
    
	
	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">

	$(document).ready(function() {
		$('#example').dataTable();
	} );

	</script>
<!-- end data table -->

<!-- alert job -->

<!-- end alert job -->

</head>

<body id="home">
<!-- alert message -->
<div class="err warning" id="add_err">
</div>
<div class="err success" id="err_success">
</div>
<!-- end alert message -->

<div class="main-wrapper">
    <div class="wrapper">
    	
        <div class="header">
           <?php include('includes/header.php');?>
        </div>
        <div class="menu">
        	<?php include('includes/menu.php');?>
        </div>
        <!-- image slideshow -->
        <div class="image_slideshow">
        	<div id="amazingslider-1" style="display:block;position:relative;">
                <ul class="amazingslider-slides" style="display:none;">
                    <?php
						$sql=mysqli_query($conn,"select * from tbl_banner WHERE cat_id=2 ORDER BY ban_id DESC");
						mysqli_query($conn,'SET NAMES utf8');
						while($row2=mysqli_fetch_array($sql)){
					?>
                    	
                    		<li><a href="<?php echo $row2['ban_url']; ?>"><img src="images/banner/<?php echo $row2['ban_image']; ?>" width="960" height="300"/></a></li>
                        
					<?php } ?>
                </ul>
            </div>
        </div>
        <!-- end image slideshow -->
        
        <div class="container">
        <!-- main -->
            <div class="main">
            	<?php include('includes/main.php');?>
            </div>
        <!-- end main -->
            <div class="side">
            	<?php include('includes/side.php'); ?>
            </div>
        </div>
        <!-- footer -->
        <div class="fonter-info-bg">
            <?php include("includes/footer.php");?>
        </div>   
        <div class="clear"></div>
        <!--end footer -->
    </div>
    <?php include('includes/copy-right.php');?>
</div>
<script type="text/javascript" src="js/engine1/wowslider.js"></script>
<script type="text/javascript" src="js/engine1/script.js"></script>
<script type="text/javascript" src="js/engine2/wowslider.js"></script>
<script type="text/javascript" src="js/engine2/script.js"></script>
<script type="text/javascript" src="js/engine4/wowslider.js"></script>
<script type="text/javascript" src="js/engine4/script.js"></script>
</body>
</html>