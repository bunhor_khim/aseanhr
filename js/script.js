// login script
$(document).ready(function(){
	
	jQuery(document).ready(function( $ ) {	
		$(document).on('click', 'a.close1, #add_err', function(){ 
		  $('#add_err').fadeOut(300 , function() {
			$('#add_err').css({display:'none'});  
		}); 
		return false;
		});
		
			$("#close_form_login").click(function(){
			$('#mask , .login-popup').fadeOut(300 , function() {
				$('#mask').remove();			
					});
				return false;
			});
	});
	
	// function email
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	
	function isValidEmailAddress(emailAddress) {
 		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
 		return pattern.test(emailAddress);
	}
	
	// check disable button register
	$('#camhragreement').click(function(){
		if (this.checked) {
			$("#registersubmit").removeAttr("disabled");
		}else{
			$("#registersubmit").attr("disabled", true);
		}
	}) 
	
	// page register
	$("#email").keyup(function(){
		var email = $("#email").val();
		if(email != 0)
		{
			if(isValidEmailAddress(email))
			{
				
			} else {
			}
		} else {
			
		}
	
	});
	
	
	$("#registersubmit").click(function(){
		user=$("#username").val();
		email=$("#email").val();
		pass=$("#password").val();
		c_pass=$("#c_password").val();
		
		if(user === ""){
			$("#add_err").css({display:'block'});
			$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a> username can't be null.");
			setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
			document.getElementById("username").focus();	
		
		}else{
			if(user.length < 3 ){
				$("#add_err").css({display:'block'});
				$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Username must have character more than 3");
				setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
				document.getElementById("username").focus();
			}else{
				  if(email ===""){
					   	$("#add_err").css({display:'block'});
						$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Email can't be null");
						setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
						document.getElementById("email").focus();
				  }else{
					  if(!emailReg.test(email)){
						  $("#add_err").css({display:'block'});
						  $("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Invalid email");
						  setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
					  }else{
						  	$.ajax({
							type:"POST",
							url:"ajax/email_valid.php",
							data:"email="+email,
							success:function(html){
								if(html=='true'){
									$("#add_err").css({display:'block'});
									$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Your email is already");
									setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);	
								}else{
									if(pass===""){
										$("#add_err").css({display:'block'});
										$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Please fill your password");
										setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
										document.getElementById("password").focus();
									}else{
									if(pass.length<6){
										$("#add_err").css({display:'block'});
										$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Password must have character more than 6");
										setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
										document.getElementById("password").focus();
									}else{
										if(c_pass===""){
											$("#add_err").css({display:'block'});
											$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Please fill your Comfirm Password.");
											setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
											document.getElementById("password").focus();
										}else{
											if(pass===c_pass){
												$.ajax({
													type: "POST",
													url: "ajax/register.php",
													data: "user="+user+"&email="+email+"&pass="+pass,
													success: function(html){
													  if(html=='true'){
														$("#err_success").css({display:'block'});
														$("#err_success").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Regiser Successfully");
														setTimeout(function(){$('#err_success').css({display:'none'});}, 5000);
														  window.location.href="/users";
													  }else{
														  $("#add_err").css({display:'block'});
														  $("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Regiter Unsuccessful");
														  setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);	
													  }
													}
												 });
												 return false;
											}else{
												$("#add_err").css({display:'block'});
												$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Comfrim password is not match.");
												setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
												document.getElementById("c_password").focus();
												
											}
										}
										
									}								
								}
							}}
							});
							
					  }
				  }
			}
			
		}
		
		 
	 });
	
	
	//login
	$("#login").click(function(){
		user_name=$("#user_name").val();
		password=$("#password").val();
		if(user_name === ""){
			$("#add_err").css({display:'block'});
			$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Please fill your Username / Email.");
			setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
			document.getElementById("user_name").focus();
		}else{
			if(password === ""){
				$("#add_err").css({display:'block'});
				$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Please fill your Password.");
				setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
				document.getElementById("password").focus();	
				
			}else{
				$.ajax({
					type: "POST",
					url: "ajax/login.php",
					data: "user_name="+user_name+"&password="+password,
					success: function(html){
					  if(html=='true'){
						$("#err_success").css({display:'block'});
						$("#err_success").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Login Successfully");
						setTimeout(function(){$('#err_success').css({display:'none'});}, 5000);
						window.location = "users/";
					  }else{
						  $("#add_err").css({display:'block'});
						  $("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Username and Password is invalid");
						  setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);	
					  }
					}
				 });
				 return false;
			}
		}
	});
	
	
	// employer login
	$("#employer_login").click(function(){
		emp_name=$("#emp_name").val();
		emp_password=$("#emp_password").val();
		if(emp_name === ""){
			$("#add_err").css({display:'block'});
			$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Please fill your Username / Email.");
			setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
			document.getElementById("emp_name").focus();
		}else{
			if(emp_password === ""){
				$("#add_err").css({display:'block'});
				$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a>Please fill your Password.");
				setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
				document.getElementById("emp_password").focus();	
				
			}else{
				$.ajax({
					type: "POST",
					url: "employer/ajax/emp_login.php",
					data: "emp_name="+emp_name+"&emp_password="+emp_password,
					success: function(html){
					  if(html=='true'){
						$("#err_success").css({display:'block'});
						$("#err_success").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Login Successfully");
						setTimeout(function(){$('#err_success').css({display:'none'});}, 5000);
						window.location = "employer/index.php";
					  }else{
						  $("#add_err").css({display:'block'});
						  $("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a> Username and Password is invalid");
						  setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);	
					  }
					}
				 });
				 return false;
			}
		}
	});
	
	
});