// login script
$(document).ready(function(){
	jQuery(document).ready(function( $ ) {	
		$(document).on('click', 'a.close1, #add_err', function(){ 
		  $('#add_err').fadeOut(300 , function() {
			$('#add_err').css({display:'none'});  
		}); 
		return false;
		});
		
			$("#close_form_login").click(function(){
			$('#mask , .login-popup').fadeOut(300 , function() {
				$('#mask').remove();			
					});
				return false;
			});
	});
	
	// function email
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	
	// function email
	function isValidEmailAddress(emailAddress) {
 		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
 		return pattern.test(emailAddress);
	}
	
	// check disable button register
	$('#camhragreement').click(function(){
		if (this.checked) {
			$("#empsubmit").removeAttr("disabled");
		}else{
			$("#empsubmit").attr("disabled", true);
		}
	}) 
	
	// page register
	$("#email").keyup(function(){
		var email = $("#email").val();
		if(email != 0)
		{
			if(isValidEmailAddress(email))
			{
				
			} else {
			}
		} else {
			
		}
	
	});
	
	$("#empsubmit").click(function(){
		
		user=$("#username").val();
		email=$("#email").val();
		pass=$("#password").val();
		c_pass=$("#c_password").val();
		com_name=$("#com_name").val();
		phone=$("#phone").val();
		
		if(user === ""){
			
			$("#add_err").css({display:'block'});
			$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your username.");
			setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
			document.getElementById("username").focus();	
		
		}else{
			if(user.length<3){
				$("#add_err").css({display:'block'});
				$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Username must have character more than 3.");
				setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
				document.getElementById("username").focus();
			}else{
				if(email === ""){
				$("#add_err").css({display:'block'});
				$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your email.");
				setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
				document.getElementById("email").focus();
				
				}else{
					if(!emailReg.test(email)){
						  $("#add_err").css({display:'block'});
						  $("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Your email is not email");
						  setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
						  document.getElementById("email").focus();
					  }else{
						$.ajax({
							type:"POST",
							url:"ajax/email_valid.php",
							data:"email="+email,
							success:function(html){
								if(html=='true'){
									$("#add_err").css({display:'block'});
									$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning!   </strong>  You email is already! ");
									setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);	
								}else{
									if(pass === ""){
										$("#add_err").css({display:'block'});
										$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your password.");
										setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
										document.getElementById("password").focus();
									}else{
										if(pass.length<6){
											$("#add_err").css({display:'block'});
											$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Password must have character more than 6");
											setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
											document.getElementById("password").focus();
										}else{
											if(c_pass===""){
												$("#add_err").css({display:'block'});
												$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your Comfirm Password.");
												setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
												document.getElementById("password").focus();
											}else{
												if(pass===c_pass){
													if(com_name === ""){
														$("#add_err").css({display:'block'});
														$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your Company Name.");
														setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
														document.getElementById("com_name").focus();
													}else{
														if(phone === ""){
															$("#add_err").css({display:'block'});
															$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your phone.");
															setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
															document.getElementById("phone").focus();
															}else{
																$.ajax({
																type: "POST",
																url: "ajax/register.php",
																data: "user="+user+"&email="+email+"&pass="+pass+"&com_name="+com_name+"&phone="+phone,
																success: function(html){
																  if(html=='true'){
																	$("#err_success").css({display:'block'});
																	$("#err_success").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Well done!   </strong>Successful!");
																	setTimeout(function(){$('#err_success').css({display:'none'});}, 5000);
																	window.location = "/employer";
																	
																  }else{
																	  $("#add_err").css({display:'block'});
																	  $("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Unsuccessful");
																	  setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);	
																  }
																}
															 });
															 return false;
															}
													}
												}else{
													$("#add_err").css({display:'block'});
													$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Comfrim password is not right.");
													setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
													document.getElementById("c_password").focus();
												}
											}
										}
									}
								}
							}
						});/* invalid email */
						
					}
				}/* email */
			}
		}
		
		 
	 });
	
	
	//login
	$("#employer_login").click(function(){
		emp_name=$("#emp_name").val();
		emp_password=$("#emp_password").val();
		if(emp_name === ""){
			$("#add_err").css({display:'block'});
			$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your Username / Email.");
			setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
			document.getElementById("emp_name").focus();
		}else{
			if(emp_password === ""){
				$("#add_err").css({display:'block'});
				$("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Warning! </strong>Please! Insert your Password.");
				setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);
				document.getElementById("emp_password").focus();	
				
			}else{
				$.ajax({
					type: "POST",
					url: "ajax/emp_login.php",
					data: "emp_name="+emp_name+"&emp_password="+emp_password,
					success: function(html){
					  if(html=='true'){
						$("#err_success").css({display:'block'});
						$("#err_success").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Login Successful!</strong>");
						setTimeout(function(){$('#err_success').css({display:'none'});}, 5000);
						window.location = "index.php";
					  }else{
						  $("#add_err").css({display:'block'});
						  $("#add_err").html("<a href='#' class='close1' data-dismiss='alert'>×</a><strong>Invalid UserName OR Password!</strong>");
						  setTimeout(function(){$('#add_err').css({display:'none'});}, 5000);	
					  }
					}
				 });
				 return false;
			}
		}
	});
	
	
	
});